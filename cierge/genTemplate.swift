//
//  genTemplate.swift
//  cierge
//
//  Created by Alberto Cesare Barbon on 19/01/17.
//  Copyright © 2017 Alberto Cesare Barbon. All rights reserved.
//

import UIKit
import Parse

class genTemplate: UIViewController, UIScrollViewDelegate {

    var image : UIImageView = UIImageView()
    var sottotitolo : UILabel = UILabel()
    var sottotitolo1 : UILabel = UILabel()
    var titolo : UILabel = UILabel()
    var descri : UITextView = UITextView()
    var scrollView : UIScrollView = UIScrollView()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        NotificationCenter.default.addObserver(self, selector: #selector(checkTemplate1.pop2), name: NSNotification.Name.init(rawValue: "cambia"), object: nil)
        
        view.frame = CGRect(x: 0, y: 0, width: view.frame.width, height: view.frame.height - (110))
        scrollView.frame = view.frame
        
        scrollView.delegate = self
        
        image.frame = CGRect(x: 0, y: 0, width: view.frame.width, height: 200)
        scrollView.addSubview(image)
        let userDefaults = Foundation.UserDefaults.standard
        let value  = userDefaults.string(forKey: "idService")
        let query = PFQuery(className:"InServices")
        query.getObjectInBackground(withId: value!, block: { (object, error) -> Void in
            
            let foto = object?["photo"] as! PFFile
            foto.getDataInBackground { (imageData, error) -> Void in
                if (error == nil) {
                    if (imageData != nil) {
                        self.image.image = UIImage(data:imageData!)
                    }
                }
            }
            
            let inutile0 = object?["info"] as! String
            let inutile1 = inutile0.components(separatedBy: "@")
            let infosArr = inutile1[1]
            let infos = infosArr.components(separatedBy: "*")
            let nome = infos[0]
            self.titolo.text = nome
            
            let sotto = infos[1]
            self.sottotitolo.text = sotto
            self.sottotitolo1.text = sotto
            
            
            let descr = infos[2]
            self.descri.text = descr
        })
        
        
        
        
        sottotitolo1.frame = CGRect(x: 20, y: image.frame.maxY + 20, width: view.frame.width - 40, height: 20)
        sottotitolo1.textColor = UIColor.lightGray
        sottotitolo1.font = UIFont.systemFont(ofSize: 18)
        sottotitolo1.textAlignment = .center
        scrollView.addSubview(sottotitolo1)
        
        titolo.frame = CGRect(x: 20, y: sottotitolo1.frame.maxY + 20, width: view.frame.width - 40, height: 35)
        titolo.font = UIFont.systemFont(ofSize: 30)
        titolo.textColor = .black
        titolo.textAlignment = .center
        scrollView.addSubview(titolo)
        
        let viewU1 : UIView = UIView(frame: CGRect(x: 20, y: titolo.frame.maxY + 20, width: view.frame.width - 40, height: 1))
        viewU1.backgroundColor = UIColor.init(red: 239/255, green: 239/255, blue: 239/255, alpha: 1)
        scrollView.addSubview(viewU1)
        
        /*let orologio : UIImageView = UIImageView()
         orologio.frame = CGRect(x: 20, y: viewU1.frame.maxY + 20, width: 20, height: 20)
         orologio.image = UIImage(named: "time.png")
         view.addSubview(orologio)*/
        
        descri.frame = CGRect(x: 20, y: titolo.frame.maxY + 40, width: view.frame.width - 40, height: 300)
        descri.font = UIFont.systemFont(ofSize: 15)
        descri.textColor = UIColor.gray
        descri.isEditable = false
        
        scrollView.addSubview(descri)
        scrollView.contentSize = CGSize(width: view.frame.width, height: descri.frame.maxY)
        view.addSubview(scrollView)
        
        let btBack : UIButton = UIButton(frame: CGRect(x: 5, y: scrollView.frame.maxY - 50, width: 150, height: 30))
        btBack.setImage(UIImage(named:"cierge-trasp-1.png"), for: .normal)
        btBack.layer.cornerRadius = 15
        btBack.clipsToBounds = true
        btBack.setTitle("Indietro", for: .normal)
        btBack.setTitleColor(.white, for: .normal)
        btBack.contentHorizontalAlignment = .left
        btBack.addTarget(self, action: #selector(ristoTemplate.pop(_:)), for: .touchUpInside)
        //view.addSubview(btBack)
        let labelBack : UILabel = UILabel(frame: CGRect(x: 15, y: scrollView.frame.maxY - 50 + 15 - 10, width: 40, height: 20))
        labelBack.text = "Indietro"
        labelBack.textColor = .white
        labelBack.textAlignment = .center
        //view.addSubview(labelBack)
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        if scrollView.contentOffset.y < -30 {
            self.navigationController?.popToRootViewController(animated: true)
        }
    }
    
    func pop(_ button: UIButton) {
        _ = navigationController?.popViewController(animated: true)
    }
    
    func pop2() {
        _ = navigationController?.popViewController(animated: true)
    }
    
    func dismiss(_ sender: UIButton) {
        let id = "1"
        let userDefaults = Foundation.UserDefaults.standard
        userDefaults.set( id, forKey: "numTemplate")
        let vc = self.storyboard!.instantiateViewController(withIdentifier: "hotelController") as! hotelController
        self.present(vc, animated: true, completion: nil)
    }


}
