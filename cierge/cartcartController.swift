//
//  cartcartController.swift
//  cierge
//
//  Created by Alberto Cesare Barbon on 03/03/17.
//  Copyright © 2017 Alberto Cesare Barbon. All rights reserved.
//

import UIKit
import Parse
import LNRSimpleNotifications
import AudioToolbox

class cartcartController: UIViewController, UITableViewDelegate, UITableViewDataSource {
    
    var tableView = UITableView()
    let notificationManager = LNRNotificationManager()
    var array : [PFObject] = []
    
    var labelPrice : UILabel = UILabel()
    var labelTot : UILabel = UILabel()
    var btPay : UIButton = UIButton()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        btPay.frame = CGRect(x: view.frame.width - 70, y: view.frame.height - 220, width: 50, height: 50)
        btPay.setTitle("Paga", for: .normal)
        btPay.setTitleColor(.white, for: .normal)
        btPay.layer.cornerRadius = 25
        btPay.clipsToBounds = true
        btPay.addTarget(self, action: #selector(cartcartController.pay(_:)), for: .touchUpInside)
        btPay.backgroundColor = UIColor.init(red: 213/255, green: 190/255, blue: 111/255, alpha: 1)
        
        labelTot.frame = CGRect(x: 20, y: view.frame.height - 190, width: 55, height: 20)
        labelTot.textColor = .gray
        labelTot.text = "Tot: €"
        labelTot.font = UIFont.systemFont(ofSize: 20)
        
        labelPrice.frame = CGRect(x: 80, y: view.frame.height - 190, width: 300, height: 20)
        labelPrice.textColor = .gray
        labelPrice.text = "0"
        labelPrice.font = UIFont.systemFont(ofSize: 20)
        
        notificationManager.notificationsPosition = LNRNotificationPosition.top
        notificationManager.notificationsBackgroundColor = UIColor.init(red: 205/255, green: 89/255, blue: 91/255, alpha: 1)
        notificationManager.notificationsTitleTextColor = UIColor.white
        notificationManager.notificationsBodyTextColor = UIColor.white
        notificationManager.notificationsSeperatorColor = UIColor.clear
        
        tableView.delegate = self
        tableView.dataSource = self
        tableView.frame = view.frame
        self.tableView.tableFooterView = UIView()
        view.addSubview(tableView)
        
        //query()
        
        let payBt = UIButton()
        payBt.frame = CGRect(x: view.frame.width - 70, y: view.frame.height - 70, width: 50, height: 50)
        payBt.layer.cornerRadius = 25
        payBt.backgroundColor = UIColor.init(red: 213/255, green: 190/255, blue: 111/255, alpha: 1)
        payBt.setTitle("Paga", for: .normal)
        payBt.isHidden = false
        payBt.isEnabled = true
        payBt.addTarget(self, action: #selector(cartcartController.pay(_:)), for: .touchUpInside)
        //view.addSubview(payBt)
    }
    
    
    
    func query() {
        array = []
        let userId = PFUser.current()?.objectId
        let query = PFQuery(className: "Purchasing")
        //query.cachePolicy = .cacheElseNetwork
        query.whereKey("userId", equalTo: userId!)
        query.whereKey("state", equalTo: "p")

        query.findObjectsInBackground { (objects, error) -> Void in
            
            if (objects?.count != 0) {
                //self.view.addSubview(self.btPay)
                //self.view.addSubview(self.labelPrice)
                //self.view.addSubview(self.labelTot)
                if objects?.count == 0 {
                    
                    let textView : UITextView = UITextView()
                    textView.frame = CGRect(x: 20, y: self.view.frame.height / 2 - 50, width: self.view.frame.width - 40, height: 100)
                    textView.textAlignment = .center
                    textView.text = "In questa pagina visualizzerai i tuoi acquisti in attesa di essere approvati dall'Hotel"
                    textView.font = UIFont.systemFont(ofSize: 15)
                    textView.textColor = .darkGray
                    self.view.addSubview(textView)
                }
            }
            
            for object in objects! {
                
                self.array.append(object)
                self.tableView.reloadData()
                
            }
            
        }
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return array.count
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 70
    }
    
    func pay(_ button: UIButton) {
        
        
        
        for object in array {
            
            
            
            let pfObj = PFObject(className: "Purchasing")
            pfObj["userId"] = object["userId"]
            pfObj["productId"] = object["productId"]
            pfObj["method"] = object["method"]
            pfObj["qty"] = object["qty"]
            pfObj["sellerId"] = object["sellerId"]
            pfObj["state"] = object["state"]
            pfObj["delivered"] = false as Bool
            pfObj.saveInBackground()
            
            object.deleteInBackground()
            
            
           
            
            array.removeFirst()
            
        }
        
        
        self.notificationManager.showNotification(title: "Richiesta", body: "Inviata", onTap: nil)
        AudioServicesPlayAlertSound(SystemSoundID(kSystemSoundID_Vibrate))
        self.tableView.reloadData()
        NotificationCenter.default.post(name: NSNotification.Name(rawValue: "reload") , object: nil)
    }
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = UITableViewCell()
        
        let random = arc4random_uniform(5)
        var color = UIColor()
        switch random {
        case 0:
            
            color = UIColor.init(red: 230/255, green: 37/255, blue: 101/255, alpha: 1)
            
            break
        case 1:
            
            color = UIColor.init(red: 43/255, green: 152/255, blue: 240/255, alpha: 1)
            
            break
        case 2:
            
            color = UIColor.init(red: 80/255, green: 174/255, blue: 85/255, alpha: 1)
            
            break
        case 3:
            
            color = UIColor.init(red: 253/255, green: 192/255, blue: 47/255, alpha: 1)
            
            break
        case 4:
            
            color = UIColor.init(red: 21/255, green: 149/255, blue: 136/255, alpha: 1)
            
            break
        case 5:
            
            color = UIColor.init(red: 241/255, green: 69/255, blue: 51/255, alpha: 1)
            
            break
        default:
            break
        }
        
        let buttonIn = UIButton(frame: CGRect(x: 15, y: 10, width: 50, height: 50))
        buttonIn.layer.cornerRadius = 25
        buttonIn.isUserInteractionEnabled = false
        buttonIn.clipsToBounds = true
        buttonIn.backgroundColor = #colorLiteral(red: 0.9019607843, green: 0.7450980392, blue: 0.4352941176, alpha: 1)
        buttonIn.setTitleColor(.white, for: .normal)
        cell.addSubview(buttonIn)
        
        let pfObj = array[indexPath.row]
        
        
        let title = UILabel(frame: CGRect(x: 80, y: cell.frame.height/2 - 10, width: cell.frame.width - 60, height: 20))
        let hotelId = pfObj["productId"]
        let query = PFQuery(className: "Products")
        query.getObjectInBackground(withId: hotelId as! String, block: { (object, error) -> Void in
            
            let price = object?["price"] as! String
            let testo = Int(self.labelPrice.text!)
            let qta = pfObj["qty"] as! String
            let prezzo : Int = testo! + Int(qta)! * Int(price)!
            self.labelPrice.text = String(describing: prezzo)
            
            let name = object?["name"] as! String
            title.text = name
            cell.addSubview(title)
            
            let arrname = name.components(separatedBy: " ")
            let string1 = arrname[0]
            let ini1 = string1.characters.first
            let stringa = String(describing: ini1!)
            
            buttonIn.setTitle(stringa, for: .normal)
            
            
        })
        
        let labelDate = UILabel(frame: CGRect(x: 80, y: title.frame.maxY + 10, width: cell.frame.width - 60, height: 20))
        
        labelDate.textColor = .gray
        labelDate.font = UIFont.systemFont(ofSize: 14)
        labelDate.text = "Attendo Conferma"
        
        cell.addSubview(labelDate)
        
        
        return cell
    }
    
    override func viewDidAppear(_ animated: Bool) {
        
        
        query()
        self.tableView.reloadData()
        self.tableView.tableFooterView = UIView()

    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        tableView.deselectRow(at: indexPath, animated: true)
        
    }
    
    func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        return true
    }
    
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
        
        let id = array[indexPath.row].objectId!
        print(id)
        
        notificationManager.showNotification(title: "Cancellazione...", body: "Richiesta Inviata", onTap: nil)
        AudioServicesPlayAlertSound(SystemSoundID(kSystemSoundID_Vibrate))
        
        let query = PFQuery(className:"PurchasingCart")
        query.getObjectInBackground(withId: id, block: { (object, error) -> Void in
            
            self.array.remove(at: indexPath.row)
            object?.deleteEventually()
            tableView.reloadData()
            
        })
        
    }

}
