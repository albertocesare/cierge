//
//  RistoTableViewCell.swift
//  cierge
//
//  Created by Alberto Cesare Barbon on 23/02/17.
//  Copyright © 2017 Alberto Cesare Barbon. All rights reserved.
//

import UIKit
import Parse
import ParseUI

class RistoTableViewCell: PFTableViewCell {
    
    @IBOutlet weak var cellImageView: PFImageView!
    @IBOutlet weak var titleLabel: UILabel!

}
