//
//  settingController.swift
//  cierge
//
//  Created by Alberto Cesare Barbon on 17/01/17.
//  Copyright © 2017 Alberto Cesare Barbon. All rights reserved.
//

import UIKit
import Parse
import LNRSimpleNotifications
import AudioToolbox

class settingController: UIViewController, UITextFieldDelegate, UINavigationControllerDelegate, UIImagePickerControllerDelegate, UIScrollViewDelegate {
    
    let notificationManager = LNRNotificationManager()
    
    let btFoto : UIButton = UIButton()
    let scrollView : UIScrollView = UIScrollView()
    var foto:UIImageView = UIImageView()
    let nome:UITextField = UITextField()
    let email:UITextField = UITextField()
    let telefono:UITextField = UITextField()
    let città:UITextField = UITextField()
    let cittàPlace:UILabel = UILabel()
    let dataNascita:UITextField = UITextField()
    let dataPlace:UILabel = UILabel()
    let sessoM:UIButton = UIButton()
    let sessoF:UIButton = UIButton()
    let cambiaP:UILabel = UILabel()
    let oldP:UITextField = UITextField()
    let oldPPlace:UILabel = UILabel()
    let newP:UITextField = UITextField()
    let newPPlace:UILabel = UILabel()
    let cnewP:UITextField = UITextField()
    let cnewPlace:UILabel = UILabel()
    let saveSetting:UIButton = UIButton()
    let save:UIButton = UIButton()
    let delete:UIButton = UIButton()
    let imageFront:UIImageView = UIImageView()
    let imageBack:UIImageView = UIImageView()
    
    let view1:UIView = UIView()
    let view2:UIView = UIView()
    let view3:UIView = UIView()
    let view4:UIView = UIView()
    let view5:UIView = UIView()
    let view6:UIView = UIView()
    let view7:UIView = UIView()
    let view8:UIView = UIView()
    let view9:UIView = UIView()
    let view10:UIView = UIView()
    
    var imagePicker: UIImagePickerController!
    
    var imageM = UIImage()
    var imageF = UIImage()

    override func viewDidLoad() {
        super.viewDidLoad()

        //NotificationCenter.default.addObserver(self, selector: #selector(checkTemplate1.pop2), name: NSNotification.Name.init(rawValue: "cambia"), object: nil)
        
        notificationManager.notificationsPosition = LNRNotificationPosition.top
        notificationManager.notificationsBackgroundColor = UIColor.init(red: 109/255, green: 213/255, blue: 154/255, alpha: 1)
        notificationManager.notificationsTitleTextColor = UIColor.white
        notificationManager.notificationsBodyTextColor = UIColor.white
        notificationManager.notificationsSeperatorColor = UIColor.clear
        
        // Do any additional setup after loading the view.
        
        let viewUp : UIView = UIView()
        viewUp.frame = CGRect(x: 0, y: 0, width: view.frame.width, height: 64)
        viewUp.backgroundColor = UIColor.init(red: 239/255, green: 239/255, blue: 239/255, alpha: 1)
        view.addSubview(viewUp)
        let label : UILabel = UILabel(frame: CGRect(x: 20, y: 25, width: view.frame.width-40, height: 31))
        label.textAlignment = .center
        label.text = "Impostazioni"
        label.textColor = UIColor.darkGray
        label.font = UIFont(name: "HelveticaNeue", size: 20)
        viewUp.addSubview(label)
        let notifica : UIButton = UIButton(frame: CGRect(x: view.frame.width - 37.5, y: 27.5, width: 30, height: 30))
        notifica.setImage(UIImage(named: "bell.ong")?.maskWith(color: .darkGray), for: .normal)
        notifica.addTarget(self, action: #selector(settingController.notifica), for: .touchUpInside)
        viewUp.addSubview(notifica)
        
        scrollView.frame = CGRect(x: 0, y: 64, width: view.frame.width, height: view.frame.height - 64)
        scrollView.delegate = self
        view.addSubview(scrollView)
        
        let user = PFUser.current()
        let userImageFile = user?["photo"] as! PFFile
        userImageFile.getDataInBackground { (imageData, error) -> Void in
            
                self.foto.image = UIImage(data:imageData!)
                self.foto.clipsToBounds = true
            
        }
        let docFront = user?["docFront"] as? PFFile
        docFront?.getDataInBackground { (imageData, error) -> Void in
            
            self.imageFront.image = UIImage(data:imageData!)
            self.imageFront.clipsToBounds = true
            
        }
        let docBack = user?["docBack"] as? PFFile
        docBack?.getDataInBackground { (imageData, error) -> Void in
            
            self.imageBack.image = UIImage(data:imageData!)
            self.imageBack.clipsToBounds = true
            
        }
        
        let query = PFQuery(className:"_User")
        query.cachePolicy = .networkElseCache
        query.getObjectInBackground(withId: (PFUser.current()?.objectId)!, block: { (object, error) -> Void in
          
            self.nome.text = (object?["name"] as! String) + " " + (object?["surname"] as! String)
            self.email.text = (object?["email"] as! String)
            let info = (object?["info"] as! String).components(separatedBy: "*")
            //let sex = info[0]
            let city = info[1]
            let data = info[2]
            let phone = info[3]
            
            self.telefono.text = phone
            self.città.text = city
            self.dataNascita.text = data
            self.preparaScroll()
            
            let sesso = info[0]
            if (sesso == "male") {
                
                self.sexM()
                
            } else {
                
                self.sexF()
            }
            
        })
        
        foto.frame = CGRect(x: view.frame.width/2 - 64, y: 20, width: 128, height: 128)
        btFoto.frame = foto.frame
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }

    func preparaScroll() {
        
        let screenSize: CGRect = UIScreen.main.bounds
        let screenWidth = screenSize.width
        //let screenHeight = screenSize.height
        
        foto.frame = CGRect(x: screenWidth/2 - 64, y: 20, width: 128, height: 128)
        foto.backgroundColor = .lightGray
        foto.layer.cornerRadius = 64
        //foto.sizeToFit()
        scrollView.addSubview(foto)
        btFoto.frame = foto.frame
        btFoto.addTarget(self, action: #selector(settingController.buttFoto), for: .touchUpInside)
        scrollView.addSubview(btFoto)
        
        
        nome.frame = CGRect(x: 0, y: 160, width: screenWidth, height: 40)
        nome.placeholder = "Nome e Cognome"
        nome.font = UIFont(name: "HelveticaNeue", size: 30)
        nome.textAlignment = .center
        nome.delegate = self
        scrollView.addSubview(nome)
        
        view1.frame = CGRect(x: 20, y: 210, width: screenWidth-40, height: 1)
        view1.backgroundColor = UIColor.init(red: 238/255, green: 238/255, blue: 238/255, alpha: 1)
        scrollView.addSubview(view1)
        
        email.frame = CGRect(x: 0, y: 240, width: screenWidth, height: 20)
        email.placeholder = ""
        email.isUserInteractionEnabled = false
        email.textAlignment = .center
        scrollView.addSubview(email)
        
        view2.frame = CGRect(x: 20, y: 270, width: screenWidth-40, height: 1)
        view2.backgroundColor = UIColor.init(red: 238/255, green: 238/255, blue: 238/255, alpha: 1)
        scrollView.addSubview(view2)
        
        telefono.frame = CGRect(x: 0, y: 310, width: screenWidth, height: 20)
        telefono.placeholder = "Numero di Telefono"
        telefono.textAlignment = .center
        telefono.delegate = self
        telefono.clearButtonMode = .whileEditing
        telefono.keyboardType = .numberPad
        scrollView.addSubview(telefono)
        
        view3.frame = CGRect(x: 20, y: 340, width: screenWidth-40, height: 1)
        view3.backgroundColor = UIColor.init(red: 238/255, green: 238/255, blue: 238/255, alpha: 1)
        scrollView.addSubview(view3)
        
        cittàPlace.frame = CGRect(x: 20, y: 380, width: screenWidth, height: 20)
        cittàPlace.text = "CITTÀ"
        cittàPlace.font = UIFont(name: "HelveticaNeue", size: 15)
        cittàPlace.textColor = UIColor.lightGray
        cittàPlace.textAlignment = .left
        scrollView.addSubview(cittàPlace)
        
        città.frame = CGRect(x: screenWidth/2  , y: 380, width: screenWidth/2 - 20, height: 20)
        città.placeholder = "città"
        città.textColor = UIColor.black
        città.font = UIFont(name: "HelveticaNeue", size: 17)
        città.textAlignment = .left
        città.delegate = self
        città.clearButtonMode = .whileEditing
        scrollView.addSubview(città)
        
        view4.frame = CGRect(x: 20, y: 410, width: screenWidth-40, height: 1)
        view4.backgroundColor = UIColor.init(red: 238/255, green: 238/255, blue: 238/255, alpha: 1)
        scrollView.addSubview(view4)
        
        dataPlace.frame = CGRect(x: 20, y: 450, width: screenWidth, height: 20)
        dataPlace.text = "DATA DI NASCITA"
        dataPlace.font = UIFont(name: "HelveticaNeue", size: 15)
        dataPlace.textColor = UIColor.lightGray
        dataPlace.textAlignment = .left
        scrollView.addSubview(dataPlace)
        
        dataNascita.frame = CGRect(x: screenWidth/2 , y: 450, width: screenWidth/2 - 20 , height: 20)
        dataNascita.placeholder = "dd/mm/yyyy"
        dataNascita.textColor = UIColor.black
        dataNascita.font = UIFont(name: "HelveticaNeue", size: 17)
        dataNascita.textAlignment = .left
        dataNascita.delegate = self
        dataNascita.clearButtonMode = .whileEditing
        scrollView.addSubview(dataNascita)
        
        view5.frame = CGRect(x: 20, y: 480, width: screenWidth - 40, height: 1)
        view5.backgroundColor = UIColor.init(red: 238/255, green: 238/255, blue: 238/255, alpha: 1)
        scrollView.addSubview(view5)
        
        sessoM.frame = CGRect(x: 100, y: 520, width: 50, height: 50)
        imageM = (UIImage(named: "male.png")?.maskWith(color: .lightGray))!
        sessoM.setImage(imageM, for: .normal)
        scrollView.addSubview(sessoM)
        let sessoMBt : UIButton = UIButton()
        sessoMBt.frame = CGRect(x: 100, y: 520, width: 50, height: 50)
        sessoMBt.addTarget(self, action: #selector(settingController.sexM), for: .touchUpInside)
        scrollView.addSubview(sessoMBt)
        
        sessoF.frame = CGRect(x: screenWidth - 150, y: 520, width: 50, height: 50)
        imageF = (UIImage(named: "female.png")?.maskWith(color: .lightGray))!
        sessoF.setImage(imageF, for: .normal)
        scrollView.addSubview(sessoF)
        let sessoFBt : UIButton = UIButton()
        sessoFBt.frame = CGRect(x: screenWidth - 150, y: 520, width: 50, height: 50)
        sessoFBt.addTarget(self, action: #selector(settingController.sexF), for: .touchUpInside)
        scrollView.addSubview(sessoFBt)
        
        view6.frame = CGRect(x: 20, y: 580, width: screenWidth - 40, height: 1)
        view6.backgroundColor = UIColor.init(red: 238/255, green: 238/255, blue: 238/255, alpha: 1)
        scrollView.addSubview(view6)
        
        
        imageFront.frame = CGRect(x: view.frame.width/2 - 120, y: view6.frame.maxY + 30, width: 100, height: 100)
        imageFront.layer.borderWidth = 1
        imageFront.layer.borderColor = UIColor.black.cgColor
        imageFront.backgroundColor = .lightGray
        scrollView.addSubview(imageFront)
        
        let buttonFront:UIButton = UIButton(frame: imageFront.frame)
        buttonFront.addTarget(self, action: #selector(settingController.buttFront), for: UIControlEvents.touchUpInside)
        scrollView.addSubview(buttonFront)
        
        imageBack.frame = CGRect(x: view.frame.width/2 + 20, y: view6.frame.maxY + 30, width: 100, height: 100)
        imageBack.layer.borderWidth = 1
        imageBack.layer.borderColor = UIColor.black.cgColor
        imageBack.backgroundColor = .lightGray
        
        scrollView.addSubview(imageBack)
        
        let buttonBack:UIButton = UIButton(frame: imageBack.frame)
        buttonBack.addTarget(self, action: #selector(settingController.buttBack), for: UIControlEvents.touchUpInside)
        scrollView.addSubview(buttonBack)
        
        
        saveSetting.frame = CGRect(x: screenSize.width / 2 - 100, y: imageFront.frame.maxY + 30, width: 200, height: 50)
        saveSetting.setTitle("Salva Impostazioni", for: UIControlState.normal)
        saveSetting.setTitleColor(UIColor.white, for: UIControlState.normal)
        saveSetting.backgroundColor = UIColor.init(red: 213/255, green: 190/255, blue: 111/255, alpha: 1)
        saveSetting.addTarget(self, action: #selector(settingController.saveSettingFunc), for: UIControlEvents.touchUpInside)
        saveSetting.layer.cornerRadius = 5
        scrollView.addSubview(saveSetting)
        /*
        cambiaP.frame = CGRect(x: 15 , y: saveSetting.frame.maxY + 30, width: screenWidth, height: 20)
        cambiaP.text = "CAMBIA PASSWORD"
        cambiaP.textColor = UIColor.black
        cambiaP.font = UIFont(name: "HelveticaNeue", size: 15)
        cambiaP.textAlignment = .left
        scrollView.addSubview(cambiaP)
        
        oldPPlace.frame = CGRect(x: 20 , y: cambiaP.frame.maxY + 20, width: screenWidth, height: 20)
        oldPPlace.text = "VECCHIA PASS"
        oldPPlace.textColor = UIColor.lightGray
        oldPPlace.font = UIFont(name: "HelveticaNeue", size: 15)
        oldPPlace.textAlignment = .left
        scrollView.addSubview(oldPPlace)
        
        oldP.frame = CGRect(x: screenWidth/2 , y: cambiaP.frame.maxY + 20, width: screenWidth/2 - 20, height: 20)
        oldP.text = ""
        oldP.placeholder = "Write Old Pass"
        oldP.textColor = UIColor.black
        oldP.font = UIFont(name: "HelveticaNeue", size: 15)
        oldP.textAlignment = .left
        oldP.isSecureTextEntry = true
        scrollView.addSubview(oldP)
        
        view7.frame = CGRect(x: 20, y: oldP.frame.maxY + 10, width: screenWidth - 40, height: 1)
        view7.backgroundColor = UIColor.init(red: 238/255, green: 238/255, blue: 238/255, alpha: 1)
        scrollView.addSubview(view7)
        
        newPPlace.frame = CGRect(x: 20 , y: view7.frame.maxY + 20, width: screenWidth, height: 20)
        newPPlace.text = "NUOVA PASS"
        newPPlace.textColor = UIColor.lightGray
        newPPlace.font = UIFont(name: "HelveticaNeue", size: 15)
        newPPlace.textAlignment = .left
        scrollView.addSubview(newPPlace)
        
        newP.frame = CGRect(x: screenWidth/2 , y: view7.frame.maxY + 20, width: screenWidth/2 - 20, height: 20)
        newP.text = ""
        newP.placeholder = "Write New Pass"
        newP.textColor = UIColor.black
        newP.font = UIFont(name: "HelveticaNeue", size: 15)
        newP.textAlignment = .left
        newP.isSecureTextEntry = true
        scrollView.addSubview(newP)
        
        view8.frame = CGRect(x: 20, y: newP.frame.maxY + 10, width: screenWidth - 40, height: 1)
        view8.backgroundColor = UIColor.init(red: 238/255, green: 238/255, blue: 238/255, alpha: 1)
        scrollView.addSubview(view8)
        
        cnewPlace.frame = CGRect(x: 20 , y: view8.frame.maxY + 20, width: screenWidth, height: 20)
        cnewPlace.text = "CONFERMA"
        cnewPlace.textColor = UIColor.lightGray
        cnewPlace.font = UIFont(name: "HelveticaNeue", size: 15)
        cnewPlace.textAlignment = .left
        scrollView.addSubview(cnewPlace)
        
        cnewP.frame = CGRect(x: screenWidth/2 , y: view8.frame.maxY + 20, width: screenWidth/2 - 20, height: 20)
        cnewP.text = ""
        cnewP.placeholder = "Confirm New Pass"
        cnewP.textColor = UIColor.black
        cnewP.font = UIFont(name: "HelveticaNeue", size: 15)
        cnewP.textAlignment = .left
        cnewP.isSecureTextEntry = true
        scrollView.addSubview(cnewP)
        
        view9.frame = CGRect(x: 20, y: cnewP.frame.maxY + 10, width: screenWidth - 40, height: 1)
        view9.backgroundColor = UIColor.init(red: 238/255, green: 238/255, blue: 238/255, alpha: 1)
        scrollView.addSubview(view9)
        
        
        save.frame = CGRect(x: screenSize.width / 2 - 100, y: view9.frame.maxY + 30, width: 200, height: 50)
        save.setTitle("Save New Password", for: UIControlState.normal)
        save.setTitleColor(UIColor.white, for: UIControlState.normal)
        save.backgroundColor = UIColor.init(red: 213/255, green: 190/255, blue: 111/255, alpha: 1)
        //save.addTarget(self, action: #selector(ViewController.logIn), for: UIControlEvents.touchUpInside)
        save.layer.cornerRadius = 5
        scrollView.addSubview(save)
        
        delete.frame = CGRect(x: screenSize.width / 2 - 150, y: save.frame.maxY + 30, width: 300, height: 20)
        delete.setTitle("Log Out", for: UIControlState.normal)
        delete.setTitleColor(UIColor.darkGray, for: UIControlState.normal)
        delete.backgroundColor = UIColor.white
        delete.addTarget(self, action: #selector(settingController.logout), for: UIControlEvents.touchUpInside)
        scrollView.addSubview(delete)
        
        view10.frame = CGRect(x: screenSize.width / 2 - 150, y: delete.frame.maxY + 10, width: 300, height: 1)
        view10.backgroundColor = UIColor.darkGray
        scrollView.addSubview(view10)
        */
        
        delete.frame = CGRect(x: screenSize.width / 2 - 150, y: saveSetting.frame.maxY + 30, width: 300, height: 20)
        delete.setTitle("Log Out", for: UIControlState.normal)
        delete.setTitleColor(UIColor.darkGray, for: UIControlState.normal)
        delete.backgroundColor = UIColor.white
        delete.addTarget(self, action: #selector(settingController.logout), for: UIControlEvents.touchUpInside)
        scrollView.addSubview(delete)
        scrollView.contentSize = CGSize(width: screenSize.width, height: delete.frame.maxY + 100)
        
    }
    
    func saveSettingFunc() {
        
        let text = nome.text
        let bool = text?.contains(" ")
        let bool7 = text?.contains("*")
        let bool8 = text?.contains("_")
        let bool9 = text?.contains("@")
        
        
        let cittaT = città.text
        let bool1 = cittaT?.contains("*")
        let bool2 = cittaT?.contains("_")
        let bool3 = cittaT?.contains("@")
        
        let dataDIN = dataNascita.text
        let bool4 = dataDIN?.contains("*")
        let bool5 = dataDIN?.contains("_")
        let bool6 = dataDIN?.contains("@")
        
        
        if (bool1 == true) && (bool2 == true) && (bool3 == true) && (bool4 == true) && (bool5 == true) && (bool6 == true) && (bool7 == true) && (bool8 == true) && (bool8 == true) {
        self.notificationManager.showNotification(title: "Sto salvando", body: "Le Impostazioni", onTap: nil)
        AudioServicesPlayAlertSound(SystemSoundID(kSystemSoundID_Vibrate))
        
        UIApplication.shared.statusBarStyle = .lightContent
        
        self.perform(#selector(settingController.status), with: nil, afterDelay: 3)
        
        
        let query = PFQuery(className:"_User")
        query.getObjectInBackground(withId: (PFUser.current()?.objectId)!, block: { (object, error) -> Void in
            object?.setValue(self.nome.text?.components(separatedBy: " ")[0], forKey: "name")
            object?.setValue(self.nome.text?.components(separatedBy: " ")[1], forKey: "surname")
            
            let infoOrig = (object?["info"] as! String).components(separatedBy: "*")[0]
            let info = infoOrig + "*" + self.città.text! + "*" + self.dataNascita.text! + "*" + self.telefono.text!
            object?.setValue(info, forKey: "info")
            object?.saveInBackground()
        })
            
        } else {
        
            if (bool == false) {
            
            let alertController = UIAlertController(title: "Errore", message: "Devi inserire almeno un nome e un cognome", preferredStyle: UIAlertControllerStyle.alert)
            
            let saveAction = UIAlertAction(title: "Ok", style: UIAlertActionStyle.cancel, handler: {
                alert -> Void in
                
                
            })
            
            alertController.addAction(saveAction)
            self.present(alertController, animated: true, completion: nil)
            
            } else {
                
                let alertController = UIAlertController(title: "Errore", message: "Non sono consentiti simboli", preferredStyle: UIAlertControllerStyle.alert)
                
                let saveAction = UIAlertAction(title: "Ok", style: UIAlertActionStyle.cancel, handler: {
                    alert -> Void in
                    
                    
                })
                
                alertController.addAction(saveAction)
                self.present(alertController, animated: true, completion: nil)
                
            }
        }
        
    }
    
    func status () {
        UIApplication.shared.statusBarStyle = .default
    }
    
    func sexM () {
        
        self.imageM = (UIImage(named: "male.png")?.maskWith(color: .black))!
        self.imageF = (UIImage(named: "female.png")?.maskWith(color: .lightGray))!
        self.sessoM.setImage(self.imageM, for: .normal)
        self.sessoF.setImage(self.imageF, for: .normal)
        
        let query = PFQuery(className:"_User")
        query.getObjectInBackground(withId: (PFUser.current()?.objectId)!, block: { (object, error) -> Void in
            
            var info = (object?["info"] as! String).components(separatedBy: "*")
            info[0] = "male"
            let stringa = info[0] + "*" + info[1] + "*" + info[2] + "*" + info[3]
            object?.setValue(stringa, forKey: "info")
            object?.saveInBackground()
        })
        
    }
    
    func sexF () {
        
        self.imageM = (UIImage(named: "male.png")?.maskWith(color: .lightGray))!
        self.imageF = (UIImage(named: "female.png")?.maskWith(color: .black))!
        self.sessoM.setImage(self.imageM, for: .normal)
        self.sessoF.setImage(self.imageF, for: .normal)
        
        let query = PFQuery(className:"_User")
        query.getObjectInBackground(withId: (PFUser.current()?.objectId)!, block: { (object, error) -> Void in
            
            var info = (object?["info"] as! String).components(separatedBy: "*")
            info[0] = "female"
            let stringa = info[0] + "*" + info[1] + "*" + info[2] + "*" + info[3]
            object?.setValue(stringa, forKey: "info")
            object?.saveInBackground()
        })
        
    }

    func logout() {
        
        PFUser.logOut()
        performSegue(withIdentifier: "login", sender: nil)
        
    }
    
    func notifica() {
        let vc = self.storyboard!.instantiateViewController(withIdentifier: "notificaController") as! notificaController
        self.present(vc, animated: true, completion: nil)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        UIApplication.shared.statusBarStyle = .default
    }
    
    var a = 0
    func buttFront() {
        imagePicker =  UIImagePickerController()
        imagePicker.delegate = self
        imagePicker.allowsEditing = false
        imagePicker.sourceType = .photoLibrary
        a = 1
        present(imagePicker, animated: true, completion: nil)
    }
    
    func buttBack() {
        imagePicker =  UIImagePickerController()
        imagePicker.delegate = self
        imagePicker.allowsEditing = false
        imagePicker.sourceType = .photoLibrary
        a = 2
        self.present(imagePicker, animated: true, completion: nil)
        
    }
    
    func buttFoto() {
        
        imagePicker =  UIImagePickerController()
        imagePicker.delegate = self
        imagePicker.allowsEditing = false
        imagePicker.sourceType = .photoLibrary
        a = 3
        self.present(imagePicker, animated: true, completion: nil)
    }
    
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        let chosenImage = info[UIImagePickerControllerOriginalImage] as! UIImage
        let user = PFUser.current()
        
        if (a == 1){
            
            imageFront.image = chosenImage
            let imageData = UIImagePNGRepresentation(chosenImage)
            let imageFile = PFFile(name:"back.png", data:imageData!)
            user?["docFront"] = imageFile
            user?.saveInBackground()
            
        } else if (a == 2) {
            
            imageBack.image = chosenImage
            let imageData = UIImagePNGRepresentation(chosenImage)
            let imageFile = PFFile(name:"front.png", data:imageData!)
            user?["docBack"] = imageFile
            user?.saveInBackground()
            
        } else {
            let imageTosave = cropImageToSquare(image: chosenImage)
            foto.image = imageTosave
            let imageData = UIImagePNGRepresentation(imageTosave!)
            let imageFile = PFFile(name:"profile.png", data:imageData!)
            user?["photo"] = imageFile
            user?.saveInBackground()
        }
        
        a = 0
        dismiss(animated: true, completion: nil)
    }
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        dismiss(animated: true, completion: nil)
    }
    

    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        view.endEditing(true)
    }
    
    func cropImageToSquare(image: UIImage) -> UIImage? {
        var imageHeight = image.size.height
        var imageWidth = image.size.width
        
        if imageHeight > imageWidth {
            imageHeight = imageWidth
        }
        else {
            imageWidth = imageHeight
        }
        
        let size = CGSize(width: imageWidth, height: imageHeight)
        
        let refWidth : CGFloat = CGFloat(image.cgImage!.width)
        let refHeight : CGFloat = CGFloat(image.cgImage!.height)
        
        let x = (refWidth - size.width) / 2
        let y = (refHeight - size.height) / 2
        
        let cropRect = CGRect(x: x, y: y, width: size.height, height: size.width)
        if let imageRef = image.cgImage!.cropping(to: cropRect) {
            return UIImage(cgImage: imageRef, scale: 0, orientation: image.imageOrientation)
        }
        
        return nil
    }

}

extension UIImage {
    
    func maskWith(color: UIColor) -> UIImage {
        UIGraphicsBeginImageContextWithOptions(size, false, scale)
        
        let context = UIGraphicsGetCurrentContext()!
        context.translateBy(x: 0, y: size.height)
        context.scaleBy(x: 1.0, y: -1.0)
        context.setBlendMode(.normal)
        
        let rect = CGRect(x: 0, y: 0, width: size.width, height: size.height)
        context.clip(to: rect, mask: cgImage!)
        
        color.setFill()
        context.fill(rect)
        
        let newImage = UIGraphicsGetImageFromCurrentImageContext()!
        
        UIGraphicsEndImageContext()
        
        return newImage
    }        
}
