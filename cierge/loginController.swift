//
//  loginController.swift
//  cierge
//
//  Created by Alberto Cesare Barbon on 16/01/17.
//  Copyright © 2017 Alberto Cesare Barbon. All rights reserved.
//

import UIKit
import Parse

class loginController: UIViewController, UITextFieldDelegate {
    
    
    var viewU3 : UIView = UIView()
    var textFieldCP : UITextField = UITextField()
    var confirmBt : UIButton = UIButton()
    var image : UIImageView = UIImageView()
    var scrollView : UIScrollView = UIScrollView()
    var textFieldU : UITextField = UITextField()
    var textFieldP : UITextField = UITextField()
    var activityView : UIActivityIndicatorView = UIActivityIndicatorView()
    
    var currentUser :PFUser?

    override func viewDidLoad() {
        super.viewDidLoad()
    
        
        let queryy = PFQuery(className: "Hotels")
        queryy.findObjectsInBackground { (obje, error) -> Void in
            
            var arrayHotelUserDefaults : [String] = []
            
            if (obje != nil) {
            
            for objec in obje! {
                
                
                let activation = objec["active"] as! Bool
                
                if (activation == true) {
                    
                    let name = objec["name"] as! String
                    let city = objec["city"] as! String
                    let id = objec.objectId
                    let string = name + "*" + city + "*" + id!
                    arrayHotelUserDefaults.append(string)
                }
                
            }
            }
            
            
            
            let userDefaults = Foundation.UserDefaults.standard
            userDefaults.set(arrayHotelUserDefaults, forKey: "arrayCerca")
            userDefaults.synchronize()
        }
        
        currentUser = PFUser.current()
        
        guard let realCurrentUser = self.currentUser else {
        
        scrollView.frame = CGRect(x: 0, y: 0, width: view.frame.width, height: view.frame.height)
        scrollView.contentSize = CGSize(width: view.frame.width, height: view.frame.height + image.frame.height)
        view.addSubview(scrollView)

        image.frame = CGRect(x: 0, y: 0, width: view.frame.width, height: view.frame.height / 2.5)
        image.image = UIImage(named: "Image.png")
        scrollView.addSubview(image)
        
        let logo : UIImageView = UIImageView ()
        logo.frame = CGRect(x: image.frame.width / 2 - 50, y: image.frame.height / 2 - 50, width: 100, height: 100)
        logo.image = UIImage(named: "cierge_logo.png")
        scrollView.addSubview(logo)
        
        let btLogin : UIButton = UIButton()
        btLogin.frame = CGRect(x: 40, y: image.frame.maxY - 40, width: 50, height: 20)
        btLogin.setTitle("Login", for: .normal)
        btLogin.setTitleColor(.lightGray, for: .highlighted)
        btLogin.addTarget(self, action: #selector(loginController.loginBt(_:)), for: .touchUpInside)
        scrollView.addSubview(btLogin)
        
        let btRegister : UIButton = UIButton()
        btRegister.frame = CGRect(x: image.frame.maxX - 140, y: image.frame.maxY - 40, width: 100, height: 20)
        btRegister.contentHorizontalAlignment = .right
        btRegister.setTitle("Registrati", for: .normal)
        btRegister.setTitleColor(.lightGray, for: .selected)
        btRegister.addTarget(self, action: #selector(loginController.registerBt(_:)), for: .touchUpInside)
        scrollView.addSubview(btRegister)
        
        
        textFieldU.frame = CGRect(x: 40, y: image.frame.maxY + 40, width: view.frame.width - 80, height: 20)
        textFieldU.placeholder = "Email"
        textFieldU.textColor = .black
        textFieldU.delegate = self
        textFieldU.autocapitalizationType = .none
        scrollView.addSubview(textFieldU)
        
        let viewU1 : UIView = UIView()
        viewU1.frame = CGRect(x: 40, y: image.frame.maxY + 65, width: view.frame.width - 80, height: 1)
        viewU1.backgroundColor = UIColor.init(red: 220/255, green: 220/255, blue: 220/255, alpha: 1)
        scrollView.addSubview(viewU1)
        
        
        textFieldP.frame = CGRect(x: 40, y: image.frame.maxY + 100, width: view.frame.width - 80, height: 20)
        textFieldP.placeholder = "Password"
        textFieldP.textColor = .black
        textFieldP.delegate = self
        textFieldP.isSecureTextEntry = true
        textFieldP.autocapitalizationType = .none
        scrollView.addSubview(textFieldP)
        
        let viewU2 : UIView = UIView()
        viewU2.frame = CGRect(x: 40, y: image.frame.maxY + 125, width: view.frame.width - 80, height: 1)
        viewU2.backgroundColor = UIColor.init(red: 220/255, green: 220/255, blue: 220/255, alpha: 1)
        scrollView.addSubview(viewU2)
        
        textFieldCP.frame = CGRect(x: 40, y: image.frame.maxY + 160, width: view.frame.width - 80, height: 20)
        textFieldCP.placeholder = "Conferma Password"
        textFieldCP.textColor = .black
        textFieldCP.isHidden = true
        textFieldCP.isUserInteractionEnabled = false
        textFieldCP.delegate = self
        textFieldCP.autocapitalizationType = .none
        textFieldCP.isSecureTextEntry = true
        scrollView.addSubview(textFieldCP)
        
        viewU3.frame = CGRect(x: 40, y: image.frame.maxY + 185, width: view.frame.width - 80, height: 1)
        viewU3.backgroundColor = UIColor.init(red: 220/255, green: 220/255, blue: 220/255, alpha: 1)
        viewU3.isHidden = true
        viewU3.isUserInteractionEnabled = true
        scrollView.addSubview(viewU3)
        
        confirmBt.frame = CGRect(x: view.frame.width / 2 - 75, y: image.frame.maxY + 200, width: 150, height: 40)
        confirmBt.layer.cornerRadius = 5
        confirmBt.setTitle("Login", for: .normal)
        confirmBt.setTitleColor(.white, for: .normal)
        confirmBt.addTarget(self, action: #selector(loginController.logIn(_:)), for: .touchUpInside)
        confirmBt.backgroundColor = UIColor.init(red: 213/255, green: 190/255, blue: 111/255, alpha: 1)
        scrollView.addSubview(confirmBt)
            return
        }
        
        activityView.frame = CGRect(x: view.frame.width / 2 - 10, y: confirmBt.frame.maxY + 30, width: 20, height: 20)
        activityView.hidesWhenStopped = true
        scrollView.addSubview(activityView)
        
        print("[Parse] l'utente \(realCurrentUser.username!) è ancora loggato")
        performSelector(inBackground: #selector(loginController.loginModule), with: self)
        
    }
    
    func loginBt(_ button: UIButton) {
        
        UIView.animate(withDuration: 0.5, animations: {
        self.textFieldCP.isHidden = true
        self.textFieldCP.isUserInteractionEnabled = false
        self.viewU3.isHidden = true
        self.viewU3.isUserInteractionEnabled = true
        
        self.confirmBt.frame = CGRect(x: self.view.frame.width / 2 - 75, y: self.image.frame.maxY + 200, width: 150, height: 40)
        self.confirmBt.setTitle("login", for: .normal)
        })
    }
    
    func registerBt(_ button: UIButton) {
        
        UIView.animate(withDuration: 0.5, animations: {
        self.textFieldCP.isHidden = false
        self.textFieldCP.isUserInteractionEnabled = true
        self.viewU3.isHidden = false
        self.viewU3.isUserInteractionEnabled = false
        
        self.confirmBt.frame = CGRect(x: self.view.frame.width / 2 - 75, y: self.image.frame.maxY + 230, width: 150, height: 40)
        self.confirmBt.setTitle("register", for: .normal)
        })
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        scrollView.contentOffset = CGPoint(x: 0, y: image.frame.maxY)
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        
        if (textFieldCP.isHidden == true) {
            
            if (textField == textFieldU) {
                
                self.textFieldP.becomeFirstResponder()
                
            } else if (textField == self.textFieldP) {
                
                self.textFieldP.resignFirstResponder()
                self.scrollView.contentOffset = CGPoint(x: 0, y: 0)
            }
            
        }
        
        if (textFieldCP.isHidden == false) {
            
            if (textField == self.textFieldU) {
                
                self.textFieldP.becomeFirstResponder()
                
            } else if (textField == self.textFieldP) {
                
                self.textFieldCP.becomeFirstResponder()
                
            } else if (textField == self.textFieldCP) {
                
                self.textFieldCP.resignFirstResponder()
                self.scrollView.contentOffset = CGPoint(x: 0, y: 0)
                
            }
            
        }
        
        return true
        
    }
    
    func logIn(_ button: UIButton) {
        activityView.startAnimating()
        if (textFieldCP.isHidden == true) {
            
            activityView.startAnimating()
        
        guard let username = self.textFieldU.text else { return }
        guard let password = self.textFieldP.text else { return }
        
        PFUser.logInWithUsername(inBackground: username, password: password) { (user, error) -> Void in
            if user != nil {
                self.loginModule()
                let query = PFQuery(className: "MatchingPush")
                query.findObjectsInBackground { (objects, error) -> Void in
                 
                    var a = 0
                    for object in objects! {
                        
                        if (PFInstallation.current()?.objectId == object["installationId"] as? String) {
                            object["userId"] = PFUser.current()?.objectId
                            object.saveInBackground()
                            a = a + 1
                        }
                        
                    }
                    
                    if (a == 0) {
                        
                        let book = PFObject(className:"MatchingPush")
                        book["userId"] = PFUser.current()?.objectId
                        book["installationId"] = PFInstallation.current()?.objectId
                        book.saveInBackground()
                        
                    }
                    
                }
                
                
            } else {
                print("nome o password sbagliata")
                let alertController = UIAlertController(title: "Errore", message: "Email o password errate", preferredStyle: UIAlertControllerStyle.alert)
                
                let saveAction = UIAlertAction(title: "Ok", style: UIAlertActionStyle.cancel, handler: {
                    alert -> Void in
                    
                    
                })
                
                alertController.addAction(saveAction)
                self.present(alertController, animated: true, completion: nil)
            }
        }
            
        } else {
            
            if (textFieldP.text == textFieldCP.text) {
                
                let newUser = PFUser()
               
                newUser.username = textFieldU.text
                newUser.password = textFieldP.text
                newUser.email = textFieldU.text
                //newUser["photo"] = PFFile(name: "nobody", data: UIImagePNGRepresentation(UIImage(named: "nobody.png")!)!)
                
                
                let data = UIImagePNGRepresentation(UIImage(named: "nobody.png")!)
                let imageFile = PFFile(name:"nobody.png", data:data!)
                imageFile?.saveInBackground()
                newUser.setObject(imageFile!, forKey: "photo")
                newUser.setValue("sesso*città*data di nascita*numero di telefono", forKey: "info")
                let bool = false
                newUser.setValue(bool, forKey: "emailVerified")
                newUser.setValue(true as Bool, forKey: "active")
                newUser.setValue("Nome", forKey: "name")
                newUser.setValue(textFieldU.text, forKey: "email")
                newUser.setValue("Cognome", forKey: "surname")
                
                let data1 = UIImagePNGRepresentation(UIImage(named: "Placeholder.png")!)
                let imageFile1 = PFFile(name:"Placeholder.png", data:data1!)
                imageFile1?.saveInBackground()
                newUser.setObject(imageFile1!, forKey: "docFront")
                
                let data2 = UIImagePNGRepresentation(UIImage(named: "Placeholder.png")!)
                let imageFile2 = PFFile(name:"Placeholder.png", data:data2!)
                imageFile2?.saveInBackground()
                newUser.setObject(imageFile2!, forKey: "docBack")
                
                newUser.signUpInBackground { (result, error) -> Void in
                    if error == nil {
                        print("[PARSE] SignUp successfull")
                        let query = PFQuery(className: "MatchingPush")
                        query.findObjectsInBackground { (objects, error) -> Void in
                            
                            var a = 0
                            for object in objects! {
                                
                                if (PFInstallation.current()?.objectId == object["installationId"] as? String) {
                                    object["userId"] = PFUser.current()?.objectId
                                    object.saveInBackground()
                                    a = a + 1
                                }
                                
                            }
                            
                            if (a == 0) {
                                
                                let book = PFObject(className:"MatchingPush")
                                book["userId"] = PFUser.current()?.objectId
                                book["installationId"] = PFInstallation.current()?.objectId
                                book.saveInBackground()
                            }
                            
                        }
                        let vc = self.storyboard!.instantiateViewController(withIdentifier: "tabController") as! tabController
                        self.present(vc, animated: true, completion: nil)
                        
                    } else {
                        //let error_message = error!.description
                        //print("[ERROR] SignUp with error: \(error_message)")
                    }
                }
            }
            
            
        }
        
        
    }

    func loginModule () {
        
        
        let query = PFQuery(className: "MatchingPush")
        query.findObjectsInBackground { (objects, error) -> Void in
            
            var a = 0
            for object in objects! {
                
                if (PFInstallation.current()?.objectId == object["installationId"] as? String) {
                    object["userId"] = PFUser.current()?.objectId
                    object.saveInBackground()
                    a = a + 1
                }
                
            }
            
            if (a == 0) {
                
                let book = PFObject(className:"MatchingPush")
                book["userId"] = PFUser.current()?.objectId
                book["installationId"] = PFInstallation.current()?.objectId
                book.saveInBackground()
                
            }
            
        }
        let vc = self.storyboard!.instantiateViewController(withIdentifier: "tabController") as! tabController
        self.present(vc, animated: true, completion: nil)
        
    }

}
