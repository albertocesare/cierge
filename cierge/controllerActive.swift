//
//  controllerActive.swift
//  cierge
//
//  Created by Alberto Cesare Barbon on 03/03/17.
//  Copyright © 2017 Alberto Cesare Barbon. All rights reserved.
//

import UIKit
import Parse
import LNRSimpleNotifications
import AudioToolbox

class controllerActive: UIViewController, UITableViewDelegate, UITableViewDataSource {
    
    var tableView = UITableView()
    var array : [PFObject] = []
    let notificationManager = LNRNotificationManager()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let swipeUp = UISwipeGestureRecognizer(target: self, action: #selector(self.respondToSwipeGesture))
        swipeUp.direction = UISwipeGestureRecognizerDirection.down
        self.view.addGestureRecognizer(swipeUp)
        
        NotificationCenter.default.addObserver(self, selector: #selector(controllerActive.query), name: NSNotification.Name.init(rawValue: "refreshAP"), object: nil)
        
        notificationManager.notificationsPosition = LNRNotificationPosition.top
        notificationManager.notificationsBackgroundColor = UIColor.init(red: 205/255, green: 89/255, blue: 91/255, alpha: 1)
        notificationManager.notificationsTitleTextColor = UIColor.white
        notificationManager.notificationsBodyTextColor = UIColor.white
        notificationManager.notificationsSeperatorColor = UIColor.clear

        tableView.delegate = self
        tableView.dataSource = self
        tableView.frame = view.frame
        tableView.refreshControl?.addTarget(self, action: #selector(controllerActive.refresh), for: UIControlEvents.valueChanged)
        self.tableView.tableFooterView = UIView()
        view.addSubview(tableView)
        array = []
        query()
        
    }
    
    func respondToSwipeGesture(gesture: UIGestureRecognizer) {
        
        if let swipeGesture = gesture as? UISwipeGestureRecognizer {
            switch swipeGesture.direction {
                
            case UISwipeGestureRecognizerDirection.down:
                query()
                break
            default:
                break
            }
        }
    }
    
    func refresh() {
        if (self.tableView.refreshControl?.isRefreshing)! {
            //query()
        }
    }
    
    /*func scrollViewDidScroll(_ scrollView: UIScrollView) {
        
        if (scrollView.contentOffset.y < -10) {
            tableView.isUserInteractionEnabled = false
            query()
            self.perform(#selector(controllerActive.baba), with: nil, afterDelay: 1)
        }
        
        
    }
    
    func baba () {
        tableView.isUserInteractionEnabled = true
    }*/
    
    func query () {
        array = []
        let userId = PFUser.current()?.objectId
        let query = PFQuery(className: "UserCheckin")
        //query.cachePolicy = .cacheThenNetwork
        query.whereKey("userId", equalTo: userId!)
        query.whereKey("state", equalTo: "a")
        
        query.findObjectsInBackground { (objects, error) -> Void in
            
            self.array = []
            for object in objects! {
                
                
                
                self.array.append(object)
                if objects?.count == 0 {
                    
                    let textView : UITextView = UITextView()
                    textView.frame = CGRect(x: 20, y: self.view.frame.height / 2 - 50, width: self.view.frame.width - 40, height: 100)
                    textView.textAlignment = .center
                    textView.text = "In questa pagina visualizzerai i tuoi check-in attivi. Non hai ancora nessuna prenotazione attiva, cerca l'albergo dove fare il check in."
                    textView.font = UIFont.systemFont(ofSize: 15)
                    textView.textColor = .darkGray
                    self.view.addSubview(textView)
                }
                
            }
            self.tableView.reloadData()
            
        }
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = UITableViewCell()
        cell.accessoryType = .disclosureIndicator
        
        let random = arc4random_uniform(5)
        var color = UIColor()
        switch random {
        case 0:
            
            color = UIColor.init(red: 213/255, green: 190/255, blue: 111/255, alpha: 1)
            
            break
        case 1:
            
            color = UIColor.init(red: 43/255, green: 152/255, blue: 240/255, alpha: 1)
            
            break
        case 2:
            
            color = UIColor.init(red: 80/255, green: 174/255, blue: 85/255, alpha: 1)
            
            break
        case 3:
            
            color = UIColor.init(red: 253/255, green: 192/255, blue: 47/255, alpha: 1)
            
            break
        case 4:
            
            color = UIColor.init(red: 21/255, green: 149/255, blue: 136/255, alpha: 1)
            
            break
        case 5:
            
            color = UIColor.init(red: 241/255, green: 69/255, blue: 51/255, alpha: 1)
            
            break
        default:
            break
        }
        
        let buttonIn = UIButton(frame: CGRect(x: 15, y: 10, width: 50, height: 50))
        buttonIn.layer.cornerRadius = 25
        buttonIn.isUserInteractionEnabled = false
        buttonIn.clipsToBounds = true
        buttonIn.backgroundColor = #colorLiteral(red: 0.9019607843, green: 0.7450980392, blue: 0.4352941176, alpha: 1)
        buttonIn.setTitleColor(.white, for: .normal)
        cell.addSubview(buttonIn)
        
        
        let pfObj = array[indexPath.row]
        
        let title = UILabel(frame: CGRect(x: 80, y: cell.frame.height/2 - 10, width: cell.frame.width - 60, height: 20))
        let hotelId = pfObj["hotelId"]
        let query = PFQuery(className: "Hotels")
        query.getObjectInBackground(withId: hotelId as! String, block: { (object, error) -> Void in
            
            let name = object?["name"] as! String
            title.text = name
            cell.addSubview(title)
            
            let arrname = name.components(separatedBy: " ")
            let string1 = arrname[0]
            let string2 = arrname[1]
            let ini1 = string1.characters.first
            let ini2 = string2.characters.first
            let stringa = String(describing: ini1!) + String(describing: ini2!)
            
            buttonIn.setTitle(stringa, for: .normal)
            
            
        })
        
        
        let checkin = pfObj["checkIndate"] as! String
        let checkout = pfObj["checkOutDate"] as! String
        
        let formatter = DateFormatter()
        formatter.dateFormat = "dd-MM-yyyy"
        let checkindate1 = formatter.date(from: checkin)
        let checkoutdate1 = formatter.date(from: checkout)
        
        let formattazione = DateFormatter()
        formattazione.dateFormat = "dd MMM yyyy"
        let checkindate2 = formattazione.string(from: checkindate1!)
        let checkoutdate2 = formattazione.string(from: checkoutdate1!)
        
        let labelDate = UILabel(frame: CGRect(x: 80, y: title.frame.maxY + 10, width: cell.frame.width - 60, height: 20))
        labelDate.text = checkindate2 + " / " + checkoutdate2
        labelDate.textColor = .gray
        labelDate.font = UIFont.systemFont(ofSize: 14)
        
        cell.addSubview(labelDate)
        
        
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return array.count
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 70
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        query()
        self.tableView.reloadData()
        self.tableView.tableFooterView = UIView()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        query()
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let userDefaults = Foundation.UserDefaults.standard
        userDefaults.set("0", forKey: "numTemplate")
        
        let pfObj = array[indexPath.row]
        let idC = array[indexPath.row].objectId
        let id = pfObj["hotelId"] as! String
        
        
        let query = PFQuery(className:"Hotels")
        query.getObjectInBackground(withId: id, block: { (object, error) -> Void in
            
            let stato = pfObj["state"] as! String
            
            userDefaults.set(object?.objectId, forKey: "id")
            userDefaults.set(stato, forKey: "stato")
            
            
            print(stato)
            
        })
        
        userDefaults.set(idC, forKey:"idCheck")
        
        userDefaults.set( id, forKey: "id")
        self.performSegue(withIdentifier: "mainSegue", sender: self)
        
    }
    
    func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        return true
    }
    
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
        
        let id = array[indexPath.row].objectId!
        print(id)
        
        notificationManager.showNotification(title: "Cancellazione...", body: "Richiesta Inviata", onTap: nil)
        AudioServicesPlayAlertSound(SystemSoundID(kSystemSoundID_Vibrate))
        
        let query = PFQuery(className:"UserCheckin")
        query.getObjectInBackground(withId: id, block: { (object, error) -> Void in
            
            self.array.remove(at: indexPath.row)
            object?.deleteEventually()
            tableView.reloadData()
            
        })
        
    }
}
