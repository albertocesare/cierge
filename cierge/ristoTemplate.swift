//
//  ristoTemplate.swift
//  cierge
//
//  Created by Alberto Cesare Barbon on 19/01/17.
//  Copyright © 2017 Alberto Cesare Barbon. All rights reserved.
//

import UIKit
import Parse
import LNRSimpleNotifications
import AudioToolbox

class ristoTemplate: UIViewController , UIScrollViewDelegate {

    var image : UIImageView = UIImageView()
    var sottotitolo : UILabel = UILabel()
    var sottotitolo1 : UILabel = UILabel()
    var titolo : UILabel = UILabel()
    var openAt : UILabel = UILabel()
    var descri : UITextView = UITextView()
    var speciality : UILabel = UILabel()
    var range : UILabel = UILabel()
    var payBt : UIButton = UIButton()
    
    var arrayOre : [String] = []
    var arrayTurni : [String] = []
    
    var datePicker : UIDatePicker = UIDatePicker()
    
    var scrollView : UIScrollView = UIScrollView()
    
    let notificationManager = LNRNotificationManager()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        scrollView.delegate = self
        
        NotificationCenter.default.addObserver(self, selector: #selector(checkTemplate1.pop2), name: NSNotification.Name.init(rawValue: "cambia"), object: nil)
        
        notificationManager.notificationsPosition = LNRNotificationPosition.top
        notificationManager.notificationsBackgroundColor = UIColor.init(red: 109/255, green: 213/255, blue: 154/255, alpha: 1)
        notificationManager.notificationsTitleTextColor = UIColor.white
        notificationManager.notificationsBodyTextColor = UIColor.white
        notificationManager.notificationsSeperatorColor = UIColor.clear
        
        view.frame = CGRect(x: 0, y: 0, width: view.frame.width, height: view.frame.height - (110))
        scrollView.frame = view.frame
        
        image.frame = CGRect(x: 0, y: 0, width: view.frame.width, height: 200)
        //view.addSubview(image)
        scrollView.addSubview(image)
        
        let userDefaults = Foundation.UserDefaults.standard
        let value  = userDefaults.string(forKey: "idService")
        let query = PFQuery(className:"InServices")
        query.getObjectInBackground(withId: value!, block: { (object, error) -> Void in
            
            let foto = object?["photo"] as! PFFile
            foto.getDataInBackground { (imageData, error) -> Void in
                if (error == nil) {
                    if (imageData != nil) {
                        self.image.image = UIImage(data:imageData!)
                    }
                }
            }
            
            let inutile0 = object?["info"] as! String
            let inutile1 = inutile0.components(separatedBy: "@")
            let infosArr = inutile1[1]
            let infos = infosArr.components(separatedBy: "*")
            let nome = infos[0]
            self.titolo.text = nome
            
            let sotto = infos[1]
            self.sottotitolo.text = sotto
            self.sottotitolo1.text = sotto
            
            let descr = infos[2]
            self.descri.text = descr
            
            let turniarrdainfo = infos[3]
            let turniarr = turniarrdainfo.components(separatedBy: "_")
            self.arrayOre = turniarr
            if (turniarr[0] == "1") {
                //1 turno
                
                let turnoOpn = turniarr[1]
                let turnoCls = turniarr[2]
                
                let string = turnoOpn + turnoCls
                
                self.arrayTurni.append(string)
                
                self.openAt.text = "Apertura: " + turnoOpn + " - " + turnoCls
                
            } else {
                
                var turnoOpn = turniarr[1]
                var turnoCls = turniarr[2]
                self.openAt.text = "Apertura: " + turnoOpn + " - " + turnoCls
                
                turnoOpn = turniarr[3]
                turnoCls = turniarr[4]
                self.openAt.text = self.openAt.text! + " | " + turnoOpn + " - " + turnoCls
            }
            
            let specialiti = infos[5]
            self.speciality.text = specialiti
            
            self.range.text = infos[6] + " €"
            
        })
        
        sottotitolo1.frame = CGRect(x: 20, y: image.frame.maxY + 20, width: view.frame.width - 40, height: 20)
        sottotitolo1.textColor = UIColor.lightGray
        sottotitolo1.font = UIFont.systemFont(ofSize: 18)
        sottotitolo1.textAlignment = .center
        scrollView.addSubview(sottotitolo1)
        
        titolo.frame = CGRect(x: 20, y: sottotitolo1.frame.maxY + 20, width: view.frame.width - 40, height: 35)
        titolo.font = UIFont.systemFont(ofSize: 30)
        titolo.textColor = .black
        titolo.textAlignment = .center
        scrollView.addSubview(titolo)
        
        let viewU1 : UIView = UIView(frame: CGRect(x: 20, y: titolo.frame.maxY + 20, width: view.frame.width - 40, height: 1))
        viewU1.backgroundColor = UIColor.init(red: 239/255, green: 239/255, blue: 239/255, alpha: 1)
        scrollView.addSubview(viewU1)
        
        openAt.frame = CGRect(x: 40, y: viewU1.frame.maxY + 15, width: view.frame.width - 70, height: 20)
        openAt.textColor = .lightGray
        openAt.font = UIFont.systemFont(ofSize: 14)
        scrollView.addSubview(openAt)
        let imageOrologio : UIImageView = UIImageView(image: UIImage(named: "time.png"))
        imageOrologio.frame = CGRect(x: 20, y: viewU1.frame.maxY + 17.5, width: 15, height: 15)
        scrollView.addSubview(imageOrologio)
        
        let imagespeci : UIImageView = UIImageView(image: UIImage(named: "Star.png"))
        imagespeci.frame = CGRect(x: 20, y: openAt.frame.maxY + 15, width: 15, height: 15)
        scrollView.addSubview(imagespeci)
        speciality.frame = CGRect(x: 40, y: imagespeci.frame.minY - 2.5, width: view.frame.width - 70, height: 20)
        speciality.textColor = .lightGray
        speciality.font = UIFont.systemFont(ofSize: 14)
        scrollView.addSubview(speciality)
        
        let imagerange : UIImageView = UIImageView(image: UIImage(named: "coin"))
        imagerange.frame = CGRect(x: 20, y: speciality.frame.maxY + 15, width: 15, height: 15)
        scrollView.addSubview(imagerange)
        range.frame = CGRect(x: 40, y: imagerange.frame.minY - 2.5, width: view.frame.width - 70, height: 20)
        range.textColor = .lightGray
        range.font = UIFont.systemFont(ofSize: 14)
        scrollView.addSubview(range)
        
        let viewU2 : UIView = UIView(frame: CGRect(x: 20, y: imagerange.frame.maxY + 15, width: view.frame.width - 40, height: 1))
        viewU2.backgroundColor = UIColor.init(red: 239/255, green: 239/255, blue: 239/255, alpha: 1)
        scrollView.addSubview(viewU2)
        
        descri.frame = CGRect(
            x: 20, y: viewU2.frame.maxY + 15, width: view.frame.width - 40, height: 250)
        descri.isEditable = false
        descri.textColor = .lightGray
        descri.backgroundColor = .gray
        descri.font = UIFont.systemFont(ofSize: 15)
        descri.backgroundColor = .white
        scrollView.addSubview(descri)
        
        scrollView.contentSize = CGSize(width: view.frame.width, height: descri.frame.maxY + 70)
        view.addSubview(scrollView)
        
        /*let blur : UIVisualEffectView = UIVisualEffectView(effect: UIBlurEffect(style: .dark))
        blur.frame = CGRect(x: 0, y: 0, width: view.frame.width, height: 64)
        view.addSubview(blur)
        
        let disBt = UIButton() as UIButton
        disBt.tintColor = UIColor.white
        disBt.frame = CGRect(x: 0, y: 20, width: 40, height: 40)
        let imageM = UIImage(named: "back.png")?.maskWith(color: .white)
        disBt.setImage(imageM, for: .normal)
        disBt.addTarget(self, action: #selector(ristoTemplate.dismiss(_:)), for: .touchUpInside)
        blur.addSubview(disBt)
        sottotitolo.frame = CGRect(x: 20, y: 35, width: view.frame.width - 40, height: 20)
        sottotitolo.textAlignment = .center
        sottotitolo.textColor = .white
        view.addSubview(sottotitolo)*/
        
        payBt.frame = CGRect(x: view.frame.width - 60, y: view.frame.height - 60, width: 40, height: 40)
        payBt.layer.cornerRadius = 20
        payBt.backgroundColor = UIColor.init(red: 213/255, green: 190/255, blue: 111/255, alpha: 1)
        payBt.setImage(UIImage(named: "piu.png"), for: .normal)
        payBt.addTarget(self, action: #selector(ristoTemplate.pay(_:)), for: .touchUpInside)
        view.addSubview(payBt)
        
        let btBack : UIButton = UIButton(frame: CGRect(x: 5, y: scrollView.frame.maxY - 50, width: 150, height: 30))
        btBack.setImage(UIImage(named:"cierge-trasp-1.png"), for: .normal)
        btBack.layer.cornerRadius = 15
        btBack.clipsToBounds = true
        btBack.setTitle("Indietro", for: .normal)
        btBack.setTitleColor(.white, for: .normal)
        btBack.contentHorizontalAlignment = .left
        btBack.addTarget(self, action: #selector(ristoTemplate.pop(_:)), for: .touchUpInside)
        //view.addSubview(btBack)
        let labelBack : UILabel = UILabel(frame: CGRect(x: 15, y: scrollView.frame.maxY - 50 + 15 - 10, width: 40, height: 20))
        labelBack.text = "Indietro"
        labelBack.textColor = .white
        labelBack.textAlignment = .center
        //view.addSubview(labelBack)
    }
    
    func pop(_ button: UIButton) {
         _ = navigationController?.popViewController(animated: true)
    }
    
    func pop2() {
        _ = navigationController?.popViewController(animated: true)
    }
    
    func dismiss(_ sender: UIButton) {
        let id = "1"
        let userDefaults = Foundation.UserDefaults.standard
        userDefaults.set( id, forKey: "numTemplate")
        let vc = self.storyboard!.instantiateViewController(withIdentifier: "hotelController") as! hotelController
        self.present(vc, animated: true, completion: nil)
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        if scrollView.contentOffset.y < -30 {
            self.navigationController?.popToRootViewController(animated: true)
        }
    }
    
    func pay(_ button: UIButton) {
        
        
        let userDefaults = Foundation.UserDefaults.standard
        let value  = userDefaults.string(forKey: "idService")
        let idc  = userDefaults.string(forKey: "idCheck")
        
        var checkInDate = ""
        var checkOutDate = ""
        var hotelId = ""
        var query = PFQuery(className:"UserCheckin")
        query.getObjectInBackground(withId: idc!, block: { (object, error) -> Void in
            
            checkInDate = object?["checkIndate"] as! String
            checkOutDate = object?["checkOutDate"] as! String
            
            query = PFQuery(className:"InServices")
            query.getObjectInBackground(withId: value!, block: { (object, error) -> Void in
                
                
                let dateFormatter = DateFormatter()
                dateFormatter.dateFormat = "dd-MM-yyyy HH:mm"
                let arrDataIn = checkInDate.components(separatedBy: "/")
                let arrDataOut = checkOutDate.components(separatedBy: "/")
                
                let giornoIn = arrDataIn[0]
                let meseIn = arrDataIn[1]
                let annoIn = arrDataIn[2]
                let giornoOut = arrDataOut[0]
                let meseOut = arrDataOut[1]
                let annoOut = arrDataOut[2]
                var oraOpen = ""
                var oraClose = ""
                
                hotelId = object?["hotelId"] as! String
                self.arrayOre = (object?["info"] as! String).components(separatedBy: "*")[3].components(separatedBy: "_")
                
                
                if (self.arrayOre[0] == "1") {
                    
                    oraOpen = self.arrayOre[1]
                    oraClose = self.arrayOre[2]
                    
                    
                } else {
                    
                    oraOpen = self.arrayOre[1]
                    oraClose = self.arrayOre[4]
                    
                }
                let dataIn = giornoIn + "-" + meseIn + "-" + annoIn + " " + oraOpen
                let dataOut = giornoOut + "-" + meseOut + "-" + annoOut + " " + oraClose
                let newdataIn = dateFormatter.date(from: dataIn)
                let newdataOut = dateFormatter.date(from: dataOut)
                self.datePicker.minimumDate = newdataIn
                self.datePicker.maximumDate = newdataOut
                
                
                
            })
            
        })
        
        
        
            let alertController = UIAlertController(title: "Selezione Data e Ospiti", message:"\n\n\n\n\n\n" , preferredStyle: UIAlertControllerStyle.alert)
        
        
            alertController.view.frame = CGRect(x: 0, y: 0, width: 300, height: 200)
            alertController.view.bounds = CGRect(x: 0, y: 0, width: 300, height: 200)
            self.datePicker.frame = CGRect(x: 5, y: 50, width: 260, height: 100)
            alertController.view.addSubview(self.datePicker)
            
            let cancelAction = UIAlertAction(title: "Cancella", style: .destructive) { (action) in
                
            }
            
            let confirmAction = UIAlertAction(title: "Conferma", style: UIAlertActionStyle.cancel, handler: {
                (action : UIAlertAction!) -> Void in
                
                let dateFormatter = DateFormatter()
                dateFormatter.dateFormat = "dd-MM-yyyy HH:mm"
                let selectedDate = dateFormatter.string(from: self.datePicker.date)
                print(selectedDate)
                let book = PFObject(className:"InBooking")
                book["userId"] = PFUser.current()?.objectId
                let stringa = selectedDate.replacingOccurrences(of: "-", with: "/")
                book["date"] = stringa.replacingOccurrences(of: " ", with: "*")
                book["hotelId"] = hotelId
                book["serviceId"] = userDefaults.string(forKey: "idService")!
                book["state"] = "p"
                let firstTextField = alertController.textFields![0] as UITextField
                let stri = "3@" + firstTextField.text!
                
                book["subServices"] = stri
                book.saveInBackground()
                
                self.notificationManager.showNotification(title: "Congratulazioni", body: "Servizio Richiesto", onTap: nil)
                AudioServicesPlayAlertSound(SystemSoundID(kSystemSoundID_Vibrate))
            })
            
            alertController.addTextField { (textField : UITextField!) -> Void in
                textField.text = "1"
                textField.textAlignment = .center
                //textField.backgroundColor = UIColor.init(red: 239/255, green: 239/255, blue: 239/255, alpha: 1)
                textField.layer.cornerRadius = 5
                textField.borderStyle = .none
                textField.layer.borderWidth = 0
                textField.keyboardType = .numberPad
            }
            
            
            alertController.addAction(cancelAction)
            alertController.addAction(confirmAction)
            self.present(alertController, animated: true, completion: nil)
            
        }
        
    
    
    
}
