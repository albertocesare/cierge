//
//  activityPage.swift
//  cierge
//
//  Created by Alberto Cesare Barbon on 16/01/17.
//  Copyright © 2017 Alberto Cesare Barbon. All rights reserved.
//

import UIKit
import Parse

class activityPage: UIViewController, UITableViewDelegate, UITableViewDataSource {
    
    var tableView : UITableView = UITableView()
    var arrayPFObj : [PFObject] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        tableView.frame = CGRect(x: 0, y: 0, width: view.frame.width, height: view.frame.height - 104)
        tableView.delegate = self
        tableView.dataSource = self
        self.tableView.register(UINib(nibName: "TableViewCell", bundle: nil), forCellReuseIdentifier: "cell")
        view.addSubview(tableView)
        
        let userDefaults = Foundation.UserDefaults.standard
        let value  = userDefaults.string(forKey: "id")
        
        let query = PFQuery(className:"OutServices")
        query.findObjectsInBackground { (objects, error) -> Void in
            
            for object in objects!
            {
                let servizio = object["hotelId"] as! String
                let tipo = object["type"] as! String
                
                if (servizio == value!) {
                    if (tipo == "a") {
                    
                        
                    let idAct = object["serviceId"] as! String
                        
                        let query1 = PFQuery(className:"Activities")
                        query1.getObjectInBackground(withId: idAct, block: { (objectA, error) -> Void in
                            
                            self.arrayPFObj.append(objectA!)
                            print(objectA!)
                            self.tableView.reloadData()
                            
                        })
                        
                    }
                }
                
            }
        }
        
    }
    
    internal func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! TableViewCell
        let pfObj = arrayPFObj[indexPath.row]
        let nome = pfObj["name"] as! String
        
        cell.nomeLabel.text = nome
        
        
        
        let objid = pfObj.objectId
        let query = PFQuery(className:"Activities")
        query.getObjectInBackground(withId: objid!, block: { (object, error) -> Void in
            
            let foto = object?["photo"] as! PFFile
            foto.getDataInBackground { (imageData, error) -> Void in
                if (error == nil) {
                    if (imageData != nil) {
                        cell.imgView.image = UIImage(data:imageData!)
                    }
                }
            }
            
        })
        
        return cell
        
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        print(arrayPFObj)
        return arrayPFObj.count
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let pfObj = arrayPFObj[indexPath.row]
        let userDefaults = Foundation.UserDefaults.standard
        userDefaults.set( pfObj.objectId, forKey: "idService")
        
        let query = PFQuery(className:"Activities")
        query.getObjectInBackground(withId: pfObj.objectId!, block: { (object, error) -> Void in
            
            let info = object?["info"] as! String
            let num = (info.components(separatedBy: "*"))[0]
            
            let userDefaults = Foundation.UserDefaults.standard
            let value  = userDefaults.string(forKey: "id")
            let userId = PFUser.current()?.objectId
            
            let query = PFQuery(className: "UserCheckin")
            query.findObjectsInBackground { (objects, error) -> Void in
                
                for object in objects! {
                    if (object["hotelId"] as? String == value) && (object["state"] as! String == "a") && (object["userId"] as? String == userId) {
            
            switch num {
            case "2":
                let vc = self.storyboard!.instantiateViewController(withIdentifier: "actListTemplate") as! actListTemplate
                self.present(vc, animated: true, completion: nil)
                
                
                break
                
            case "1":
                let vc = self.storyboard!.instantiateViewController(withIdentifier: "actCheckTemplate") as! actCheckTemplate1
                self.present(vc, animated: true, completion: nil)
                
                break
            
            default:
                break
            }
                        
                     break
                    }
                }
            }
            
        })
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 220
    }
    
}
