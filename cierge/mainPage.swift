//
//  mainPage.swift
//  cierge
//
//  Created by Alberto Cesare Barbon on 17/01/17.
//  Copyright © 2017 Alberto Cesare Barbon. All rights reserved.
//

import UIKit
import Parse
import MapKit
import LNRSimpleNotifications
import AudioToolbox

class mainPage: UIViewController, MKMapViewDelegate {
    
    var scrollView : UIScrollView = UIScrollView()
    var foto : UIImageView = UIImageView()
    var titolo : UILabel = UILabel()
    var indirizzo : UILabel = UILabel()
    var textDescrizione : UITextView = UITextView()
    var mapView : MKMapView = MKMapView()
    var bt : UIButton = UIButton()
    
    var phoneNumber = ""
    var coordsBt = ""
    var titoloPin = ""
    let notificationManager = LNRNotificationManager()
    var arrayidRist : [String] = []
    var arrayidAct : [String] = []
    var arrayidPlac : [String] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        let userDefaults = Foundation.UserDefaults.standard
        let value  = userDefaults.string(forKey: "id")
        
        let queryy = PFQuery(className: "OutServices")
        queryy.cachePolicy = .cacheElseNetwork
        queryy.whereKey("hotelId", equalTo: value!)
        queryy.whereKey("type", equalTo: "r")
        queryy.findObjectsInBackground()
            {(objects, error) in
                
                for object in objects! {
                    self.arrayidRist.append(object["serviceId"] as! String)
                    //print(self.arrayid)
                }
                userDefaults.setValue(self.arrayidRist, forKey: "arrayRist")
                print(self.arrayidRist)
        }
        
        let queryyy = PFQuery(className: "OutServices")
        queryyy.whereKey("type", equalTo: "a")
        queryyy.whereKey("hotelId", equalTo: value!)
        queryyy.findObjectsInBackground()
            {(objects, error) in
                
                for object in objects! {
                    self.arrayidAct.append(object["serviceId"] as! String)
                    print(self.arrayidAct)
                }
                userDefaults.setValue(self.arrayidAct, forKey: "arrayAct")
                print(self.arrayidAct)
                
                let quer = PFQuery(className: "OutServices")
                quer.whereKey("type", equalTo: "p")
                queryyy.whereKey("hotelId", equalTo: value!)
                quer.findObjectsInBackground()
                    {(objects, error) in
                        
                        for object in objects! {
                            self.arrayidPlac.append(object["serviceId"] as! String)
                            //print(self.arrayid)
                        }
                        userDefaults.setValue(self.arrayidPlac, forKey: "arrayPlac")
                        print(self.arrayidPlac)
                }
        }
        
        
        
        
        
        
        
        
        
        
        
        
        mapView.delegate = self

        scrollView.frame = CGRect(x: 0, y: 0, width: view.frame.width, height: view.frame.height - 64)
        
        view.addSubview(scrollView)
        
        
        
        
        
        
        let query = PFQuery(className:"Hotels")
        query.getObjectInBackground(withId: value!, block: { (object, error) -> Void in
            
            if (object?["photo"] as? PFFile != nil) {
                let foto = object?["photo"] as! PFFile
                foto.getDataInBackground { (imageData, error) -> Void in
                    if (error == nil) {
                        if (imageData != nil) {
                            self.foto.image = UIImage(data:imageData!)
                        }
                    }
                }
            } else {
                self.foto.backgroundColor = .lightGray
                
            }
            
            
            
            let nome = object?["name"] as! String
            self.titoloPin = object?["name"] as! String
            self.titolo.text = nome
            let add = object?["address"] as! String
            let city = object?["city"] as! String
            self.indirizzo.text = add + ", " + city
            let descrizione = object?["description"] as! String
            self.textDescrizione.text = descrizione
            
            self.phoneNumber = object?["phone"] as! String
            self.coordsBt = (object?["coords"] as! String).replacingOccurrences(of: "_", with: ",")
            print(self.coordsBt)
            
            
        })
        
        foto.frame = CGRect(x: 0, y: 0, width: scrollView.frame.width, height: 200)
        foto.backgroundColor = .lightGray
        scrollView.addSubview(foto)
        
        titolo.frame = CGRect(x: 20, y: foto.frame.maxY + 20, width: scrollView.frame.width - 40, height: 25)
        titolo.font = UIFont.systemFont(ofSize: 30)
        scrollView.addSubview(titolo)
        
        let imgPos : UIImageView = UIImageView()
        imgPos.image = UIImage(named: "place.png")
        imgPos.frame = CGRect(x: 20, y: titolo.frame.maxY + 17, width: 10, height: 15)
        scrollView.addSubview(imgPos)
        
        indirizzo.frame = CGRect(x: 40, y: titolo.frame.maxY + 15, width: scrollView.frame.width - 60, height: 20)
        indirizzo.textColor = .gray
        scrollView.addSubview(indirizzo)
        
        
        bt.frame = CGRect(x: 20, y: indirizzo.frame.maxY + 20, width: scrollView.frame.width - 40, height: 50)
        bt.layer.cornerRadius = 5
        bt.setTitle("Invia Richiesta di check-in", for: .normal)
        bt.addTarget(self, action: #selector(mainPage.send(_:)), for: .touchUpInside)///////////////////
        bt.backgroundColor = UIColor.init(red: 213/255, green: 190/255, blue: 111/255, alpha: 1)
        
        let value3  = userDefaults.string(forKey: "idCheck")
        if (value3 != "none") {
            bt.backgroundColor = UIColor.init(red: 109/255, green: 213/255, blue: 154/255, alpha: 1)
            bt.setTitle("Check in attivo per questo Hotel", for: .normal)
            bt.isUserInteractionEnabled = false
        }
        
        scrollView.addSubview(bt)
        
        textDescrizione.frame = CGRect(x: 20, y: bt.frame.maxY + 20, width: view.frame.width - 40, height: 300)
        textDescrizione.isEditable = false
        textDescrizione.textColor = .gray
        textDescrizione.font = UIFont.systemFont(ofSize: 15)
        scrollView.addSubview(textDescrizione)
        
        let phoneBtn : UIButton = UIButton()
        
        phoneBtn.addTarget(self, action: #selector(mainPage.phone(_:)), for: .touchUpInside)
        phoneBtn.frame = CGRect(x: scrollView.frame.width - 120, y: scrollView.frame.height - 100, width: 40, height: 40)
        let imagePhone : UIImage = UIImage(named: "phoneBtn.png")!
        phoneBtn.setImage(imagePhone, for: .normal)

        view.addSubview(phoneBtn)
        
        let locationBtn : UIButton = UIButton()
        locationBtn.addTarget(self, action: #selector(mainPage.loca(_:)), for: .touchUpInside)
        locationBtn.frame = CGRect(x: scrollView.frame.width - 60, y: scrollView.frame.height - 100, width: 40, height: 40)
        let imageLocation : UIImage = UIImage(named: "locationBtn.png")!
        locationBtn.setImage(imageLocation, for: .normal)
        view.addSubview(locationBtn)
        
        mapView.frame = CGRect(x: 0, y: textDescrizione.frame.maxY, width: scrollView.frame.width, height: 300)
        mapView.isUserInteractionEnabled = false
        scrollView.addSubview(mapView)
        
        scrollView.contentSize = CGSize(width: scrollView.frame.width, height: mapView.frame.maxY + 20)
        
        
        
        notificationManager.notificationsPosition = LNRNotificationPosition.top
        notificationManager.notificationsBackgroundColor = UIColor.init(red: 109/255, green: 213/255, blue: 154/255, alpha: 1)
        notificationManager.notificationsTitleTextColor = UIColor.white
        notificationManager.notificationsBodyTextColor = UIColor.white
        notificationManager.notificationsSeperatorColor = UIColor.clear
        
        /*let alertSoundURL: NSURL? = Bundle.main.url(forResource: "click", withExtension: "wav") as NSURL?
        if let _ = alertSoundURL {
            var mySound: SystemSoundID = 0
            AudioServicesCreateSystemSoundID(alertSoundURL!, &mySound)
            notificationManager.notificationSound = mySound
        }*/
        
        
        
    }
    
    func mapViewDidFinishLoadingMap(_ mapView: MKMapView) {
        
        let userDefaults = Foundation.UserDefaults.standard
        let value  = userDefaults.string(forKey: "id")
        
        let query = PFQuery(className:"Hotels")
        query.getObjectInBackground(withId: value!, block: { (object, error) -> Void in
        
            let coords = object?["coords"] as! String
            let arraycoords = coords.components(separatedBy: "_")
            let latitudine = Double(arraycoords[0])
            let longitudine = Double(arraycoords[1])
            
            let dropPin = MKPointAnnotation()
            let coordinate2d : CLLocationCoordinate2D = CLLocationCoordinate2D(latitude: latitudine!, longitude: longitudine!)
            dropPin.coordinate = coordinate2d
            self.mapView.addAnnotation(dropPin)
            let regione = MKCoordinateRegion.init(center: coordinate2d, span: MKCoordinateSpan(latitudeDelta: 0.1, longitudeDelta: 0.1))
            self.mapView.setRegion(regione, animated: true)
            
        })
        
    }
    
    func loca(_ sender: UIButton) {
        
        
        UIApplication.shared.open(NSURL(string:"http://maps.apple.com/?ll=" + coordsBt + "&q=Hotel")! as URL, options: [:], completionHandler: nil)
        
    }
    
    func notitica() {
        NotificationCenter.default.post(name: NSNotification.Name(rawValue: "refreshAP"), object: nil, userInfo: nil)
    }

    func send(_ sender: UIButton) {
        
        
        self.perform(#selector(mainPage.notitica), with: nil, afterDelay: 2)
        
        
        notificationManager.showNotification(title: "Congratulazione!", body: "Richiesta di check-in inviata", onTap: nil)
        AudioServicesPlayAlertSound(SystemSoundID(kSystemSoundID_Vibrate))
        let userDefaults = Foundation.UserDefaults.standard
        let value  = userDefaults.string(forKey: "id")
        
        let pfObj = PFObject(className: "UserCheckin")
        pfObj["userId"] = PFUser.current()?.objectId
        pfObj["hotelId"] = value
        pfObj["state"] = "p"
        pfObj.saveInBackground()
        self.bt.backgroundColor = UIColor.init(red: 109/255, green: 231/255, blue: 154/255, alpha: 1)
        UIView.animate(withDuration: 2, animations: {
        
          self.bt.backgroundColor = UIColor.init(red: 213/255, green: 190/255, blue: 111/255, alpha: 1)
            
        })
        
        
    }
    
    func phone(_ sender: UIButton) {
        
        UIApplication.shared.open(NSURL(string:"telprompt:" + phoneNumber)! as URL, options: [:], completionHandler: nil)
    }
    
    
    var incrementor = 0
    func methodThatTriggersNotification(_ title: String, body: String) {
       
    }
    
    
    

}


