//
//  actPage1.swift
//  cierge
//
//  Created by Alberto Cesare Barbon on 24/02/17.
//  Copyright © 2017 Alberto Cesare Barbon. All rights reserved.
//

import UIKit
import ParseUI
import Parse

class actPage1: PFQueryTableViewController {

    var array : [PFObject] = []
    
    override func queryForTable() -> PFQuery<PFObject> {
        
        let userDefaults = Foundation.UserDefaults.standard
        let idCheck = userDefaults.value(forKey: "idCheck")
        
        if (idCheck as! String != "none") {
            tableView.allowsSelection = true
        } else {
            tableView.allowsSelection = false
            
            self.perform(#selector(self.popUp), with: nil, afterDelay: 0.5)
        }
        
        navigationController?.navigationBar.isHidden = true
        
        let arrayid  = userDefaults.array(forKey: "arrayAct")
        print(arrayid!)
        let query = PFQuery(className: "Activities")
        
        query.cachePolicy = .networkElseCache
        query.whereKey("active", equalTo: true as Bool)
        query.whereKey("objectId", containedIn: arrayid!)
        
        
        return query
        
    }
    
    func popUp () {
        let alertController = UIAlertController(title: "Attenzione", message: "Puoi usufruire del servizio solo dopo aver ricevuto conferma di check-in da parte dell'hotel", preferredStyle: UIAlertControllerStyle.alert)
        
        let saveAction = UIAlertAction(title: "Ok", style: UIAlertActionStyle.cancel, handler: {
            alert -> Void in
            
            
        })
        
        
        
        alertController.addAction(saveAction)
        self.present(alertController, animated: true, completion: nil)
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath, object: PFObject?) -> PFTableViewCell? {
        
        array.append(object!)
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! ActTableViewCell
        
        let imageFile = object?.object(forKey: "photo") as? PFFile
        cell.cellImageView.backgroundColor = .clear //placeholder
        
        cell.cellImageView.file = imageFile
        cell.cellImageView.loadInBackground()
        
        cell.titleLabel.text = object?["name"] as? String
        
        return cell
        
        
    }
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        return 220
        
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        print("ciao")
        
        let pfObj = array[indexPath.row]
        let userDefaults = Foundation.UserDefaults.standard
        userDefaults.set( pfObj.objectId, forKey: "idService")
        let idCheck = userDefaults.value(forKey: "idCheck")
        
        if (idCheck as! String != "none") {
        tableView.allowsSelection = true
        print(pfObj["info"] as! String)
        let tipo = (pfObj["info"] as! String).components(separatedBy: "*")[0]
        
        switch tipo {
        case "1":
            
            tableView.deselectRow(at: indexPath, animated: true)
            self.performSegue(withIdentifier: "segueActCheck", sender: self)
            
            break
            
        case "2":
            
            tableView.deselectRow(at: indexPath, animated: true)
            self.performSegue(withIdentifier: "segueActList", sender: self)
            
            break
            
        default:
            break
        }
            
            
        } else {
            tableView.deselectRow(at: indexPath, animated: true)
            let alertController = UIAlertController(title: "Attenzione", message: "Puoi usufruire del servizio solo dopo aver ricevuto conferma di check-in da parte dell'hotel", preferredStyle: UIAlertControllerStyle.alert)
            
            let saveAction = UIAlertAction(title: "Ok", style: UIAlertActionStyle.cancel, handler: {
                alert -> Void in
                
                
            })
            
            alertController.addAction(saveAction)
            self.present(alertController, animated: true, completion: nil)
        }
        
        
    }

}
