//
//  SecondViewController.swift
//  cierge
//
//  Created by Alberto Cesare Barbon on 15/01/17.
//  Copyright © 2017 Alberto Cesare Barbon. All rights reserved.
//

import UIKit
import AutoCompleteTextField
import Parse
import LNRSimpleNotifications
import AudioToolbox
import RealmSwift

class SecondViewController: UIViewController, AutocompleteDelegate, UITextFieldDelegate {
    
    var autoCompleteViewController: AutoCompleteViewController!
    var isFirstLoad: Bool = true
    var countriesTextField: UITextField = UITextField()
    
    var Hotels : [String] = []
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        countriesTextField.delegate = self
        
        
        
        
        UIApplication.shared.statusBarStyle = .default
        
        let userDefaults = Foundation.UserDefaults.standard
        self.Hotels = userDefaults.array(forKey: "arrayCerca") as! [String]
        
        
        let viewUp : UIView = UIView()
        viewUp.backgroundColor = UIColor.init(red: 239/255, green: 239/255, blue: 239/255, alpha: 1)
        viewUp.frame = CGRect(x: 0, y: 0, width: view.frame.width, height: 64)
        view.addSubview(viewUp)
        
        let notifica : UIButton = UIButton(frame: CGRect(x: view.frame.width - 37.5, y: 27.5, width: 30, height: 30))
        notifica.setImage(UIImage(named: "bell.ong")?.maskWith(color: .darkGray), for: .normal)
        notifica.addTarget(self, action: #selector(SecondViewController.notifica), for: .touchUpInside)
        viewUp.addSubview(notifica)
        
        countriesTextField.frame = CGRect(x: 20, y: 79, width: view.frame.width - 40, height: 30)
        countriesTextField.backgroundColor = UIColor.init(red: 239/255, green: 239/255, blue: 239/255, alpha: 1)
        countriesTextField.layer.cornerRadius = 5
        countriesTextField.placeholder = "Cerca Hotel"
        countriesTextField.textAlignment = .center
        countriesTextField.tintColor = UIColor.lightGray
        countriesTextField.textColor = UIColor.gray
        view.addSubview(countriesTextField)
        
        let viewU10 : UIView = UIView(frame: CGRect(x: 0, y: self.countriesTextField.frame.maxY + 20, width: self.view.frame.width, height: 1))
        viewU10.backgroundColor = UIColor.init(red: 239/255, green: 239/255, blue: 239/255, alpha: 1)
        self.view.addSubview(viewU10)
        
        let label : UILabel = UILabel(frame: CGRect(x: 20, y: 30, width: view.frame.width-40, height: 20))
        label.textAlignment = .center
        label.text = "Cerca"
        label.textColor = UIColor.darkGray
        label.font = UIFont(name: "HelveticaNeue", size: 20)
        viewUp.addSubview(label)
        
            
    
        
    }

    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        
        if self.isFirstLoad {
            self.isFirstLoad = false
            Autocomplete.setupAutocompleteForViewcontroller(self)
        }
    }
    
    func autoCompleteTextField() -> UITextField {
        return self.countriesTextField
    }
    func autoCompleteThreshold(_ textField: UITextField) -> Int {
        return 1
    }
    
    func autoCompleteItemsForSearchTerm(_ term: String) -> [AutocompletableOption] {
        let filteredCountries = self.Hotels.filter { (country) -> Bool in
            return country.lowercased().contains(term.lowercased())
        }
        
        let countriesAndFlags: [AutocompletableOption] = filteredCountries.map { ( country) -> AutocompleteCellData in
            var country = country
            country.replaceSubrange(country.startIndex...country.startIndex, with: String(country.characters[country.startIndex]).capitalized)
            return AutocompleteCellData(text: country, image: UIImage(named: country))
            }.map( { $0 as AutocompletableOption })
        
        return countriesAndFlags
    }
    
    func autoCompleteHeight() -> CGFloat {
        return self.view.frame.height / 3.0
    }
    
    func didSelectItem(_ item: AutocompletableOption) {
        //print("selezionato")
        //print(item.text)
        countriesTextField.text = ""
        let id = item.text.components(separatedBy: "*")
        let userDefaults = Foundation.UserDefaults.standard
        userDefaults.set( id[2], forKey: "id")
        userDefaults.set( "none", forKey: "stato")
        userDefaults.set("none", forKey: "idCheck")
        userDefaults.set( "0", forKey: "numTemplate")
        
        let vc = self.storyboard!.instantiateViewController(withIdentifier: "hotelController") as! hotelController
        
        
        
        
        self.present(vc, animated: true, completion: nil)
        
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        countriesTextField.resignFirstResponder()
        return true
    }
    
    override func viewWillAppear(_ animated: Bool) {
        UIApplication.shared.statusBarStyle = .default
        countriesTextField.text = ""
    }
    
    func notifica() {
        let vc = self.storyboard!.instantiateViewController(withIdentifier: "notificaController") as! notificaController
        self.present(vc, animated: true, completion: nil)
    }


    
}

