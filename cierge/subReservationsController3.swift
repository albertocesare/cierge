//
//  subReservationsController3.swift
//  cierge
//
//  Created by Alberto Cesare Barbon on 03/03/17.
//  Copyright © 2017 Alberto Cesare Barbon. All rights reserved.
//

import UIKit
import Parse
import EventKit
import LNRSimpleNotifications
import AudioToolbox

class subReservationsController3: UIViewController, UITableViewDelegate, UITableViewDataSource {
    
    @IBOutlet weak var navBar : UINavigationBar? = UINavigationBar()
    var tableView : UITableView = UITableView()
    var array : [PFObject] = []
    let notificationManager = LNRNotificationManager()
    
    var arrayDate : [String] = []
    var arraySections : [Int] = []

    override func viewDidLoad() {
        super.viewDidLoad()
        
        tableView.delegate = self
        tableView.dataSource = self
        tableView.frame = CGRect(x: 0, y: 100, width: view.frame.width, height: view.frame.height - 84)
        self.tableView.tableFooterView = UIView()
        view.addSubview(tableView)

        notificationManager.notificationsPosition = LNRNotificationPosition.top
        notificationManager.notificationsBackgroundColor = UIColor.init(red: 28/255, green: 154/255, blue: 229/255, alpha: 1)
        notificationManager.notificationsTitleTextColor = UIColor.white
        notificationManager.notificationsBodyTextColor = UIColor.white
        notificationManager.notificationsSeperatorColor = UIColor.clear
        
        UIApplication.shared.statusBarStyle = .lightContent
        self.navBar?.titleTextAttributes = [
            NSFontAttributeName: UIFont.systemFont(ofSize: 20),
            NSForegroundColorAttributeName : UIColor.white
        ]
        
        query()
    }
    
    func query () {
        let userDefaults = Foundation.UserDefaults.standard
        let value = userDefaults.string(forKey: "idSubR")
        let query = PFQuery(className: "UserCheckin")
        query.getObjectInBackground(withId: value!, block: { (object, error) -> Void in
          
            let checkin = object?["checkIndate"] as! String + " 01:01"
            let checkout = object?["checkOutDate"] as! String + " 23:59"
            
            let formatter = DateFormatter()
            formatter.dateFormat = "dd-MM-yyyy HH:mm"
            formatter.locale = Locale.autoupdatingCurrent
            let checkindate1 = formatter.date(from: checkin)
            let checkoutdate1 = formatter.date(from: checkout)
            
            let formattazione = DateFormatter()
            formattazione.dateFormat = "dd MMM yyyy"
            let checkindate2 = formattazione.string(from: checkindate1!)
            let checkoutdate2 = formattazione.string(from: checkoutdate1!)
            
            let date : UILabel = UILabel(frame: CGRect(x: 20, y: (self.navBar?.frame.maxY)! + 10, width: self.view.frame.width - 40, height: 20))
            date.textAlignment = .center
            date.textColor = UIColor.init(red: 213/255, green: 190/255, blue: 111/255, alpha: 1)
            date.text = "in: " + checkindate2 + "     out: " + checkoutdate2
            self.view.addSubview(date)
            
            let hotelId = object?["hotelId"] as! String
            let query = PFQuery(className: "Hotels")
            query.getObjectInBackground(withId: hotelId, block: { (object, error) -> Void in
                
                self.navBar?.topItem?.title = object?["name"] as? String
                
                
                let queryAct = PFQuery(className: "ActivBooking")
                let userId = PFUser.current()?.objectId
                queryAct.whereKey("userId", equalTo: userId!)
                queryAct.whereKey("hotelId", equalTo: hotelId)
                queryAct.findObjectsInBackground { (objs, error) -> Void in
                    
                    self.array = self.array + objs!
                    
                    
                    let queryBooing = PFQuery(className: "RestBooking")
                    queryBooing.whereKey("userId", equalTo: userId!)
                    queryBooing.whereKey("hotelId", equalTo: hotelId)
                    queryBooing.findObjectsInBackground { (objes, error) -> Void in
                        
                        self.array = self.array + objes!
                        
                        
                        let queryIn = PFQuery(className: "InBooking")
                        queryIn.whereKey("userId", equalTo: userId!)
                        queryIn.whereKey("hotelId", equalTo: hotelId)
                        queryIn.findObjectsInBackground { (obs, error) -> Void in
                            
                            self.array = self.array + obs!
                            let dateFormatter = DateFormatter()
                            /*dateFormatter.dateFormat = "dd/MM/yyyy"
                            self.array.sort(by: {(dateFormatter.date(from: ($0["date"] as! String).components(separatedBy: "*")[0]))! < (dateFormatter.date(from: ($1["date"] as! String).components(separatedBy: "*")[0]))!})*/
                            
                            dateFormatter.dateFormat = "dd/MM/yyyy*HH:mm"
                            self.array.sort(by: {(dateFormatter.date(from: $0["date"] as! String))! < (dateFormatter.date(from: ($1["date"] as! String)))!})
                            
                            var a = 0
                            for o in self.array {
                                
                                var data = (o["date"] as! String)//.components(separatedBy: "*")[0]
                                data = data.replacingOccurrences(of: "*", with: " ")
                                let forrmatterOra = DateFormatter()
                                forrmatterOra.locale = Locale.current
                                forrmatterOra.dateFormat = "dd/MM/yyyy HH:mm"
                                let nsdata = formatter.date(from: (data))
                                
                                if (nsdata! >= checkoutdate1! || nsdata! <= checkindate1!) {
                                
                                    self.array.remove(at: a)
                                    a = a - 1
                                }
                                a = a + 1
                            }
                            
                            
                            
                            if self.array.count == 0 {
                                
                                let textView : UITextView = UITextView()
                                textView.frame = CGRect(x: 20, y: self.view.frame.height / 2 - 50, width: self.view.frame.width - 40, height: 100)
                                textView.textAlignment = .center
                                textView.text = "Non hai ancora effettuato alcuna prenotazione"
                                textView.font = UIFont.systemFont(ofSize: 15)
                                textView.textColor = .darkGray
                                self.view.addSubview(textView)
                            }
                            
                            var b = 0
                            for o in self.array {
                                
                                if (b == 0) {
                                    
                                    let int = 1
                                    self.arraySections.append(int)
                                    self.arrayDate.append((o["date"] as! String).components(separatedBy: "*")[0])
                                    
                                } else {
                                    
                                    let data1 = (o["date"] as! String).components(separatedBy: "*")[0]
                                    let data2 = (self.array[b - 1]["date"] as! String).components(separatedBy: "*")[0]
                                    
                                    if (data1 == data2) {
                                        
                                        let conto = self.arraySections.count - 1
                                        self.arraySections[conto] = self.arraySections[conto] + 1
                                        
                                    } else {
                                        
                                        let int = 1
                                        self.arraySections.append(int)
                                        self.arrayDate.append((o["date"] as! String).components(separatedBy: "*")[0])
                                        
                                    }
                                    
                                    
                                }
                                
                                b = b + 1
                            }
                            
                            print(self.arraySections)
                            print(self.arrayDate)
                            print(self.array)
                            self.tableView.reloadData()
                            
                            
                        }
                        
                    }
                
                }
                
                
            })
            
            
        })
        
        
        
        
    }
    
    @IBAction func dismiss() {
        dismiss(animated: true, completion: nil)
    }
    
    
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 70
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arraySections[section]
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return arraySections.count
    }
    
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        
        
        
        return self.arrayDate[section]
    }
    
    var a = 0
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = UITableViewCell()

        
        //let pfobj = array[a]
        let pfobj = array[indexPath.row]
        a = a + 1
        
        let pallino : UIView = UIView()
        pallino.frame = CGRect(x: 15, y: 27.5, width: 15, height: 15)
        pallino.layer.cornerRadius = 7.5
        pallino.clipsToBounds = true
        let state = pfobj["state"] as! String
        if (state == "a") {
            
            pallino.backgroundColor = UIColor.init(red: 109/255, green: 213/255, blue: 154/255, alpha: 1)
            
        } else if (state == "p") {
            
            //pallino.backgroundColor = UIColor.init(red: 213/255, green: 158/255, blue: 109/255, alpha: 2)
            pallino.backgroundColor = UIColor.orange
            
        } else {
            
            pallino.backgroundColor = #colorLiteral(red: 0.7450980544, green: 0.1568627506, blue: 0.07450980693, alpha: 1)
            
        }
        cell.addSubview(pallino)

        let className = pfobj.parseClassName
        var serviceId = ""
        var query = PFQuery()
        if (className == "RestBooking") {
            
            serviceId = pfobj["restaurantId"] as! String
            query = PFQuery(className: "Restaurants")
            query.getObjectInBackground(withId: serviceId, block: { (object, error) -> Void in
                
                let nome : UILabel = UILabel(frame: CGRect(x: 45, y: 15, width: cell.frame.width - 75, height: 20))
                nome.text = object?["name"] as? String
                cell.addSubview(nome)
            })
            
        } else if(className == "InBooking") {
            
            serviceId = pfobj["serviceId"] as! String
            query = PFQuery(className: "InServices")
            query.getObjectInBackground(withId: serviceId, block: { (object, error) -> Void in
                
                let nome : UILabel = UILabel(frame: CGRect(x: 45, y: 15, width: cell.frame.width - 75, height: 20))
                nome.text = (object?["info"] as! String).components(separatedBy: "@")[1].components(separatedBy: "*")[0]
                cell.addSubview(nome)
            })
            
        } else {
            
            serviceId = pfobj["activityId"] as! String
            query = PFQuery(className: "Activities")
            query.getObjectInBackground(withId: serviceId, block: { (object, error) -> Void in
                
                let nome : UILabel = UILabel(frame: CGRect(x: 45, y: 15, width: cell.frame.width - 75, height: 20))
                nome.text = object?["name"] as? String
                cell.addSubview(nome)
            })
        }
        
        let data = pfobj["date"] as! String
        
        let formatter = DateFormatter()
        formatter.dateFormat = "dd-MM-yyyy*HH:mm"
        let checkindate1 = formatter.date(from: data)
        
        let formattazione = DateFormatter()
        formattazione.dateFormat = "dd MMM yyyy, HH:mm"
        let checkindate2 = formattazione.string(from: checkindate1!)
        
        let labelDate = UILabel(frame: CGRect(x: 45, y: 15 + 20 + 5, width: cell.frame.width - 15, height: 20))
        labelDate.text = checkindate2
        labelDate.textColor = .gray
        labelDate.font = UIFont.systemFont(ofSize: 14)
        
        cell.addSubview(labelDate)
        
        let ora : UILabel = UILabel(frame: CGRect(x: cell.frame.width - 50, y: 35 - 12.5, width: 100, height: 25))
        ora.text = data.components(separatedBy: "*")[1]
        ora.font = UIFont.systemFont(ofSize: 30)
        cell.addSubview(ora)
        
        
        return cell
    }
    

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
    }
    
    
    func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        return true
    }
    
    func tableView(_ tableView: UITableView,
                   editActionsForRowAt indexPath: IndexPath) -> [UITableViewRowAction]? {
        
        let more = UITableViewRowAction(style: .normal, title: "Delete") { action, index in
            
            let pfobj = self.array[indexPath.row]
            var query = PFQuery(className:"InBooking")
            
            if (pfobj["restaurantId"] as? String != nil) {
                
                query = PFQuery(className:"RestBooking")
                
            } else  if (pfobj["serviceId"] as? String != nil) {
                
                query = PFQuery(className:"InBooking")
                
                
            } else  if (pfobj["activityId"] as? String != nil) {
                
                query = PFQuery(className:"ActivBooking")
                
                
            }
            
            query.getObjectInBackground(withId: pfobj.objectId!, block: { (object, error) -> Void in
                
                
                object?.deleteInBackground()
                tableView.reloadData()
                self.array.remove(at: indexPath.row)
                tableView.reloadData()
                
            })
            
            
            self.notificationManager.showNotification(title: "Cancellazione...", body: "Richiesta Inviata", onTap: nil)
            AudioServicesPlayAlertSound(SystemSoundID(kSystemSoundID_Vibrate))
            
        }
        more.backgroundColor = UIColor.init(red: 255/255, green: 59/255, blue: 48/255, alpha: 2)
        
        let favorite = UITableViewRowAction(style: .normal, title: "Evento") { action, index in
            
            let pfObj = self.array[indexPath.row]
            let date = ((pfObj["date"] as! String).components(separatedBy: "*"))[0]
            let formatter = DateFormatter()
            formatter.dateFormat = "dd/MM/yyyy"
            let nsdate = formatter.date(from: date)
            
            let alertController = UIAlertController(title: "Aggiungo a calendario", message:"" , preferredStyle: UIAlertControllerStyle.alert)
            
            let cancelAction = UIAlertAction(title: "Annulla", style: .destructive) { (action) in
                
            }
            
            let confirmAction = UIAlertAction(title: "Conferma", style: UIAlertActionStyle.cancel, handler: {
                (action : UIAlertAction!) -> Void in
                
                let firstTextField = alertController.textFields![0] as UITextField
                
                self.addEventToCalendar(title: firstTextField.text!, description: "f", startDate: nsdate! as NSDate, endDate: nsdate! as NSDate)
                self.notificationManager.showNotification(title: "Sto Aggiungendo", body: "L'evento al calendario", onTap: nil)
                AudioServicesPlayAlertSound(SystemSoundID(kSystemSoundID_Vibrate))
            })
            
            alertController.addTextField { (textField : UITextField!) -> Void in
                textField.placeholder = "Title"
                textField.textAlignment = .center
                //textField.backgroundColor = UIColor.init(red: 239/255, green: 239/255, blue: 239/255, alpha: 1)
                textField.layer.cornerRadius = 5
                textField.borderStyle = .none
                textField.layer.borderWidth = 0
            }
            
            
            alertController.addAction(cancelAction)
            alertController.addAction(confirmAction)
            self.present(alertController, animated: true, completion: nil)
            
        }
        favorite.backgroundColor = UIColor.init(red: 9/255, green: 152/255, blue: 232/255, alpha: 2)
        
        
        return [favorite, more]
    }
    
    
    
    func addEventToCalendar(title: String, description: String?, startDate: NSDate, endDate: NSDate, completion: ((_ success: Bool, _ error: NSError?) -> Void)? = nil) {
        let eventStore = EKEventStore()
        
        eventStore.requestAccess(to: .event, completion: { (granted, error) in
            if (granted) && (error == nil) {
                let event = EKEvent(eventStore: eventStore)
                event.title = title
                event.startDate = startDate as Date
                event.endDate = endDate as Date
                event.notes = description
                event.calendar = eventStore.defaultCalendarForNewEvents
                do {
                    try eventStore.save(event, span: .thisEvent)
                } catch let e as NSError {
                    completion?(false, e)
                    return
                }
                completion?(true, nil)
            } else {
                completion?(false, error as NSError?)
            }
        })
    }
    
    
}
