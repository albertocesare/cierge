//
//  TableViewCell.swift
//  cierge
//
//  Created by Alberto Cesare Barbon on 18/01/17.
//  Copyright © 2017 Alberto Cesare Barbon. All rights reserved.
//

import UIKit

class TableViewCell: UITableViewCell {
    @IBOutlet weak var imgView: UIImageView!
    @IBOutlet weak var nomeLabel: UILabel!

    override func awakeFromNib() {
        super.awakeFromNib()
        nomeLabel.text = " "
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
