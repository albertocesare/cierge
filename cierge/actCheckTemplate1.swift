//
//  actCheckTemplate1.swift
//  cierge
//
//  Created by Alberto Cesare Barbon on 09/02/17.
//  Copyright © 2017 Alberto Cesare Barbon. All rights reserved.
//

import UIKit
import Parse
import LNRSimpleNotifications
import AudioToolbox

class actCheckTemplate1: UIViewController, UITableViewDelegate, UITableViewDataSource, UIScrollViewDelegate {
    
    var scrollView : UIScrollView = UIScrollView()
    
    var image : UIImageView = UIImageView()
    var sottotitolo : UILabel = UILabel()
    var sottotitolo1 : UILabel = UILabel()
    var titolo : UILabel = UILabel()
    var descri : UITextView = UITextView()
    var tableView : UITableView = UITableView()
    var openAt : UILabel = UILabel()
    var payBt : UIButton = UIButton()
    var datePicker : UIDatePicker = UIDatePicker()
    
    var arrayServizi : [String] = []
    
    let blur1: UIVisualEffectView = UIVisualEffectView(effect: UIBlurEffect(style: UIBlurEffectStyle.dark))
    
    var viewQta : UIView = UIView()
    let labelQta : UILabel = UILabel()
    
    var arrayTurni : [String] = []
    var arraySelected : [String] = []
    
    var hotelId = ""
    var serviceId = ""
    
    var arrayOre : [String] = []
    
    var imageConc : UIImageView = UIImageView()
    var labelHotel : UILabel = UILabel()
    let nomeCon : UILabel = UILabel()
    
    let notificationManager = LNRNotificationManager()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        scrollView.delegate = self
        navigationController?.navigationBar.isHidden = true
        NotificationCenter.default.addObserver(self, selector: #selector(checkTemplate1.pop2), name: NSNotification.Name.init(rawValue: "cambia"), object: nil)
        
        notificationManager.notificationsPosition = LNRNotificationPosition.top
        notificationManager.notificationsBackgroundColor = UIColor.init(red: 109/255, green: 213/255, blue: 154/255, alpha: 1)
        notificationManager.notificationsTitleTextColor = UIColor.white
        notificationManager.notificationsBodyTextColor = UIColor.white
        notificationManager.notificationsSeperatorColor = UIColor.clear
        
        imageConc.layer.cornerRadius = 25
        
        tableView.delegate = self
        tableView.dataSource = self
        tableView.allowsMultipleSelection = true
        self.tableView.register(UINib(nibName: "CheckListCell", bundle: nil), forCellReuseIdentifier: "cell")
        scrollView.frame = view.frame
        
        
        view.frame = CGRect(x: 0, y: 0, width: view.frame.width, height: view.frame.height - (110))
        scrollView.frame = view.frame
        
        image.frame = CGRect(x: 0, y: 0, width: view.frame.width, height: 200)
        scrollView.addSubview(image)
        
        let userDefaults = Foundation.UserDefaults.standard
        let value  = userDefaults.string(forKey: "idService")
        var query = PFQuery(className:"Activities")
        query.getObjectInBackground(withId: value!, block: { (object, error) -> Void in
            
            
            let foto = object?["photo"] as! PFFile
            foto.getDataInBackground { (imageData, error) -> Void in
                if (error == nil) {
                    if (imageData != nil) {
                        self.image.image = UIImage(data:imageData!)
                    }
                }
            }
            
            let inutile0 = object?["info"] as! String
            //let inutile1 = inutile0.components(separatedBy: "_")[1]
            let infos = inutile0.components(separatedBy: "*")
            let nome = object?["name"] as! String
            self.titolo.text = nome
            
            let sotto = infos[3]
            
            self.sottotitolo.text = sotto
            self.sottotitolo1.text = sotto
            
            
            
            
            
            let subService = (object?["subServices"] as! String).components(separatedBy: "@")[1].components(separatedBy: "*")
            var count = subService.count - 1
            while count > -1 {
                
                let servN = subService[count]
                
                
                let arrayServizio = servN.components(separatedBy: "_")
                let active = arrayServizio[0]
                if (String(active) == "a") {
                    self.arrayServizi.append(servN)
                    self.tableView.reloadData()
                }
                
                
                print(self.arrayServizi)
                count = count - 1
                
            }
            
            let turniarrdainfo = infos[1]
            let turniarr = turniarrdainfo.components(separatedBy: "_")
            
            ///////
            self.arrayOre = turniarr
            
            if (turniarr[0] == "1") {
                //1 turno
                
                let turnoOpn = turniarr[1]
                let turnoCls = turniarr[2]
                
                let string = turnoOpn + turnoCls
                
                self.arrayTurni.append(string)
                
                self.openAt.text = "Apertura: " + turnoOpn + " - " + turnoCls
                
            } else {
                
                var turnoOpn = turniarr[1]
                var turnoCls = turniarr[2]
                self.openAt.text = "Apertura: " + turnoOpn + " - " + turnoCls
                
                turnoOpn = turniarr[3]
                turnoCls = turniarr[4]
                self.openAt.text = self.openAt.text! + " | " + turnoOpn + " - " + turnoCls
            }
        })
        
        
        
        
        
        
        
        
        sottotitolo1.frame = CGRect(x: 20, y: image.frame.maxY + 20, width: view.frame.width - 40, height: 20)
        sottotitolo1.textColor = UIColor.lightGray
        sottotitolo1.font = UIFont.systemFont(ofSize: 18)
        sottotitolo1.textAlignment = .center
        scrollView.addSubview(sottotitolo1)
        
        titolo.frame = CGRect(x: 20, y: sottotitolo1.frame.maxY + 20, width: view.frame.width - 40, height: 35)
        titolo.font = UIFont.systemFont(ofSize: 30)
        titolo.textColor = .black
        titolo.textAlignment = .center
        scrollView.addSubview(titolo)
        
        let viewU1 : UIView = UIView(frame: CGRect(x: 20, y: titolo.frame.maxY + 15, width: view.frame.width - 40, height: 1))
        viewU1.backgroundColor = UIColor.init(red: 239/255, green: 239/255, blue: 239/255, alpha: 1)
        scrollView.addSubview(viewU1)
        
        let imageOrologio : UIImageView = UIImageView(image: UIImage(named: "time.png"))
        imageOrologio.frame = CGRect(x: 20, y: viewU1.frame.maxY + 17.5, width: 15, height: 15)
        scrollView.addSubview(imageOrologio)
        
        openAt.frame = CGRect(x: 40, y: viewU1.frame.maxY + 15, width: view.frame.width - 70, height: 20)
        openAt.textColor = .lightGray
        openAt.font = UIFont.systemFont(ofSize: 14)
        scrollView.addSubview(openAt)
        let viewU2 : UIView = UIView(frame: CGRect(x: 20, y: openAt.frame.maxY + 15, width: view.frame.width - 40, height: 1))
        viewU2.backgroundColor = UIColor.init(red: 239/255, green: 239/255, blue: 239/255, alpha: 1)
        scrollView.addSubview(viewU2)
        
        
        
        payBt.frame = CGRect(x: view.frame.width - 60, y: view.frame.height - 60, width: 40, height: 40)
        payBt.layer.cornerRadius = 20
        payBt.backgroundColor = UIColor.init(red: 213/255, green: 190/255, blue: 111/255, alpha: 1)
        payBt.setImage(UIImage(named: "piu.png"), for: .normal)
        payBt.isHidden = true
        payBt.isEnabled = false
        payBt.addTarget(self, action: #selector(checkTemplate1.pay(_:)), for: .touchUpInside)
        view.addSubview(payBt)
        
        
        
        
        query = PFQuery(className: "ActivDescr")
        query.findObjectsInBackground { (objec, error) -> Void in
            
            
            for descr in objec! {
                let userDefaults = Foundation.UserDefaults.standard
                let idH  = userDefaults.string(forKey: "id")
                let idS  = userDefaults.string(forKey: "idService")
                
                
                
                if (descr["hotelId"] as? String == idH && descr["activityId"] as? String == idS) {
                    
                    self.imageConc.frame = CGRect(x: 20, y: viewU2.frame.maxY + 10, width: 50, height: 50)
                    self.imageConc.layer.cornerRadius = 25
                    self.scrollView.addSubview(self.imageConc)
                    self.labelHotel.frame = CGRect(x: 80, y: viewU2.frame.maxY + 10, width: self.view.frame.width - 60, height: 20)
                    self.scrollView.addSubview(self.labelHotel)
                    self.nomeCon.frame = CGRect(x: 80, y: self.labelHotel.frame.maxY + 10, width: self.view.frame.width - 60, height: 20
                    )
                    self.scrollView.addSubview(self.nomeCon)
                    
                    let query = PFQuery(className:"Hotels")
                    query.getObjectInBackground(withId: (descr["hotelId"] as? String)!, block: { (o, error) -> Void in
                        
                        self.nomeCon.text = o?["ciergeName"] as? String
                        self.labelHotel.text = o?["name"] as? String
                        let foto = o?["ciergePhoto"] as! PFFile
                        foto.getDataInBackground { (imageData, error) -> Void in
                            if (error == nil) {
                                if (imageData != nil) {
                                    self.imageConc.image = UIImage(data:imageData!)
                                }
                            }
                        }
                        
                    })
                    
                    self.descri.frame = CGRect(x: 20, y: self.imageConc.frame.maxY + 10, width: self.view.frame.width - 40, height: 250)
                    self.descri.text = descr["description"] as! String
                    self.descri.isEditable = false
                    self.descri.textColor = .gray
                    self.descri.font = UIFont.systemFont(ofSize: 15)
                    self.descri.backgroundColor = .white
                    self.scrollView.addSubview(self.descri)
                    
                    self.view.addSubview(self.scrollView)
                    
                    
                    self.tableView.frame = CGRect(x: 20, y: self.descri.frame.maxY + 15, width: self.view.frame.width - 40, height: 88)
                    self.scrollView.addSubview(self.tableView)
                    
                    self.scrollView.contentSize = CGSize(width: self.view.frame.width, height: self.tableView.frame.maxY + 70)
                    self.view.addSubview(self.scrollView)
                    
                    let blur : UIVisualEffectView = UIVisualEffectView(effect: UIBlurEffect(style: .dark))
                    blur.frame = CGRect(x: 0, y: 0, width: self.view.frame.width, height: 64)
                    
                    let disBt = UIButton() as UIButton
                    disBt.tintColor = UIColor.white
                    disBt.frame = CGRect(x: 0, y: 20, width: 40, height: 40)
                    let imageM = UIImage(named: "back.png")?.maskWith(color: .white)
                    disBt.setImage(imageM, for: .normal)
                    disBt.addTarget(self, action: #selector(actCheckTemplate1.dismiss(_:)), for: .touchUpInside)
                    blur.addSubview(disBt)
                    
                    let sottotitolo : UILabel = UILabel()
                    sottotitolo.frame = CGRect(x: 20, y: 35, width: self.view.frame.width - 40, height: 20)
                    sottotitolo.textAlignment = .center
                    sottotitolo.textColor = .white
                    sottotitolo.text = "Attività"
                    blur.addSubview(sottotitolo)
                    
                    self.view.addSubview(blur)
                    
                    let btBack : UIButton = UIButton(frame: CGRect(x: 5, y: self.scrollView.frame.maxY - 50, width: 150, height: 30))
                    btBack.setImage(UIImage(named:"cierge-trasp-1.png"), for: .normal)
                    btBack.layer.cornerRadius = 15
                    btBack.clipsToBounds = true
                    btBack.setTitle("Indietro", for: .normal)
                    btBack.setTitleColor(.white, for: .normal)
                    btBack.contentHorizontalAlignment = .left
                    //btBack.addTarget(self, action: #selector(ristoTemplate.pop(_:)), for: .touchUpInside)
                    //self.view.addSubview(btBack)
                    let labelBack : UILabel = UILabel(frame: CGRect(x: 15, y: self.scrollView.frame.maxY - 50 + 15 - 10, width: 40, height: 20))
                    labelBack.text = "Indietro"
                    labelBack.textColor = .white
                    labelBack.textAlignment = .center
                    //self.view.addSubview(labelBack)
                    
                } else {
                    
                    
                    let userDefaults = Foundation.UserDefaults.standard
                    let value  = userDefaults.string(forKey: "idService")
                    let query = PFQuery(className:"Activities")
                    query.getObjectInBackground(withId: value!, block: { (object, error) -> Void in
                        
                        let count = (object?["info"] as! String).components(separatedBy: "*").count
                        self.descri.text = (object?["info"] as! String).components(separatedBy: "*")[count-1]
                        
                    })
                    
                    
                    
                    
                    self.descri.frame = CGRect(x: 20, y: viewU1.frame.maxY + 10, width: self.view.frame.width - 40, height: 250)
                    
                    self.descri.isEditable = false
                    self.descri.textColor = .gray
                    self.descri.font = UIFont.systemFont(ofSize: 15)
                    self.descri.backgroundColor = .white
                    self.scrollView.addSubview(self.descri)
                    
                    self.tableView.frame = CGRect(x: 20, y: self.descri.frame.maxY + 15, width: self.view.frame.width - 40, height: 88)
                    self.scrollView.addSubview(self.tableView)
                    
                    self.scrollView.contentSize = CGSize(width: self.view.frame.width, height: self.tableView.frame.maxY + 70)
                    self.view.addSubview(self.scrollView)
                    
                    let btBack : UIButton = UIButton(frame: CGRect(x: 5, y: self.scrollView.frame.maxY - 50, width: 150, height: 30))
                    btBack.setImage(UIImage(named:"cierge-trasp-1.png"), for: .normal)
                    btBack.layer.cornerRadius = 15
                    btBack.clipsToBounds = true
                    btBack.setTitle("Indietro", for: .normal)
                    btBack.setTitleColor(.white, for: .normal)
                    btBack.contentHorizontalAlignment = .left
                    btBack.addTarget(self, action: #selector(ristoTemplate.pop(_:)), for: .touchUpInside)
                    //self.view.addSubview(btBack)
                    let labelBack : UILabel = UILabel(frame: CGRect(x: 15, y: self.scrollView.frame.maxY - 50 + 15 - 10, width: 40, height: 20))
                    labelBack.text = "Indietro"
                    labelBack.textColor = .white
                    labelBack.textAlignment = .center
                    //self.view.addSubview(labelBack)
                
                
                
                    
                    
                    
                }
                
                
                
                
                
            }
            
            
            
            
        }
        
        
        
        
        
        
    }
    
    func pop(_ button: UIButton) {
        _ = navigationController?.popViewController(animated: true)
    }
    
    func pop2() {
        _ = navigationController?.popViewController(animated: true)
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        if scrollView.contentOffset.y < -30 {
            self.navigationController?.popToRootViewController(animated: true)
        }
    }
    
    func dismiss(_ sender: UIButton) {
        let id = "4"
        let userDefaults = Foundation.UserDefaults.standard
        userDefaults.set( id, forKey: "numTemplate")
        let vc = self.storyboard!.instantiateViewController(withIdentifier: "hotelController") as! hotelController
        self.present(vc, animated: true, completion: nil)
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! CheckListCell
        
        let servizio = arrayServizi[indexPath.row]
        let arrayServizio = servizio.components(separatedBy: "_")
        let oggetto = arrayServizio[1]
        let prezzo = arrayServizio[2]
        
        cell.nomeLabel.text = oggetto
        cell.prezzoLabel.text = prezzo + " €"
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        print(arrayServizi.count)
        print(arrayServizi)
        return arrayServizi.count
    }
    
    var labelTitolo : UILabel = UILabel()
    var a = 0
    var index = 0
    var nome = ""
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let userDefaults = Foundation.UserDefaults.standard
        let value  = userDefaults.string(forKey: "idCheck")
        print(value!)
        
        if (value == "none") {
            
            
        } else {
        
        index = indexPath.row
        
        let servizio = arrayServizi[indexPath.row]
        print(servizio)
        let infoServizi = servizio.components(separatedBy: "_")
        nome = infoServizi[1]
        
        let userDefaults = Foundation.UserDefaults.standard
        let stato  = userDefaults.string(forKey: "stato")
        if (stato == "a"){
            
            a = a + 1
            self.payBt.isHidden = false
            self.payBt.isEnabled = true
            tableView.cellForRow(at: indexPath as IndexPath)?.accessoryType = UITableViewCellAccessoryType.checkmark
            
            let alertController = UIAlertController(title: "Inserisci Quantità", message: "", preferredStyle: UIAlertControllerStyle.alert)
            
            let saveAction = UIAlertAction(title: "Conferma", style: UIAlertActionStyle.default, handler: {
                alert -> Void in
                
                let firstTextField = alertController.textFields![0] as UITextField
                firstTextField.keyboardType = .numberPad
                let quantita = firstTextField.text
                let indexcella = indexPath.row
                let stringa = String(describing: quantita!) + "x" + String(describing: indexcella)
                self.arraySelected.append(stringa)
                self.d = self.arraySelected.count - 1
                
                
            })
            
            let cancelAction = UIAlertAction(title: "Cancella", style: UIAlertActionStyle.destructive, handler: {
                (action : UIAlertAction!) -> Void in
                
                /*if(self.arraySelected.count < 1) {
                 self.payBt.isHidden = true
                 self.payBt.isEnabled = false
                 }*/
                
                tableView.deselectRow(at: indexPath, animated: true)
                tableView.cellForRow(at: indexPath as IndexPath)?.accessoryType = UITableViewCellAccessoryType.none
                
            })
            
            alertController.addTextField { (textField : UITextField!) -> Void in
                textField.text = "1"
                textField.textAlignment = .center
                textField.keyboardType = .numberPad
                //textField.backgroundColor = UIColor.init(red: 239/255, green: 239/255, blue: 239/255, alpha: 1)
                textField.layer.cornerRadius = 5
                textField.borderStyle = .none
                textField.layer.borderWidth = 0
            }
            
            
            alertController.addAction(saveAction)
            alertController.addAction(cancelAction)
            
            self.present(alertController, animated: true, completion: nil)
            
        } else {
            tableView.deselectRow(at: indexPath, animated: true)
            
        }
            
        }
        
    }
    
    func tableView(_ tableView: UITableView, didDeselectRowAt indexPath: IndexPath) {
        
        a = a - 1
        if (a < 1) {
            
        }
        tableView.cellForRow(at: indexPath as IndexPath)?.accessoryType = UITableViewCellAccessoryType.none
        
        var b = 0
        for object in arraySelected {
            
            let inutile = object.components(separatedBy: "x")
            if (inutile[1] == String(describing: indexPath.row)) {
                arraySelected.remove(at: b)
            }
            b = b + 1
        }
        
        print(arraySelected)
        
        if (arraySelected.count == 0) {
            payBt.isHidden = true
        }
        
    }
    
    
    
    
    
    var d = 0
    
    func pay(_ button: UIButton) {
        
        
        let userDefaults = Foundation.UserDefaults.standard
        let value  = userDefaults.string(forKey: "idCheck")
        print(value!)
        
        let user = PFUser.current()?.objectId
        var checkInDate = ""
        var checkOutDate = ""
        
        let query = PFQuery(className:"UserCheckin")
        serviceId = value!
        query.getObjectInBackground(withId: value!, block: { (object, error) -> Void in
            
            print(object!)
            
            let userID = object?["userId"] as! String
            //let hotelID = object?["hotelId"] as! String
            if (userID == user){
                //if(self.hotelId == hotelID) {
                
                if ((object?["state"] as! String) == "a") {
                    
                    self.payBt.isUserInteractionEnabled = true
                    self.payBt.isHidden = false
                    
                    checkInDate = object?["checkIndate"] as! String
                    checkOutDate = object?["checkOutDate"] as! String
                    
                    var dateFormatter = DateFormatter()
                    dateFormatter.dateFormat = "dd-MM-yyyy HH:mm"
                    let arrDataIn = checkInDate.components(separatedBy: "/")
                    let arrDataOut = checkOutDate.components(separatedBy: "/")
                    
                    let giornoIn = arrDataIn[0]
                    let meseIn = arrDataIn[1]
                    let annoIn = arrDataIn[2]
                    let giornoOut = arrDataOut[0]
                    let meseOut = arrDataOut[1]
                    let annoOut = arrDataOut[2]
                    var oraOpen = ""
                    var oraClose = ""
                    
                    if (self.arrayOre[0] == "1") {
                        
                        oraOpen = self.arrayOre[1]
                        oraClose = self.arrayOre[2]
                        
                        
                    } else {
                        
                        oraOpen = self.arrayOre[1]
                        oraClose = self.arrayOre[4]
                        
                    }
                    let dataIn = giornoIn + "-" + meseIn + "-" + annoIn + " " + oraOpen
                    print(dataIn)
                    let dataOut = giornoOut + "-" + meseOut + "-" + annoOut + " " + oraClose
                    dateFormatter = DateFormatter()
                    dateFormatter.dateFormat = "dd-MM-yyyy HH:mm"
                    let newdataIn = dateFormatter.date(from: dataIn)
                    print(newdataIn!)
                    let newdataOut = dateFormatter.date(from: dataOut)
                    print(newdataOut!)
                    self.datePicker.minimumDate = newdataIn
                    self.datePicker.maximumDate = newdataOut
                }
                //}
            }
            
            
            let alertController = UIAlertController(title: "Selezione Date", message:"\n\n\n\n\n\n\n\n " , preferredStyle: UIAlertControllerStyle.actionSheet)
            self.datePicker.frame = CGRect(x: alertController.view.frame.width/2 - 150, y: 20, width: 300, height: 200)
            alertController.view.addSubview(self.datePicker)
            alertController.view.frame = CGRect(x: 0, y: 0, width: 300, height: 200)
            alertController.view.bounds = CGRect(x: 0, y: 0, width: 300, height: 200)
            
            let cancelAction = UIAlertAction(title: "Cancella", style: .destructive) { (action) in
                
                
                
            }
            
            let confirmAction = UIAlertAction(title: "Conferma", style: UIAlertActionStyle.cancel, handler: {
                
                (action : UIAlertAction!) -> Void in
                let dateFormatter = DateFormatter()
                dateFormatter.dateFormat = "dd-MM-yyyy HH:mm"
                let selectedDate = dateFormatter.string(from: self.datePicker.date)
                print(selectedDate)
                let book = PFObject(className:"ActivBooking")
                book["userId"] = PFUser.current()?.objectId
                let stringa = selectedDate.replacingOccurrences(of: "-", with: "/")
                book["date"] = stringa.replacingOccurrences(of: " ", with: "*")
                let userDefaults = Foundation.UserDefaults.standard
                book["hotelId"] = object?["hotelId"]
                book["activityId"] = userDefaults.string(forKey: "idService")
                book["state"] = "p"
                var stri = "1@"
                for object in self.arraySelected {
                    let split = object.components(separatedBy: "x")
                    let ind = split[1]
                    let obj = self.arrayServizi[Int(ind)!]
                    let arrayObj = obj.components(separatedBy: "_")
                    let nome = arrayObj[1]
                    let prezzo = arrayObj[2]
                    let qta = split[0]
                    stri = stri + nome + "_" + prezzo + "_" + qta + "*"
                }
                book["subServices"] = stri
                book.saveInBackground()
                for o in self.arraySelected {
                    let split = o.components(separatedBy: "x")
                    let indice = Int(split[1])!
                    let index : NSIndexPath = IndexPath(item: indice, section: 0) as NSIndexPath
                    print(index)
                    self.tableView.deselectRow(at: index as IndexPath, animated: true)
                    self.tableView.cellForRow(at: index as IndexPath)?.accessoryType = UITableViewCellAccessoryType.none
                }
                self.payBt.isHidden = true
                self.arraySelected.removeAll()
                self.notificationManager.showNotification(title: "Congratulazioni", body: "Attività Richiesta", onTap: nil)
                AudioServicesPlayAlertSound(SystemSoundID(kSystemSoundID_Vibrate))
            })
            
            
            alertController.addAction(cancelAction)
            alertController.addAction(confirmAction)
            self.present(alertController, animated: true, completion: nil)
            
        })
        
    }
    
    
}
