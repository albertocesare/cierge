//
//  listTemplate1.swift
//  cierge
//
//  Created by Alberto Cesare Barbon on 03/02/17.
//  Copyright © 2017 Alberto Cesare Barbon. All rights reserved.
//

import UIKit
import Parse
import LNRSimpleNotifications
import AudioToolbox

class listTemplate1: UIViewController, UITableViewDelegate, UITableViewDataSource, UIScrollViewDelegate {
    
    var scrollView : UIScrollView = UIScrollView()
    
    var image : UIImageView = UIImageView()
    var sottotitolo : UILabel = UILabel()
    var sottotitolo1 : UILabel = UILabel()
    var titolo : UILabel = UILabel()
    var descri : UITextView = UITextView()
    var tableView : UITableView = UITableView()
    var openAt : UILabel = UILabel()
    var payBt : UIButton = UIButton()
    var datePicker : UIDatePicker = UIDatePicker()
    
    var arrayServizi : [String] = []
    
    let blur1: UIVisualEffectView = UIVisualEffectView(effect: UIBlurEffect(style: UIBlurEffectStyle.light))
    
    var viewQta : UIView = UIView()
    let labelQta : UILabel = UILabel()
    
    var arrayTurni : [String] = []
    var arraySelected : [String] = []
    
    var hotelId = ""
    var serviceId = ""
    
    var arrayOre : [String] = []
    
    let notificationManager = LNRNotificationManager()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        scrollView.delegate = self
        
        NotificationCenter.default.addObserver(self, selector: #selector(checkTemplate1.pop2), name: NSNotification.Name.init(rawValue: "cambia"), object: nil)
        
        notificationManager.notificationsPosition = LNRNotificationPosition.top
        notificationManager.notificationsBackgroundColor = UIColor.init(red: 109/255, green: 213/255, blue: 154/255, alpha: 1)
        notificationManager.notificationsTitleTextColor = UIColor.white
        notificationManager.notificationsBodyTextColor = UIColor.white
        notificationManager.notificationsSeperatorColor = UIColor.clear
        
        tableView.delegate = self
        tableView.dataSource = self
        tableView.allowsMultipleSelection = true
        self.tableView.register(UINib(nibName: "CheckListCell", bundle: nil), forCellReuseIdentifier: "cell")
        
        view.frame = CGRect(x: 0, y: 0, width: view.frame.width, height: view.frame.height - (110))
        scrollView.frame = view.frame
        
        image.frame = CGRect(x: 0, y: 0, width: view.frame.width, height: 200)
        scrollView.addSubview(image)
        
        
        let userDefaults = Foundation.UserDefaults.standard
        let value  = userDefaults.string(forKey: "idService")
        let query = PFQuery(className:"InServices")
        serviceId = value!
        query.getObjectInBackground(withId: value!, block: { (object, error) -> Void in
            
            self.hotelId = object?["hotelId"] as! String
            
            let foto = object?["photo"] as! PFFile
            foto.getDataInBackground { (imageData, error) -> Void in
                if (error == nil) {
                    if (imageData != nil) {
                        self.image.image = UIImage(data:imageData!)
                    }
                }
            }
            
            let inutile0 = object?["info"] as! String
            let inutile1 = inutile0.components(separatedBy: "@")
            let infosArr = inutile1[1]
            let infos = infosArr.components(separatedBy: "*")
            let nome = infos[0]
            self.titolo.text = nome
            
            let sotto = infos[1]
            self.sottotitolo.text = sotto
            self.sottotitolo1.text = sotto
            
            
            let descr = infos[2]
            self.descri.text = descr
            
            var count = infos.count - 1
            while count > 4 {
                
                let servN = infos[count]
                
                
                let arrayServizio = servN.components(separatedBy: "_")
                let active = arrayServizio[0]
                if (String(active) == "a") {
                    self.arrayServizi.append(servN)
                }
                
                
                print(self.arrayServizi)
                count = count - 1
                self.tableView.reloadData()
            }
            
            let turniarrdainfo = infos[3]
            let turniarr = turniarrdainfo.components(separatedBy: "_")
            
            ///////
            self.arrayOre = turniarr
            
            if (turniarr[0] == "1") {
                //1 turno
                
                let turnoOpn = turniarr[1]
                let turnoCls = turniarr[2]
                
                let string = turnoOpn + turnoCls
                
                self.arrayTurni.append(string)
                
                self.openAt.text = "Apertura: " + turnoOpn + " - " + turnoCls
                
            } else {
                
                var turnoOpn = turniarr[1]
                var turnoCls = turniarr[2]
                self.openAt.text = "Apertura: " + turnoOpn + " - " + turnoCls
                
                turnoOpn = turniarr[3]
                turnoCls = turniarr[4]
                self.openAt.text = self.openAt.text! + " | " + turnoOpn + " - " + turnoCls
                
                
                
            }
            
            
            
        })
        
        
        
        
        
        sottotitolo1.frame = CGRect(x: 20, y: image.frame.maxY + 20, width: view.frame.width - 40, height: 20)
        sottotitolo1.textColor = UIColor.lightGray
        sottotitolo1.font = UIFont.systemFont(ofSize: 18)
        sottotitolo1.textAlignment = .center
        scrollView.addSubview(sottotitolo1)
        
        titolo.frame = CGRect(x: 20, y: sottotitolo1.frame.maxY + 20, width: view.frame.width - 40, height: 35)
        titolo.font = UIFont.systemFont(ofSize: 30)
        titolo.textColor = .black
        titolo.textAlignment = .center
        scrollView.addSubview(titolo)
        
        let viewU1 : UIView = UIView(frame: CGRect(x: 20, y: titolo.frame.maxY + 15, width: view.frame.width - 40, height: 1))
        viewU1.backgroundColor = UIColor.init(red: 239/255, green: 239/255, blue: 239/255, alpha: 1)
        scrollView.addSubview(viewU1)
        
        let imageOrologio : UIImageView = UIImageView(image: UIImage(named: "time.png"))
        imageOrologio.frame = CGRect(x: 20, y: viewU1.frame.maxY + 17.5, width: 15, height: 15)
        scrollView.addSubview(imageOrologio)
        
        openAt.frame = CGRect(x: 40, y: viewU1.frame.maxY + 15, width: view.frame.width - 70, height: 20)
        openAt.textColor = .lightGray
        openAt.font = UIFont.systemFont(ofSize: 14)
        scrollView.addSubview(openAt)
        let viewU2 : UIView = UIView(frame: CGRect(x: 20, y: openAt.frame.maxY + 15, width: view.frame.width - 40, height: 1))
        viewU2.backgroundColor = UIColor.init(red: 239/255, green: 239/255, blue: 239/255, alpha: 1)
        scrollView.addSubview(viewU2)
        
        descri.frame = CGRect(
            x: 20, y: viewU2.frame.maxY + 15, width: view.frame.width - 40, height: 250)
        descri.isEditable = false
        descri.textColor = .gray
        descri.font = UIFont.systemFont(ofSize: 14)
        descri.backgroundColor = .white
        scrollView.addSubview(descri)
        
        tableView.frame = CGRect(x: 20, y: descri.frame.maxY + 15, width: view.frame.width - 40, height: 88)
        scrollView.addSubview(tableView)
        
        scrollView.contentSize = CGSize(width: view.frame.width, height: tableView.frame.maxY + 70)
        view.addSubview(scrollView)
        
        let btBack : UIButton = UIButton(frame: CGRect(x: 5, y: scrollView.frame.maxY - 50, width: 150, height: 30))
        btBack.setImage(UIImage(named:"cierge-trasp-1.png"), for: .normal)
        btBack.layer.cornerRadius = 15
        btBack.clipsToBounds = true
        btBack.setTitle("Indietro", for: .normal)
        btBack.setTitleColor(.white, for: .normal)
        btBack.contentHorizontalAlignment = .left
        btBack.addTarget(self, action: #selector(ristoTemplate.pop(_:)), for: .touchUpInside)
        //view.addSubview(btBack)
        let labelBack : UILabel = UILabel(frame: CGRect(x: 15, y: scrollView.frame.maxY - 50 + 15 - 10, width: 40, height: 20))
        labelBack.text = "Indietro"
        labelBack.textColor = .white
        labelBack.textAlignment = .center
        //view.addSubview(labelBack)
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        if scrollView.contentOffset.y < -30 {
            self.navigationController?.popToRootViewController(animated: true)
        }
    }
    
    func pop(_ button: UIButton) {
        _ = navigationController?.popViewController(animated: true)
    }
    
    func pop2() {
        _ = navigationController?.popViewController(animated: true)
    }
    
    func dismiss(_ sender: UIButton) {
        let id = "1"
        let userDefaults = Foundation.UserDefaults.standard
        userDefaults.set( id, forKey: "numTemplate")
        let vc = self.storyboard!.instantiateViewController(withIdentifier: "hotelController") as! hotelController
        self.present(vc, animated: true, completion: nil)
    }
    
    func dismisss() {
        let id = "1"
        let userDefaults = Foundation.UserDefaults.standard
        userDefaults.set( id, forKey: "numTemplate")
        let vc = self.storyboard!.instantiateViewController(withIdentifier: "hotelController") as! hotelController
        self.present(vc, animated: true, completion: nil)
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! CheckListCell
        
        //cell.accessoryType = .checkmark
        
        let servizio = arrayServizi[indexPath.row]
        let arrayServizio = servizio.components(separatedBy: "_")
        let oggetto = arrayServizio[1]
        let prezzo = arrayServizio[2]
        
        cell.nomeLabel.text = oggetto
        cell.prezzoLabel.text = prezzo + " €"
        
        
        
        return cell
    }
    
    
    let viewPay : UIView = UIView()
    let blur2: UIVisualEffectView = UIVisualEffectView(effect: UIBlurEffect(style: UIBlurEffectStyle.light))
    
    var index = 0
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        index = indexPath.row
        let user = PFUser.current()?.objectId
        var checkInDate = ""
        var checkOutDate = ""
        
        let userDefaults = Foundation.UserDefaults.standard
        let value  = userDefaults.string(forKey: "idCheck")
        print(value!)
        
        if (value == "none") {
            
            tableView.deselectRow(at: indexPath, animated: true)
            
        } else {
        
        let query = PFQuery(className:"UserCheckin")
        serviceId = value!
        query.getObjectInBackground(withId: value!, block: { (object, error) -> Void in
            
            
            let userID = object?["userId"] as! String
            let hotelID = object?["hotelId"] as! String
            if (userID == user){
                if(self.hotelId == hotelID) {
                    
                    if ((object?["state"] as! String) == "a") {
                        
                        self.payBt.isUserInteractionEnabled = true
                        self.payBt.isHidden = false
                        
                        checkInDate = object?["checkIndate"] as! String
                        checkOutDate = object?["checkOutDate"] as! String
                        
                        let dateFormatter = DateFormatter()
                        dateFormatter.dateFormat = "dd-MM-yyyy HH:mm"
                        let arrDataIn = checkInDate.components(separatedBy: "/")
                        let arrDataOut = checkOutDate.components(separatedBy: "/")
                        
                        let giornoIn = arrDataIn[0]
                        let meseIn = arrDataIn[1]
                        let annoIn = arrDataIn[2]
                        let giornoOut = arrDataOut[0]
                        let meseOut = arrDataOut[1]
                        let annoOut = arrDataOut[2]
                        var oraOpen = ""
                        var oraClose = ""
                        
                        if (self.arrayOre[0] == "1") {
                            
                            oraOpen = self.arrayOre[1]
                            oraClose = self.arrayOre[2]
                            
                            
                        } else {
                            
                            oraOpen = self.arrayOre[1]
                            oraClose = self.arrayOre[4]
                            
                        }
                        let dataIn = giornoIn + "-" + meseIn + "-" + annoIn + " " + oraOpen
                        print(dataIn)
                        let dataOut = giornoOut + "-" + meseOut + "-" + annoOut + " " + oraClose
                        let newdataIn = dateFormatter.date(from: dataIn)
                        let newdataOut = dateFormatter.date(from: dataOut)
                        self.datePicker.minimumDate = newdataIn
                        self.datePicker.maximumDate = newdataOut
                    }
                }
            }
            
            
            let alertController = UIAlertController(title: "Selezione Date", message:"\n\n\n\n\n\n\n\n " , preferredStyle: UIAlertControllerStyle.actionSheet)
            self.datePicker.frame = CGRect(x: alertController.view.frame.width/2 - 150, y: 20, width: 300, height: 200)
            alertController.view.addSubview(self.datePicker)
            alertController.view.frame = CGRect(x: 0, y: 0, width: 300, height: 200)
            alertController.view.bounds = CGRect(x: 0, y: 0, width: 300, height: 200)
            
            let cancelAction = UIAlertAction(title: "Cancella", style: .destructive) { (action) in
                
                tableView.deselectRow(at: indexPath, animated: true)
                self.arraySelected.removeAll()
                
            }
            
            let confirmAction = UIAlertAction(title: "Conferma", style: UIAlertActionStyle.cancel, handler: {
                (action : UIAlertAction!) -> Void in
                
                let dateFormatter = DateFormatter()
                dateFormatter.dateFormat = "dd-MM-yyyy HH:mm"
                let selectedDate = dateFormatter.string(from: self.datePicker.date)
                print(selectedDate)
                let book = PFObject(className:"InBooking")
                book["userId"] = PFUser.current()?.objectId
                let stringa = selectedDate.replacingOccurrences(of: "-", with: "/")
                book["date"] = stringa.replacingOccurrences(of: " ", with: "*")
                book["hotelId"] = self.hotelId
                book["serviceId"] = userDefaults.string(forKey: "idService")!
                book["state"] = "p"
                let servizio = self.arrayServizi[indexPath.row]
                let arrayServizio = servizio.components(separatedBy: "_")
                let oggetto = arrayServizio[1]
                let prezzo = arrayServizio[2]
                let stri = "2@" + oggetto + "_" + prezzo + "_1"
                
                book["subServices"] = stri
                book.saveInBackground()
                self.tableView.deselectRow(at: indexPath, animated: true)
                /*for o in self.arraySelected {
                    let split = o.components(separatedBy: "x")
                    let indice = Int(split[1])!
                    let index : NSIndexPath = IndexPath(item: indice, section: 0) as NSIndexPath
                    print(index)
                    self.tableView.deselectRow(at: index as IndexPath, animated: true)
                    self.tableView.cellForRow(at: index as IndexPath)?.accessoryType = UITableViewCellAccessoryType.none
                }*/
                
                self.arraySelected.removeAll()
                self.notificationManager.showNotification(title: "Congratulazioni", body: "Servizio Richiesto", onTap: nil)
                AudioServicesPlayAlertSound(SystemSoundID(kSystemSoundID_Vibrate))
            })
            
            
            alertController.addAction(cancelAction)
            alertController.addAction(confirmAction)
            self.present(alertController, animated: true, completion: nil)
            
        })
            
        }
    }
    
    
    
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrayServizi.count
    }
    
    
}

