//
//  historyController.swift
//  cierge
//
//  Created by Alberto Cesare Barbon on 03/03/17.
//  Copyright © 2017 Alberto Cesare Barbon. All rights reserved.
//

import UIKit
import Parse
import LNRSimpleNotifications
import AudioToolbox

class historyController: UIViewController, UITableViewDelegate, UITableViewDataSource {
    
    var tableView = UITableView()
    var array : [PFObject] = []
    let notificationManager = LNRNotificationManager()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        notificationManager.notificationsPosition = LNRNotificationPosition.top
        notificationManager.notificationsBackgroundColor = UIColor.init(red: 205/255, green: 89/255, blue: 91/255, alpha: 1)
        notificationManager.notificationsTitleTextColor = UIColor.white
        notificationManager.notificationsBodyTextColor = UIColor.white
        notificationManager.notificationsSeperatorColor = UIColor.clear
        
        tableView.delegate = self
        tableView.dataSource = self
        tableView.frame = view.frame
        self.tableView.tableFooterView = UIView()
        view.addSubview(tableView)
        query()
    }
    
    
    override func viewDidAppear(_ animated: Bool) {
        query()
        print("Appear History")
    }
    
    func query() {
    
        array = []
        let userId = PFUser.current()?.objectId
        let query = PFQuery(className: "UserCheckin")
        //query.cachePolicy = .cacheThenNetwork
        query.whereKey("userId", equalTo: userId!)
        query.whereKey("state", equalTo: "a")
        
        query.findObjectsInBackground { (objects, error) -> Void in
            
            
            
            for object in objects! {
                
                let formatter = DateFormatter()
                formatter.dateFormat = "dd-MM-yyyy"
                let checkIn = object["checkIndate"] as! String
                let date = formatter.date(from: checkIn)
                let now = NSDate()
                if objects?.count == 0 {
                    
                    let textView : UITextView = UITextView()
                    textView.frame = CGRect(x: 20, y: self.view.frame.height / 2 - 50, width: self.view.frame.width - 40, height: 100)
                    textView.textAlignment = .center
                    textView.text = "In questa pagina visualizzerai le prenotazioni raggruppate in base ai soggiorni in corso e passati"
                    textView.font = UIFont.systemFont(ofSize: 15)
                    textView.textColor = .darkGray
                    self.view.addSubview(textView)
                }
                if (date! < now as Date) {
                    
                    self.array.append(object)
                    self.tableView.reloadData()
                    
                }
                
            }
            
        }
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = UITableViewCell()
        cell.accessoryType = .disclosureIndicator
        cell.backgroundColor = UIColor.init(red: 250/255, green: 250/255, blue: 250/255, alpha: 1)
        
        let pfObj = array[indexPath.row]
        
        let title = UILabel(frame: CGRect(x: 15, y: 15, width: cell.frame.width/2 - 5, height: 20))
        let hotelId = pfObj["hotelId"]
        let query = PFQuery(className: "Hotels")
        query.getObjectInBackground(withId: hotelId as! String, block: { (object, error) -> Void in
            
            let name = object?["name"] as! String
            title.text = name
            cell.addSubview(title)
            
        })
        
        
        let checkin = pfObj["checkIndate"] as! String
        let checkout = pfObj["checkOutDate"] as! String
        
        let formatter = DateFormatter()
        formatter.dateFormat = "dd-MM-yyyy"
        let checkindate1 = formatter.date(from: checkin)
        let checkoutdate1 = formatter.date(from: checkout)
        
        let formattazione = DateFormatter()
        formattazione.dateFormat = "dd MMM yyyy"
        let checkindate2 = formattazione.string(from: checkindate1!)
        let checkoutdate2 = formattazione.string(from: checkoutdate1!)
        
        let labelDate = UILabel(frame: CGRect(x: 15, y: 15 + 20 + 5, width: cell.frame.width - 15, height: 20))
        labelDate.text = "in: " + checkindate2 + "  -  out: " + checkoutdate2
        labelDate.textColor = .gray
        labelDate.font = UIFont.systemFont(ofSize: 14)
        
        cell.addSubview(labelDate)
        
        
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return array.count
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 70
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        
        self.tableView.reloadData()
        self.tableView.tableFooterView = UIView()
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        /*let userDefaults = Foundation.UserDefaults.standard
         userDefaults.set("0", forKey: "numTemplate")
         
         let pfObj = array[indexPath.row]
         let idC = array[indexPath.row].objectId
         let id = pfObj["hotelId"] as! String
         
         let query = PFQuery(className:"Hotels")
         query.getObjectInBackground(withId: id, block: { (object, error) -> Void in
         
         let stato = pfObj["state"] as! String
         let userDefaults = Foundation.UserDefaults.standard
         userDefaults.set(object?.objectId, forKey: "id")
         userDefaults.set(stato, forKey: "stato")
         userDefaults.set(idC, forKey:"idCheck")
         
         print(stato)
         
         })
         
         userDefaults.set( id, forKey: "id")*/
        
        let userDefaults = Foundation.UserDefaults.standard
        userDefaults.set(array[indexPath.row].objectId, forKey: "idSubR")
        self.performSegue(withIdentifier: "segueSub1", sender: self)
        
    }
    
    
}
