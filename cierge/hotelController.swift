//
//  hotelController.swift
//  cierge
//
//  Created by Alberto Cesare Barbon on 15/01/17.
//  Copyright © 2017 Alberto Cesare Barbon. All rights reserved.
//

import UIKit
import PageMenu
import Parse

class hotelController: UIViewController, CAPSPageMenuDelegate {
    
    var pageMenu : CAPSPageMenu?
    
    @IBOutlet weak var navBar : UINavigationBar? = UINavigationBar()

    override func viewDidLoad() {
        super.viewDidLoad()

        UIApplication.shared.statusBarStyle = .lightContent
        
        
        
        var controllerArray : [UIViewController] = []
        
        let controllerMain : UIViewController = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "mainPage") as! mainPage
        controllerMain.title = "Info"
        controllerArray.append(controllerMain)
        
        let controllerInser : UIViewController = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "navController") as! navController
        controllerInser.title = "Servizi Hotel"
        controllerArray.append(controllerInser)
        
        let controllerProd : UIViewController = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "navControllerProd") as! navControllerProd
        controllerProd.title = "Prodotti"
        controllerArray.append(controllerProd)
        
        let controllerRest : UIViewController = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "navControllerRisto") as! navControllerRisto
        controllerRest.title = "Ristoranti"
        controllerArray.append(controllerRest)
        
        let controllerAct : UIViewController = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "navControllerAct") as! navControllerAct
        controllerAct.title = "Attività"
        controllerArray.append(controllerAct)
        
        let controllerPlace : UIViewController = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "navControllerPlac") as! navControllerPlac
        controllerPlace.title = "Luoghi"
        controllerArray.append(controllerPlace)
        
        let parameters: [CAPSPageMenuOption] = [
            .menuItemSeparatorWidth(0),
            .useMenuLikeSegmentedControl(false),
            .menuItemSeparatorPercentageHeight(0.1)
        ]
        pageMenu = CAPSPageMenu(viewControllers: controllerArray, frame: CGRect(x: 0, y: 74, width: self.view.frame.width, height: view.frame.height - 64), pageMenuOptions: parameters)
        
        
        pageMenu?.viewBackgroundColor = UIColor.white
        pageMenu?.selectionIndicatorColor = UIColor.yellow
        pageMenu?.selectedMenuItemLabelColor = UIColor.black
        pageMenu?.unselectedMenuItemLabelColor = UIColor.lightGray
        pageMenu?.scrollMenuBackgroundColor = UIColor.white
        pageMenu?.enableHorizontalBounce = true
        
        pageMenu!.delegate = self
        self.view.addSubview(pageMenu!.view)
        
        let userDefaults = Foundation.UserDefaults.standard
        let v  = userDefaults.string(forKey: "numTemplate")
        pageMenu?.moveToPage(Int(v!)!)
        
        /*let viewUp : UIView = UIView()
        viewUp.backgroundColor = UIColor.white
        let blur : UIVisualEffectView = UIVisualEffectView(effect: UIBlurEffect(style: .dark))
        blur.frame = CGRect(x: 0, y: 0, width: view.frame.width, height: 64)
        
        viewUp.frame = CGRect(x: 0, y: 0, width: view.frame.width, height: 64)
        view.addSubview(viewUp)
        viewUp.addSubview(blur)
        
        let titolo : UILabel = UILabel()
        titolo.frame = CGRect(x: 0, y: 30, width: viewUp.frame.width, height: 20)
        titolo.textAlignment = .center
        titolo.textColor = UIColor.white
        titolo.font = UIFont.systemFont(ofSize: 20)
        let value  = userDefaults.string(forKey: "id")
        //let stato  = userDefaults.string(forKey: "stato")
        
        let query = PFQuery(className:"Hotels")
        query.getObjectInBackground(withId: value!, block: { (object, error) -> Void in
            
            titolo.text = object?["name"] as? String
            
        })
        viewUp.addSubview(titolo)
        
        let disBt = UIButton() as UIButton
        disBt.tintColor = UIColor.white
        disBt.frame = CGRect(x: 10, y: 30, width: 20, height: 20)
        let imageM = UIImage(named: "back.png")?.maskWith(color: .white)
        disBt.setImage(imageM, for: .normal)
        disBt.addTarget(self, action: #selector(hotelController.dismiss(_:)), for: .touchUpInside)
        viewUp.addSubview(disBt)*/
        
        
        
        let value  = userDefaults.string(forKey: "id")
        let query = PFQuery(className:"Hotels")
        query.getObjectInBackground(withId: value!, block: { (object, error) -> Void in
            
            self.navBar?.titleTextAttributes = [
                NSFontAttributeName: UIFont.systemFont(ofSize: 20),
                NSForegroundColorAttributeName : UIColor.white
            ]
            
            self.navBar?.topItem?.title = object?["name"] as? String
            
        })
        
    
        
    }
    
    
    func didMoveToPage(_ controller: UIViewController, index: Int) {
        
        NotificationCenter.default.post(name: NSNotification.Name(rawValue: "cambia") , object: nil)
        
    }
    
    
    @IBAction func dismiss(_ sender: UIButton) {
        /*let vc = self.storyboard!.instantiateViewController(withIdentifier: "tabController") as! tabController
        self.present(vc, animated: true, completion: nil)*/
        dismiss(animated: true, completion: nil)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        
    }

    
}
