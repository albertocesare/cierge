//
//  restaurantsPage1.swift
//  cierge
//
//  Created by Alberto Cesare Barbon on 23/02/17.
//  Copyright © 2017 Alberto Cesare Barbon. All rights reserved.
//

import UIKit
import Parse
import ParseUI

class restaurantsPage1: PFQueryTableViewController {
    
    var array : [PFObject] = []
    

    override func queryForTable() -> PFQuery<PFObject> {
        
        //let query = PFQuery(className: "Restaurants")
        //query.cachePolicy = .cacheElseNetwork
        
        //query.whereKey("active", equalTo: true as Bool)
        navigationController?.navigationBar.isHidden = true
        
        let userDefaults = Foundation.UserDefaults.standard
        let idCheck = userDefaults.value(forKey: "idCheck")
        
        if (idCheck as! String != "none") {
            tableView.allowsSelection = true
        } else {
            tableView.allowsSelection = false
            
            self.perform(#selector(self.popUp), with: nil, afterDelay: 0.5)
        }
        
    
        let arrayid  = userDefaults.array(forKey: "arrayRist")
        let query = PFQuery(className: "Restaurants")
        query.cachePolicy = .cacheElseNetwork
        query.whereKey("objectId", containedIn: arrayid!)
        
        
        return query
        
    }
    
    func popUp () {
        let alertController = UIAlertController(title: "Attenzione", message: "Puoi usufruire del servizio solo dopo aver ricevuto conferma di check-in da parte dell'hotel", preferredStyle: UIAlertControllerStyle.alert)
        
        let saveAction = UIAlertAction(title: "Ok", style: UIAlertActionStyle.cancel, handler: {
            alert -> Void in
            
            
        })
        
        
        
        alertController.addAction(saveAction)
        self.present(alertController, animated: true, completion: nil)
    }
    
    
    
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath, object: PFObject?) -> PFTableViewCell? {
        
        array.append(object!)
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! RistoTableViewCell
        
        let imageFile = object?.object(forKey: "photo") as? PFFile
        cell.cellImageView.backgroundColor = .clear //placeholder
        
        cell.cellImageView.file = imageFile
        cell.cellImageView.loadInBackground()
        
        cell.titleLabel.text = object?["name"] as? String
        
        return cell
        
        
    }
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        return 220
        
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        print("ciao")
        
        let userDefaults = Foundation.UserDefaults.standard
        let idCheck = userDefaults.value(forKey: "idCheck")
        
        if (String(describing: idCheck) != "none") {
        tableView.allowsSelection = true
        let pfObj = array[indexPath.row]
        
        
        
        let userDefaults = Foundation.UserDefaults.standard
        userDefaults.set( pfObj.objectId, forKey: "idService")
        

        tableView.deselectRow(at: indexPath, animated: true)
        //performSegue(withIdentifier: "ristoSegue", sender: self)
            
        
        } else {
            tableView.allowsSelection = false
            tableView.deselectRow(at: indexPath, animated: true)
            let alertController = UIAlertController(title: "Attenzione", message: "Puoi usufruire del servizio solo dopo aver ricevuto conferma di check-in da parte dell'hotel", preferredStyle: UIAlertControllerStyle.alert)
            
            let saveAction = UIAlertAction(title: "Ok", style: UIAlertActionStyle.cancel, handler: {
                alert -> Void in
                
                
            })
            
            alertController.addAction(saveAction)
            self.present(alertController, animated: true, completion: nil)
        }
        
    }
    
    
    
}
