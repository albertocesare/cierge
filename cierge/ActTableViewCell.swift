//
//  ActTableViewCell.swift
//  cierge
//
//  Created by Alberto Cesare Barbon on 24/02/17.
//  Copyright © 2017 Alberto Cesare Barbon. All rights reserved.
//

import UIKit
import Parse
import ParseUI

class ActTableViewCell: PFTableViewCell {

    @IBOutlet weak var cellImageView: PFImageView!
    @IBOutlet weak var titleLabel: UILabel!
}
