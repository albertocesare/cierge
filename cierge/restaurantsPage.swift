//
//  restaurantsPage.swift
//  cierge
//
//  Created by Alberto Cesare Barbon on 17/01/17.
//  Copyright © 2017 Alberto Cesare Barbon. All rights reserved.
//

import UIKit
import Parse

class restaurantsPage: UIViewController, UITableViewDelegate, UITableViewDataSource {
    
    var tableView : UITableView = UITableView()
    var arrayPFObj : [PFObject] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        tableView.frame = CGRect(x: 0, y: 0, width: view.frame.width, height: view.frame.height - 104)
        tableView.delegate = self
        tableView.dataSource = self
        self.tableView.register(UINib(nibName: "TableViewCell", bundle: nil), forCellReuseIdentifier: "cell")
        view.addSubview(tableView)
        
        let userDefaults = Foundation.UserDefaults.standard
        let value  = userDefaults.string(forKey: "id")
        
        let query = PFQuery(className:"Restaurants")
        query.findObjectsInBackground { (objects, error) -> Void in
            
            for object in objects! {
                
                /*let servizio = object["hotelId"] as! String
                let tipo = object["type"] as! String
                
                if (servizio == value!) {
                    if (tipo == "r") {
                        
                        
                        let idAct = object["serviceId"] as! String
                        
                        let query1 = PFQuery(className:"Restaurants")
                        query1.getObjectInBackground(withId: idAct, block: { (objectA, error) -> Void in
                            
                            self.arrayPFObj.append(objectA!)
                            self.tableView.reloadData()
                            print(objectA!)
                            
                        })
                        
                    }
                }*/
                
                if (object["active"] as! Bool == true) {
                    self.arrayPFObj.append(object)
                    self.tableView.reloadData()
                    print(object)
                }
                
            }
        }
        
    }
    
    internal func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! TableViewCell
        let pfObj = arrayPFObj[indexPath.row]
        let nome = pfObj["name"] as! String
        
        cell.nomeLabel.text = nome
        
        let objid = pfObj.objectId
        let query = PFQuery(className:"Restaurants")
        query.getObjectInBackground(withId: objid!, block: { (object, error) -> Void in
            
            let foto = object?["photo"] as! PFFile
            foto.getDataInBackground { (imageData, error) -> Void in
                if (error == nil) {
                    if (imageData != nil) {
                        cell.imgView.image = UIImage(data:imageData!)
                    }
                }
            }
            
        })
        
        return cell
        
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let pfObj = arrayPFObj[indexPath.row]
        let userDefaults = Foundation.UserDefaults.standard
        userDefaults.set( pfObj.objectId, forKey: "idService")
        let value  = userDefaults.string(forKey: "id")
        let userId = PFUser.current()?.objectId
        
        let query = PFQuery(className: "UserCheckin")
        query.findObjectsInBackground { (objects, error) -> Void in
            
            for object in objects! {
                if (object["hotelId"] as? String == value) && (object["state"] as! String == "a") && (object["userId"] as? String == userId) {
        let vc = self.storyboard!.instantiateViewController(withIdentifier: "ristoController") as! ristoController
        self.present(vc, animated: true, completion: nil)
                
                    break
                }
            }
        }
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return self.arrayPFObj.count
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 220
    }
    
}
