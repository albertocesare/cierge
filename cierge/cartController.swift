//
//  cartController.swift
//  cierge
//
//  Created by Alberto Cesare Barbon on 02/02/17.
//  Copyright © 2017 Alberto Cesare Barbon. All rights reserved.
//

import UIKit
import Parse
import LNRSimpleNotifications
import AudioToolbox

class cartController: UIViewController, UITableViewDelegate, UITableViewDataSource {
    
    let items = ["Acquistati", "Carrello"]
    var segment : UISegmentedControl = UISegmentedControl()
    
    var tableView : UITableView = UITableView()
    
    var arrayPFObjA : [PFObject] = []
    var arrayPFObjP : [PFObject] = []
    
    var tableNumber = 0
    
    var labelTot : UILabel = UILabel()
    var viewLinea : UIView = UIView()
    let payBt : UIButton = UIButton()
    
    let notificationManager = LNRNotificationManager()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        UIApplication.shared.statusBarStyle = .default
        
        notificationManager.notificationsPosition = LNRNotificationPosition.top
        notificationManager.notificationsBackgroundColor = UIColor.init(red: 205/255, green: 89/255, blue: 91/255, alpha: 1)
        notificationManager.notificationsTitleTextColor = UIColor.white
        notificationManager.notificationsBodyTextColor = UIColor.white
        notificationManager.notificationsSeperatorColor = UIColor.clear
        
        tableView.delegate = self
        tableView.dataSource = self
        
        let viewU : UIView = UIView(frame: CGRect(x: 0, y: 0, width: view.frame.width, height: 64))
        viewU.backgroundColor = UIColor.init(red: 239/255, green: 239/255, blue: 239/255, alpha: 1)
        view.addSubview(viewU)
        
        let label : UILabel = UILabel(frame: CGRect(x: 20, y: 30, width: view.frame.width-40, height: 20))
        label.textAlignment = .center
        label.text = "Carrello"
        label.font = UIFont(name: "HelveticaNeue", size: 20)
        viewU.addSubview(label)
        
        segment = UISegmentedControl(items: items)
        segment.frame = CGRect(x: 30, y: 79, width: view.frame.width - 60, height: 25)
        segment.addTarget(self, action: #selector(changeColor), for: .valueChanged)
        segment.tintColor = UIColor.init(red: 213/255, green: 190/255, blue: 111/255, alpha: 1)
        segment.selectedSegmentIndex = 0
        view.addSubview(segment)
        
        
        
        tableView.frame = CGRect(x: 0, y: segment.frame.maxY + 20, width: view.frame.width, height: view.frame.height - segment.frame.maxY)
        self.tableView.register(UINib(nibName: "CheckInCell", bundle: nil), forCellReuseIdentifier: "cell")
        view.addSubview(tableView)
        
        let viewU10 : UIView = UIView(frame: CGRect(x: 0, y: tableView.frame.minY - 1, width: self.view.frame.width, height: 1))
        viewU10.backgroundColor = UIColor.init(red: 239/255, green: 239/255, blue: 239/255, alpha: 1)
        view.addSubview(viewU10)
        
        
        let query = PFQuery(className: "PurchasingCart")
        query.findObjectsInBackground { (objects, error) -> Void in
         
            
            
            for object in objects! {
                
                let name = object["userId"] as! String
                let stato = object["state"] as! String
                let userId = PFUser.current()?.objectId
                
                if (name == userId) {
                    
                    if (stato == "a")
                    {
                        
                        self.arrayPFObjA.append(object)
                        print(self.arrayPFObjA)
                        self.tableView.reloadData()
                        
                    } else {
                        
                        self.arrayPFObjP.append(object)
                        self.tableView.reloadData()
                        print(self.arrayPFObjP)
                        
                    }
                    
                }
                
            }
            
            
        }
        
        
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if(tableNumber == 0) {
            return arrayPFObjA.count
        }
            
        else { return arrayPFObjP.count}
    }
    
    var price = 0
    
    internal func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        
        
        
        if (tableNumber == 1) && (arrayPFObjP.count != 0) {
            
            viewLinea.frame = CGRect(x: 20, y: view.frame.height - 20, width: 230, height: 1)
            viewLinea.backgroundColor = UIColor.init(red: 200/255, green: 200/255, blue: 200/255, alpha: 1)
            view.addSubview(viewLinea)
            view.bringSubview(toFront: viewLinea)
            labelTot.frame = CGRect(x: 20, y: viewLinea.frame.minY - 20, width: 230, height: 20)
            labelTot.textAlignment = .left
            labelTot.textColor = .lightGray
            view.addSubview(labelTot)
            view.bringSubview(toFront: labelTot)
            
            
            payBt.frame = CGRect(x: view.frame.width - 70, y: view.frame.height - 70, width: 50, height: 50)
            payBt.layer.cornerRadius = 25
            payBt.backgroundColor = UIColor.init(red: 213/255, green: 190/255, blue: 111/255, alpha: 1)
            payBt.setTitle("Paga", for: .normal)
            payBt.isHidden = false
            payBt.isEnabled = true
            payBt.addTarget(self, action: #selector(cartController.pay(_:)), for: .touchUpInside)
            view.addSubview(payBt)
            
        } else {
            
            viewLinea.removeFromSuperview()
            labelTot.removeFromSuperview()
            payBt.removeFromSuperview()
        }
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! CheckInCell
        
        cell.backgroundColor = UIColor.white
        let viewCell: UIView = UIView()
        viewCell.frame = cell.frame
        viewCell.backgroundColor = .white
        cell.selectedBackgroundView = viewCell
        
        cell.addSubview(viewCell)
        
        let imageView : UIImageView = UIImageView()
        imageView.frame = CGRect(x: 0, y: 0, width: cell.frame.height / 3 * 4 + 10, height: cell.frame.height)
        cell.addSubview(imageView)
        
        let nomeHotel : UILabel = UILabel()
        nomeHotel.frame = CGRect(x: cell.frame.height / 3 * 4 + 20, y: 0, width: cell.frame.width - imageView.frame.maxX - 20, height: 20)
        nomeHotel.backgroundColor = UIColor.white
        cell.addSubview(nomeHotel)
        
        let città : UILabel = UILabel()
        città.frame = CGRect(x: cell.frame.height / 3 * 4 + 20, y: 25, width: cell.frame.width - imageView.frame.maxX - 20, height: 20)
        città.textColor = UIColor.init(red: 84/255, green: 191/255, blue: 248/255, alpha: 1)
        città.backgroundColor = UIColor.white
        cell.addSubview(città)
        
        let viewC : UIView = UIView()
        viewC.frame = CGRect(x: cell.frame.width - 10, y: 0, width: 10, height: cell.frame.height)
        cell.addSubview(viewC)
        
        var pfObj : PFObject?
        if (tableNumber == 0)
        {
            
            pfObj = arrayPFObjA[indexPath.row]
            
        } else {
            
            pfObj = arrayPFObjP[indexPath.row]
            
            
        }
        
        let state = pfObj?["state"] as! String
        
        if (state == "a") {
            
            viewC.backgroundColor = UIColor.init(red: 109/255, green: 213/255, blue: 154/255, alpha: 1)
            
        } else if (state == "p") {
            
            viewC.backgroundColor = UIColor.init(red: 213/255, green: 158/255, blue: 109/255, alpha: 2)
            
        } else {
            
            viewC.backgroundColor = UIColor.init(red: 205/255, green: 205/255, blue: 205/255, alpha: 1)
            
        }
        let hotelId = pfObj?["productId"] as! String
        let query = PFQuery(className:"Products")
        query.getObjectInBackground(withId: hotelId, block: { (object, error) -> Void in
            
            let foto = object?["photo"] as! PFFile
            foto.getDataInBackground { (imageData, error) -> Void in
                if (error == nil) {
                    if (imageData != nil) {
                        imageView.image = UIImage(data:imageData!)
                    }
                }
            }
            nomeHotel.text = object?["name"] as? String
            //città.text = object?["city"] as? String
            
            let prezzo = Int(object?["price"] as! String)
            if (self.tableNumber == 1) {
            
                self.price = Int(self.price) + prezzo!
                self.labelTot.text = "Total: " + String(self.price) + " €"
            }
            
            let ownerId = object?["ownerId"] as! String
            let q = PFQuery(className:"Hotels")
            q.getObjectInBackground(withId: ownerId, block: { (ob, error) -> Void in
                
                città.text = ob?["name"] as? String
                
            })
            
        })
        
        
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 70
    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        

        self.tableView.reloadData()
        self.tableView.tableFooterView = UIView()
    }
    
    
    func changeColor(sender: UISegmentedControl) {
        switch sender.selectedSegmentIndex {
        case 0:
            
            tableNumber = 0
            tableView.reloadData()
            payBt.isHidden = true
            payBt.isUserInteractionEnabled = false
            viewLinea.isUserInteractionEnabled = false
            viewLinea.isHidden = true
            labelTot.isUserInteractionEnabled = false
            labelTot.isHidden = true
            break
            
        case 1:
            price = 0
            tableNumber = 1
            tableView.reloadData()
            payBt.isHidden = false
            payBt.isUserInteractionEnabled = true
            viewLinea.isUserInteractionEnabled = true
            viewLinea.isHidden = false
            labelTot.isUserInteractionEnabled = true
            labelTot.isHidden = false
            break
            
        default: break
            
        }
        
    }
    
    func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        return true
    }
    
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
        
        var id = ""
        
        notificationManager.showNotification(title: "Cancellazione...", body: "Richiesta Inviata", onTap: nil)
        AudioServicesPlayAlertSound(SystemSoundID(kSystemSoundID_Vibrate))
        
        
        if (tableNumber == 0) {
            
            id = arrayPFObjA[indexPath.row].objectId!
            
        } else {
            
            id = arrayPFObjP[indexPath.row].objectId!
            
        }
        
        let query = PFQuery(className:"PurchasingCart")
        query.getObjectInBackground(withId: id, block: { (object, error) -> Void in
            
            object?.deleteEventually()
            tableView.reloadData()
            self.labelTot.text = "Totale: 0€"
            self.price = 0
            
            if (self.tableNumber == 0) {
                
                self.arrayPFObjA.remove(at: indexPath.row)
                
            } else {
                
                self.arrayPFObjP.remove(at: indexPath.row)
                
            }
            
            tableView.reloadData()
            
        })
        
    }
    
    
    func pay(_ button: UIButton) {
     
        for object in arrayPFObjP {
            
            let pfObj = PFObject(className: "Purchasing")
            pfObj["userId"] = object["userId"]
            pfObj["productId"] = object["productId"]
            pfObj["method"] = object["method"]
            pfObj["qty"] = object["qty"]
            pfObj["sellerId"] = object["sellerId"]
            pfObj["state"] = object["state"]
            pfObj.saveInBackground()
            
        }
        
    }
    

}
