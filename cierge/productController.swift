//
//  productController.swift
//  cierge
//
//  Created by Alberto Cesare Barbon on 27/01/17.
//  Copyright © 2017 Alberto Cesare Barbon. All rights reserved.
//

import UIKit
import Parse

class productController: UIViewController, UIScrollViewDelegate {
    
    var scrollView : UIScrollView = UIScrollView()
    var imageView : UIImageView = UIImageView()
    var titoloUp : UILabel = UILabel()
    
    var image : UIImageView = UIImageView()
    var sottotitolo : UILabel = UILabel()
    var sottotitolo1 : UILabel = UILabel()
    var titolo : UILabel = UILabel()
    var descri : UITextView = UITextView()
    var range : UILabel = UILabel()
    var payBt : UIButton = UIButton()
    var pickUp : UIButton = UIButton()

    override func viewDidLoad() {
        super.viewDidLoad()
        
        scrollView.delegate = self
        
        let userDefaults = Foundation.UserDefaults.standard
        let valuee  = userDefaults.string(forKey: "idCheck")
        print(valuee!)
        
        if (valuee == "none") {
            
            payBt.isHidden = true
            payBt.isUserInteractionEnabled = false
            pickUp.isHidden = true
            pickUp.isUserInteractionEnabled = false
            
        } else {
            
            payBt.isHidden = false
            payBt.isUserInteractionEnabled = true
            pickUp.isHidden = false
            pickUp.isUserInteractionEnabled = true
            
        }
        
        NotificationCenter.default.addObserver(self, selector: #selector(productController.pop2), name: NSNotification.Name.init(rawValue: "cambia"), object: nil)

        scrollView.frame = view.frame
        imageView.frame = CGRect(x: 0, y: 0, width: view.frame.width, height: 200)
        
        view.frame = CGRect(x: 0, y: 0, width: view.frame.width, height: view.frame.height - (110))
        scrollView.frame = view.frame
        
        let value  = userDefaults.string(forKey: "idService")
        let query = PFQuery(className:"Products")
        
        query.getObjectInBackground(withId: value!, block: { (object, error) -> Void in
            
            let foto = object?["photo"] as! PFFile
            foto.getDataInBackground { (imageData, error) -> Void in
                if (error == nil) {
                    if (imageData != nil) {
                        self.imageView.image = UIImage(data:imageData!)
                    }
                }
            }
            
            self.titoloUp.text = object?["name"] as? String
            self.titolo.text = object?["name"] as? String
            self.sottotitolo1.text = ((object?["info"] as? String)!.components(separatedBy: "*"))[0]
            print(self.sottotitolo1.text!)
            self.descri.text = ((object?["info"] as? String)!.components(separatedBy: "*"))[1]
            self.range.text = (object?["price"] as? String)! + " €"
        
        })
        
        scrollView.addSubview(imageView)
        view.addSubview(scrollView)
        
        sottotitolo1.frame = CGRect(x: 20, y: 270, width: view.frame.width - 40, height: 20)
        sottotitolo1.textColor = .lightGray
        sottotitolo1.textAlignment = .center
        scrollView.addSubview(sottotitolo1)
        
        titolo.frame = CGRect(x: 20, y: sottotitolo1.frame.maxY + 20, width: view.frame.width - 40, height: 30)
        titolo.textAlignment = .center
        titolo.font = UIFont.systemFont(ofSize: 30)
        scrollView.addSubview(titolo)
        
        range.frame = CGRect(x: 20, y: titolo.frame.maxY + 15, width: view.frame.width - 40, height: 20)
        range.textAlignment = .center
        range.font = UIFont.systemFont(ofSize: 25)
        range.textColor = .lightGray
        scrollView.addSubview(range)
        
        descri.frame = CGRect(x: 20, y: range.frame.maxY + 15, width: view.frame.width - 40, height: 250)
        descri.textColor = .lightGray
        scrollView.addSubview(descri)
        
        pickUp.frame = CGRect(x: 20, y: descri.frame.maxY + 15, width: view.frame.width - 40, height: 20)
        pickUp.setTitle("Pick up at Hotel", for: .normal)
        pickUp.setTitleColor(UIColor.init(red: 77/255, green: 144/255, blue: 248/255, alpha: 1) , for: .normal)
        pickUp.addTarget(self, action: #selector(productController.pick(_:)), for: .touchUpInside)
        scrollView.addSubview(pickUp)
        
        payBt.frame = CGRect(x: view.frame.width - 60, y: view.frame.height - 60, width: 40, height: 40)
        payBt.layer.cornerRadius = 20
        payBt.backgroundColor = UIColor.init(red: 213/255, green: 190/255, blue: 111/255, alpha: 1)
        payBt.setImage(UIImage(named: "piu.png"), for: .normal)
        payBt.addTarget(self, action: #selector(productController.pay(_:)), for: .touchUpInside)
        view.addSubview(payBt)
        
        scrollView.contentSize = CGSize(width: view.frame.width, height: pickUp.frame.maxY + 20)
        
        let btBack : UIButton = UIButton(frame: CGRect(x: 5, y: scrollView.frame.maxY - 50, width: 150, height: 30))
        btBack.setImage(UIImage(named:"cierge-trasp-1.png"), for: .normal)
        btBack.layer.cornerRadius = 15
        btBack.clipsToBounds = true
        btBack.setTitle("Back", for: .normal)
        btBack.setTitleColor(.white, for: .normal)
        btBack.contentHorizontalAlignment = .left
        btBack.addTarget(self, action: #selector(productController.pop2), for: .touchUpInside)
        //view.addSubview(btBack)
        let labelBack : UILabel = UILabel(frame: CGRect(x: 15, y: scrollView.frame.maxY - 50 + 15 - 10, width: 40, height: 20))
        labelBack.text = "Back"
        labelBack.textColor = .white
        labelBack.textAlignment = .center
        //view.addSubview(labelBack)
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        if scrollView.contentOffset.y < -30 {
            self.navigationController?.popToRootViewController(animated: true)
        }
    }
    
    func pop2() {
        _ = navigationController?.popViewController(animated: true)
    }
    
    func dismiss(_ sender: UIButton) {
        let id = "2"
        let userDefaults = Foundation.UserDefaults.standard
        userDefaults.set( id, forKey: "numTemplate")
        let vc = self.storyboard!.instantiateViewController(withIdentifier: "hotelController") as! hotelController
        self.present(vc, animated: true, completion: nil)
    }
    
    func pick(_ sender: UIButton) {
        let userDefaults = Foundation.UserDefaults.standard
        let value  = userDefaults.string(forKey: "idService")
        
        let alertController = UIAlertController(title: "Inserisci quantità", message: "", preferredStyle: UIAlertControllerStyle.alert)
        
        let saveAction = UIAlertAction(title: "Conferma", style: UIAlertActionStyle.default, handler: {
            alert -> Void in
            
            let firstTextField = alertController.textFields![0] as UITextField
            firstTextField.keyboardType = .numberPad
            let quantita = firstTextField.text
            let pfObj = PFObject(className:"PurchasingCart")
            
            let query = PFQuery(className:"Products")
            
            query.getObjectInBackground(withId: value!, block: { (object, error) -> Void in
                
                
                pfObj["state"] = "p"
                pfObj["qty"] = quantita
                pfObj["productId"] = value
                pfObj["sellerId"] = object?["sellerId"] as? String
                pfObj["userId"] = PFUser.current()?.objectId
                pfObj["method"] = "p"
                pfObj.saveInBackground()
                
            })
            
            
            
            
        })
        
        let cancelAction = UIAlertAction(title: "Annulla", style: UIAlertActionStyle.destructive, handler: {
            (action : UIAlertAction!) -> Void in
            
            
        })
        
        alertController.addTextField { (textField : UITextField!) -> Void in
            textField.text = "1"
            textField.textAlignment = .center
            textField.keyboardType = .numberPad
            //textField.backgroundColor = UIColor.init(red: 239/255, green: 239/255, blue: 239/255, alpha: 1)
            textField.layer.cornerRadius = 5
            textField.borderStyle = .none
            textField.layer.borderWidth = 0
            textField.keyboardType = .numberPad
            textField.clipsToBounds = true
        }
        
        
        alertController.addAction(saveAction)
        alertController.addAction(cancelAction)
        
        self.present(alertController, animated: true, completion: nil)
        
    }
    
    

    func pay(_ button: UIButton) {
        
        NotificationCenter.default.post(name: NSNotification.Name(rawValue: "reload1") , object: nil)
            
        let userDefaults = Foundation.UserDefaults.standard
        let value  = userDefaults.string(forKey: "idService")
        
        let alertController = UIAlertController(title: "Inserisci quantità", message: "", preferredStyle: UIAlertControllerStyle.alert)
        
        let saveAction = UIAlertAction(title: "Conferma", style: UIAlertActionStyle.default, handler: {
            alert -> Void in
            
            let firstTextField = alertController.textFields![0] as UITextField
            let quantita = firstTextField.text
            let pfObj = PFObject(className:"Purchasing")
            
            let query = PFQuery(className:"Products")
            
            query.getObjectInBackground(withId: value!, block: { (object, error) -> Void in
                
                
                pfObj["state"] = "p"
                pfObj["qty"] = quantita
                pfObj["productId"] = value
                pfObj["sellerId"] = object?["sellerId"] as? String
                pfObj["userId"] = PFUser.current()?.objectId
                pfObj["method"] = "d"
                pfObj.saveInBackground()
                
            })
            
        })
        
        let cancelAction = UIAlertAction(title: "Annulla", style: UIAlertActionStyle.destructive, handler: {
            (action : UIAlertAction!) -> Void in
            
            
        })
        
        alertController.addTextField { (textField : UITextField!) -> Void in
            textField.text = "1"
            textField.textAlignment = .center
            //textField.backgroundColor = UIColor.init(red: 239/255, green: 239/255, blue: 239/255, alpha: 1)
            textField.layer.cornerRadius = 5
            textField.borderStyle = .none
            textField.layer.borderWidth = 0
        }
        
        
        alertController.addAction(saveAction)
        alertController.addAction(cancelAction)
        
        self.present(alertController, animated: true, completion: nil)
            
        
        
    }
    
    
    
    


}
