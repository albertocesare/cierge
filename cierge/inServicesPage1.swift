//
//  inServicesPage1.swift
//  cierge
//
//  Created by Alberto Cesare Barbon on 23/02/17.
//  Copyright © 2017 Alberto Cesare Barbon. All rights reserved.
//

import UIKit
import Parse
import ParseUI

class inServicesPage1: PFQueryTableViewController {
    
    var array : [PFObject] = []
    
    override func queryForTable() -> PFQuery<PFObject> {
        
        let userDefaults = Foundation.UserDefaults.standard
        let idCheck = userDefaults.value(forKey: "idCheck")
        
        if (idCheck as! String != "none") {
            tableView.allowsSelection = true
        } else {
            tableView.allowsSelection = false
            
            self.perform(#selector(self.popUp), with: nil, afterDelay: 0.5)
        }
        
        let query = PFQuery(className: "InServices")
        query.cachePolicy = .cacheElseNetwork
        let value  = userDefaults.string(forKey: "id")
        query.whereKey("hotelId", equalTo: value!)
        navigationController?.navigationBar.isHidden = true
        
        
        return query
        
    }
    
    func popUp () {
        let alertController = UIAlertController(title: "Attenzione", message: "Puoi usufruire del servizio solo dopo aver ricevuto conferma di check-in da parte dell'hotel", preferredStyle: UIAlertControllerStyle.alert)
        
        let saveAction = UIAlertAction(title: "Ok", style: UIAlertActionStyle.cancel, handler: {
            alert -> Void in
            
            
        })
        
        
        
        alertController.addAction(saveAction)
        self.present(alertController, animated: true, completion: nil)
    }
    
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath, object: PFObject?) -> PFTableViewCell? {
        
        array.append(object!)
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! BaseTableViewCell
        
        let imageFile = object?.object(forKey: "photo") as? PFFile
        cell.cellImageView.backgroundColor = .clear //placeholder
        
        cell.cellImageView.file = imageFile
        cell.cellImageView.loadInBackground()
        
        let objectId = object?.objectId
        let query = PFQuery(className: "InServices")
        query.getObjectInBackground(withId: objectId!, block: { (object, error) -> Void in
            
            cell.titleLabel.text = (object?["info"] as? String)?.components(separatedBy: "@")[1].components(separatedBy: "*")[0]
            
        })
        
        return cell
        
        
    }
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        return 220
        
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        
        let pfObj = array[indexPath.row]
        let info = pfObj["info"] as! String
        let infoArr = info.components(separatedBy: "@")
        let numTIpo = infoArr[0]
        
        let userDefaults = Foundation.UserDefaults.standard
        userDefaults.set( pfObj.objectId, forKey: "idService")
        let value  = userDefaults.string(forKey: "id")
        let userId = PFUser.current()?.objectId
        
        let idCheck = userDefaults.value(forKey: "idCheck")
        print(idCheck!)
        
        if (idCheck as! String == "none") {
            tableView.allowsSelection = false
            tableView.deselectRow(at: indexPath, animated: true)
            let alertController = UIAlertController(title: "Attenzione", message: "Puoi usufruire del servizio solo dopo aver ricevuto conferma di check-in da parte dell'hotel", preferredStyle: UIAlertControllerStyle.alert)
            
            let saveAction = UIAlertAction(title: "Ok", style: UIAlertActionStyle.cancel, handler: {
                alert -> Void in
                
                
            })
            
            alertController.addAction(saveAction)
            self.present(alertController, animated: true, completion: nil)
            
        } else {
        
            tableView.allowsSelection = true
        let query = PFQuery(className: "UserCheckin")
        query.findObjectsInBackground { (objects, error) -> Void in
            
            for object in objects! {
                print(object)
                if (object["hotelId"] as? String == value) && (object["state"] as! String == "a") && (object["userId"] as? String == userId) {
                    
                    
                    
                    switch numTIpo {
                    case "1":
                        tableView.deselectRow(at: indexPath, animated: true)
                        self.performSegue(withIdentifier: "checkSegue", sender: self)
                        
                        break
                        
                    case "2":
                        tableView.deselectRow(at: indexPath, animated: true)
                        self.performSegue(withIdentifier: "listSegue", sender: self)
                        
                        break
                        
                    case "3":
                        tableView.deselectRow(at: indexPath, animated: true)
                        self.performSegue(withIdentifier: "ristoSegue", sender: self)
                        
                        break
                        
                    case "4":
                        tableView.deselectRow(at: indexPath, animated: true)
                        self.performSegue(withIdentifier: "genSegue", sender: self)
                        
                        break
                        
                    default:
                        break
                    }
                    
                    
                    
                    break
                }
                
            }
            
            
            
        }
        
    }
    
        
    }
    
    

}
