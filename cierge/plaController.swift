//
//  placesController.swift
//  cierge
//
//  Created by Alberto Cesare Barbon on 01/02/17.
//  Copyright © 2017 Alberto Cesare Barbon. All rights reserved.
//

import UIKit
import Parse
import MapKit

class plaController: UIViewController, MKMapViewDelegate, UIScrollViewDelegate {
    
    var image : UIImageView = UIImageView()
    var sottotitolo : UILabel = UILabel()
    var sottotitolo1 : UILabel = UILabel()
    var titolo : UILabel = UILabel()
    var openAt : UILabel = UILabel()
    var descri : UITextView = UITextView()
    var speciality : UILabel = UILabel()
    //var range : UILabel = UILabel()
    //var payBt : UIButton = UIButton()
    var indirizzo : UILabel = UILabel()
    
    var datePicker : UIDatePicker = UIDatePicker()
    
    var mapView : MKMapView = MKMapView()
    
    var imageConc : UIImageView = UIImageView()
    var labelHotel : UILabel = UILabel()
    let nomeCon : UILabel = UILabel()
    
    var arrayOre : [String] = []
    var arrayTurni : [String] = []
    
    var scrollView : UIScrollView = UIScrollView()
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        scrollView.delegate = self
        
        NotificationCenter.default.addObserver(self, selector: #selector(checkTemplate1.pop2), name: NSNotification.Name.init(rawValue: "cambia"), object: nil)
        
        scrollView.frame = view.frame
        mapView.delegate = self
        image.frame = CGRect(x: 0, y: 0, width: view.frame.width, height: 200)
        //view.addSubview(image)
        scrollView.addSubview(image)
        
        view.frame = CGRect(x: 0, y: 0, width: view.frame.width, height: view.frame.height - (110))
        scrollView.frame = view.frame
        
        let userDefaults = Foundation.UserDefaults.standard
        let value  = userDefaults.string(forKey: "idService")
        let query = PFQuery(className:"Places")
        query.getObjectInBackground(withId: value!, block: { (object, error) -> Void in
            
            let foto = object?["photo"] as! PFFile
            foto.getDataInBackground { (imageData, error) -> Void in
                if (error == nil) {
                    if (imageData != nil) {
                        self.image.image = UIImage(data:imageData!)
                    }
                }
            }
            
            /*let inutile0 = object?["info"] as! String
             let inutile1 = inutile0.components(separatedBy: "@")
             let infosArr = inutile1[1]
             let infos = infosArr.components(separatedBy: "*")*/
            
            self.titolo.text = object?["name"] as? String
            self.indirizzo.text = object?["address"] as? String
            
            let turniarrdainfo = ((object?["info"] as? String)?.components(separatedBy: "*"))?[0]
            let turniarr = turniarrdainfo?.components(separatedBy: "_")
            self.arrayOre = turniarr!
            if (turniarr?[0] == "1") {
                //1 turno
                
                let turnoOpn = turniarr?[1]
                let turnoCls = turniarr?[2]
                
                let string = turnoOpn! + turnoCls!
                
                self.arrayTurni.append(string)
                
                self.openAt.text = "Open at: " + turnoOpn! + " - " + turnoCls!
                
            } else if (turniarr?[0] == "0") {
                
                
                self.openAt.text = " h24"
                
                
            } else {
                
                var turnoOpn = turniarr?[1]
                var turnoCls = turniarr?[2]
                self.openAt.text = "Open at: " + turnoOpn! + " - " + turnoCls!
                
                turnoOpn = turniarr?[3]
                turnoCls = turniarr?[4]
                self.openAt.text = self.openAt.text! + " | " + turnoOpn! + " - " + turnoCls!
            }
            
            
            self.speciality.text = ((object?["info"] as? String)?.components(separatedBy: "*"))?[2]
            
            let query = PFQuery(className: "RestDescr")
            query.findObjectsInBackground { (objec, error) -> Void in
                
                self.titolo.frame = CGRect(x: 20, y: self.image.frame.maxY + 20, width: self.scrollView.frame.width - 40, height: 25)
                self.titolo.font = UIFont.systemFont(ofSize: 30)
                self.scrollView.addSubview(self.titolo)
                
                let imgPos : UIImageView = UIImageView()
                imgPos.image = UIImage(named: "place.png")
                imgPos.frame = CGRect(x: 20, y: self.titolo.frame.maxY + 17, width: 10, height: 15)
                self.scrollView.addSubview(imgPos)
                self.indirizzo.frame = CGRect(x: 40, y: self.titolo.frame.maxY + 15, width: self.scrollView.frame.width - 60, height: 20)
                self.indirizzo.font = UIFont.systemFont(ofSize: 14)
                self.indirizzo.textColor = UIColor.lightGray
                self.scrollView.addSubview(self.indirizzo)
                
                self.openAt.frame = CGRect(x: 40, y: self.indirizzo.frame.maxY + 15, width: self.view.frame.width - 70, height: 20)
                self.openAt.textColor = .lightGray
                self.openAt.font = UIFont.systemFont(ofSize: 14)
                self.scrollView.addSubview(self.openAt)
                let imageOrologio : UIImageView = UIImageView(image: UIImage(named: "time.png"))
                imageOrologio.frame = CGRect(x: 20, y: self.indirizzo.frame.maxY + 17.5, width: 15, height: 15)
                self.scrollView.addSubview(imageOrologio)
                
                /*let imagerange : UIImageView = UIImageView(image: UIImage(named: "coin.png"))
                imagerange.frame = CGRect(x: 20, y: self.openAt.frame.maxY + 15, width: 15, height: 15)
                self.scrollView.addSubview(imagerange)
                self.range.frame = CGRect(x: 40, y: imagerange.frame.minY - 2.5, width: self.view.frame.width - 70, height: 20)
                self.range.textColor = .lightGray
                self.range.font = UIFont.systemFont(ofSize: 14)
                self.scrollView.addSubview(self.range)*/
                
                let imagespeci : UIImageView = UIImageView(image: UIImage(named: "Star.png"))
                imagespeci.frame = CGRect(x: 20, y: self.openAt.frame.maxY + 15, width: 15, height: 15)
                self.scrollView.addSubview(imagespeci)
                self.speciality.frame = CGRect(x: 40, y: imagespeci.frame.minY - 2.5, width: self.view.frame.width - 70, height: 20)
                self.speciality.textColor = .lightGray
                self.speciality.font = UIFont.systemFont(ofSize: 14)
                self.scrollView.addSubview(self.speciality)
                
                let viewU1 : UIView = UIView(frame: CGRect(x: 20, y: self.speciality.frame.maxY + 15, width: self.view.frame.width - 40, height: 1))
                viewU1.backgroundColor = UIColor.init(red: 239/255, green: 239/255, blue: 239/255, alpha: 1)
                self.scrollView.addSubview(viewU1)
                
                
                
                
                for obj in objec! {
                    
                    if (object?.objectId == obj["restaurantId"] as? String) {
                        
                        let userDefaults = Foundation.UserDefaults.standard
                        let idHotel  = userDefaults.string(forKey: "id")
                        if (idHotel == obj["hotelId"] as? String) {
                            
                            
                            self.imageConc.frame = CGRect(x: 20, y: viewU1.frame.maxY + 10, width: 50, height: 50)
                            self.imageConc.layer.cornerRadius = 25
                            self.scrollView.addSubview(self.imageConc)
                            self.labelHotel.frame = CGRect(x: 80, y: viewU1.frame.maxY + 10, width: self.view.frame.width - 60, height: 20)
                            self.scrollView.addSubview(self.labelHotel)
                            self.nomeCon.frame = CGRect(x: 80, y: self.labelHotel.frame.maxY + 10, width: self.view.frame.width - 60, height: 20
                            )
                            self.scrollView.addSubview(self.nomeCon)
                            
                            
                            
                            let query = PFQuery(className:"Hotels")
                            query.getObjectInBackground(withId: (obj["hotelId"] as? String)!, block: { (o, error) -> Void in
                                
                                self.nomeCon.text = o?["ciergeName"] as? String
                                self.labelHotel.text = o?["name"] as? String
                                let foto = o?["ciergePhoto"] as! PFFile
                                foto.getDataInBackground { (imageData, error) -> Void in
                                    if (error == nil) {
                                        if (imageData != nil) {
                                            self.imageConc.image = UIImage(data:imageData!)
                                        }
                                    }
                                }
                                
                            })
                            
                            self.descri.frame = CGRect(x: 20, y: self.imageConc.frame.maxY + 10, width: self.view.frame.width - 40, height: 250)
                            
                            self.descri.text = obj["description"] as! String
                            self.descri.textColor = UIColor.lightGray
                            self.scrollView.addSubview(self.descri)
                            
                            
                            self.mapView.frame = CGRect(x: 0, y: self.descri.frame.maxY + 10, width: self.scrollView.frame.width, height: 300)
                            self.scrollView.addSubview(self.mapView)
                            
                            self.scrollView.contentSize = CGSize(width: self.view.frame.width, height: self.mapView.frame.maxY + 70)
                            
                            
                        }} else if (object?.objectId != obj["restaurantId"] as? String) {
                        
                        
                        /*self.imageConc.frame = CGRect(x: 20, y: viewU1.frame.maxY + 10, width: 50, height: 1)
                         self.scrollView.addSubview(self.imageConc)
                         self.labelHotel.frame = CGRect(x: 80, y: viewU1.frame.maxY + 10, width: self.view.frame.width - 60, height: 1)
                         self.scrollView.addSubview(self.labelHotel)
                         self.nomeCon.frame = CGRect(x: 80, y: self.labelHotel.frame.maxY + 10, width: self.view.frame.width - 60, height: 1)
                         self.scrollView.addSubview(self.nomeCon)*/
                        
                        self.descri.frame = CGRect(x: 20, y: viewU1.frame.maxY + 10, width: self.view.frame.width - 40, height: 250)
                        
                        self.descri.text = ((object?["info"] as! String).components(separatedBy: "*"))[3]
                        self.descri.textColor = .lightGray
                        self.scrollView.addSubview(self.descri)
                        
                        self.mapView.frame = CGRect(x: 0, y: self.descri.frame.maxY + 10, width: self.scrollView.frame.width, height: 300)
                        self.scrollView.addSubview(self.mapView)
                        
                        self.scrollView.contentSize = CGSize(width: self.view.frame.width, height: self.mapView.frame.maxY + 70)
                        
                    }
                    
                    
                    
                }
                
                
                
                
                
            }
            
        })
        
        
        view.addSubview(scrollView)
        
        let locationBtn : UIButton = UIButton()
        locationBtn.frame = CGRect(x: scrollView.frame.width - 60, y: view.frame.height - 60, width: 40, height: 40)
        let imageLocation : UIImage = UIImage(named: "locationBtn.png")!
        locationBtn.setImage(imageLocation, for: .normal)
        view.addSubview(locationBtn)
        let btBack : UIButton = UIButton(frame: CGRect(x: 5, y: scrollView.frame.maxY - 50, width: 150, height: 30))
        btBack.setImage(UIImage(named:"cierge-trasp-1.png"), for: .normal)
        btBack.layer.cornerRadius = 15
        btBack.clipsToBounds = true
        btBack.setTitle("Back", for: .normal)
        btBack.setTitleColor(.white, for: .normal)
        btBack.contentHorizontalAlignment = .left
        btBack.addTarget(self, action: #selector(ristoTemplate.pop(_:)), for: .touchUpInside)
        //view.addSubview(btBack)
        let labelBack : UILabel = UILabel(frame: CGRect(x: 15, y: scrollView.frame.maxY - 50 + 15 - 10, width: 40, height: 20))
        labelBack.text = "Back"
        labelBack.textColor = .white
        labelBack.textAlignment = .center
        //view.addSubview(labelBack)
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        if scrollView.contentOffset.y < -30 {
            self.navigationController?.popToRootViewController(animated: true)
        }
    }
    
    func pop(_ button: UIButton) {
        _ = navigationController?.popViewController(animated: true)
    }
    
    func pop2() {
        _ = navigationController?.popViewController(animated: true)
    }
    
    func dismiss(_ sender: UIButton) {
        let id = "5"
        let userDefaults = Foundation.UserDefaults.standard
        userDefaults.set( id, forKey: "numTemplate")
        let vc = self.storyboard!.instantiateViewController(withIdentifier: "hotelController") as! hotelController
        self.present(vc, animated: true, completion: nil)
    }
    
    func pay(_ button: UIButton) {
        
        let user = PFUser.current()?.objectId
        var checkInDate = ""
        var checkOutDate = ""
        let userDefaults = Foundation.UserDefaults.standard
        let value  = userDefaults.string(forKey: "id")
        
        let query = PFQuery(className: "UserCheckin")
        query.findObjectsInBackground { (objects, error) -> Void in
            
            for object in objects! {
                
                let userID = object["userId"] as! String
                let hotelID = object["hotelId"] as! String
                if (userID == user){
                    if(value == hotelID) {
                        
                        checkInDate = object["checkIndate"] as! String
                        checkOutDate = object["checkOutDate"] as! String
                        
                        let dateFormatter = DateFormatter()
                        dateFormatter.dateFormat = "dd-MM-yy HH:mm"
                        let arrDataIn = checkInDate.components(separatedBy: "/")
                        let arrDataOut = checkOutDate.components(separatedBy: "/")
                        
                        let giornoIn = arrDataIn[0]
                        let meseIn = arrDataIn[1]
                        let annoIn = "20" + arrDataIn[2]
                        let giornoOut = arrDataOut[0]
                        let meseOut = arrDataOut[1]
                        let annoOut = "20" + arrDataOut[2]
                        var oraOpen = ""
                        var oraClose = ""
                        
                        if (self.arrayOre[0] == "1") {
                            
                            oraOpen = self.arrayOre[1]
                            oraClose = self.arrayOre[2]
                            
                            
                        } else {
                            
                            oraOpen = self.arrayOre[1]
                            oraClose = self.arrayOre[4]
                            
                        }
                        let dataIn = giornoIn + "-" + meseIn + "-" + annoIn + " " + oraOpen
                        let dataOut = giornoOut + "-" + meseOut + "-" + annoOut + " " + oraClose
                        let newdataIn = dateFormatter.date(from: dataIn)
                        let newdataOut = dateFormatter.date(from: dataOut)
                        self.datePicker.minimumDate = newdataIn
                        self.datePicker.maximumDate = newdataOut
                    }
                }
                
            }
            
            let alertController = UIAlertController(title: "Seleziona data e ospiti", message:"\n\n\n\n\n\n" , preferredStyle: UIAlertControllerStyle.alert)
            
            
            alertController.view.frame = CGRect(x: 0, y: 0, width: 300, height: 200)
            alertController.view.bounds = CGRect(x: 0, y: 0, width: 300, height: 200)
            self.datePicker.frame = CGRect(x: 5, y: 50, width: 260, height: 100)
            alertController.view.addSubview(self.datePicker)
            
            let cancelAction = UIAlertAction(title: "Annulla", style: .destructive) { (action) in
                
            }
            
            let confirmAction = UIAlertAction(title: "Conferma", style: UIAlertActionStyle.cancel, handler: {
                (action : UIAlertAction!) -> Void in
                
                
            })
            
            alertController.addTextField { (textField : UITextField!) -> Void in
                textField.text = "1"
                textField.textAlignment = .center
                //textField.backgroundColor = UIColor.init(red: 239/255, green: 239/255, blue: 239/255, alpha: 1)
                textField.layer.cornerRadius = 5
                textField.borderStyle = .none
                textField.layer.borderWidth = 0
            }
            
            
            alertController.addAction(cancelAction)
            alertController.addAction(confirmAction)
            self.present(alertController, animated: true, completion: nil)
            
        }
        
    }
    
    func mapViewDidFinishLoadingMap(_ mapView: MKMapView) {
        
        let userDefaults = Foundation.UserDefaults.standard
        let value  = userDefaults.string(forKey: "idService")
        
        let query = PFQuery(className:"Places")
        query.getObjectInBackground(withId: value!, block: { (object, error) -> Void in
            
            let coords = object?["coords"] as! String
            let arraycoords = coords.components(separatedBy: "_")
            let latitudine = Double(arraycoords[0])
            let longitudine = Double(arraycoords[1])
            
            let dropPin = MKPointAnnotation()
            let coordinate2d : CLLocationCoordinate2D = CLLocationCoordinate2D(latitude: latitudine!, longitude: longitudine!)
            dropPin.coordinate = coordinate2d
            self.mapView.addAnnotation(dropPin)
            let regione = MKCoordinateRegion.init(center: coordinate2d, span: MKCoordinateSpan(latitudeDelta: 0.1, longitudeDelta: 0.1))
            self.mapView.setRegion(regione, animated: true)
            
        })
        
    }
    
    
    
}
