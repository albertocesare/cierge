//
//  subReservationsCell.swift
//  cierge
//
//  Created by Alberto Cesare Barbon on 30/01/17.
//  Copyright © 2017 Alberto Cesare Barbon. All rights reserved.
//

import UIKit

class subReservationsCell: UITableViewCell {
    
    @IBOutlet weak var pallino: UIView!
    @IBOutlet weak var labelTitolo: UILabel!
    @IBOutlet weak var labelHote: UILabel!
    @IBOutlet weak var labelOra: UILabel!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
