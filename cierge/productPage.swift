//
//  productPage.swift
//  cierge
//
//  Created by Alberto Cesare Barbon on 17/01/17.
//  Copyright © 2017 Alberto Cesare Barbon. All rights reserved.
//

import UIKit
import Parse

class productPage: UIViewController, UICollectionViewDelegate, UICollectionViewDataSource {
    
    var collectionView: UICollectionView!
    var arrayPFObj : [PFObject] = []

    override func viewDidLoad() {
        super.viewDidLoad()
        
        let layout: UICollectionViewFlowLayout = UICollectionViewFlowLayout()
        layout.sectionInset = UIEdgeInsets(top: 40, left: 20, bottom: 40, right: 20)
        layout.itemSize = CGSize(width: 150, height: 150)

        let frame : CGRect = CGRect(x: 0, y: 0, width: view.frame.width, height: view.frame.height - 104)
        self.collectionView = UICollectionView(frame: frame, collectionViewLayout: layout)
        
        collectionView.delegate = self
        collectionView.dataSource = self
        collectionView.backgroundColor = .white
        let nibName = UINib(nibName: "CollectionViewCell", bundle:nil)
        self.collectionView.register(nibName, forCellWithReuseIdentifier: "Cell")
        view.addSubview(collectionView)
        
        let userDefaults = Foundation.UserDefaults.standard
        let value  = userDefaults.string(forKey: "id")
        
        let query = PFQuery(className:"Products")
        query.findObjectsInBackground { (objects, error) -> Void in
            
            for object in objects! {
                
                let servizio = object["ownerId"] as! String
                print(servizio)
                if (servizio == value!) {
                    self.arrayPFObj.append(object)
                    self.collectionView.reloadData()
                }
                
            }
        }
        
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return arrayPFObj.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "Cell", for: indexPath) as! CollectionViewCell
        
        let pfObj = arrayPFObj[indexPath.row]
        let info = pfObj["name"] as! String
        
        cell.nomeLabel.text = info
        
        let prezzo = pfObj["price"] as! String
        
        cell.prezzoLabel.text = prezzo + " €"
        
        let objid = pfObj.objectId
        let query = PFQuery(className:"Products")
        query.getObjectInBackground(withId: objid!, block: { (object, error) -> Void in
            
            let foto = object?["photo"] as! PFFile
            foto.getDataInBackground { (imageData, error) -> Void in
                if (error == nil) {
                    if (imageData != nil) {
                        cell.imgView.image = UIImage(data:imageData!)
                    }
                }
            }
            
        })
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        let pfObj = arrayPFObj[indexPath.row]
        let userDefaults = Foundation.UserDefaults.standard
        userDefaults.set( pfObj.objectId, forKey: "idService")
        let value  = userDefaults.string(forKey: "id")
        let userId = PFUser.current()?.objectId
        
        let query = PFQuery(className: "UserCheckin")
        query.findObjectsInBackground { (objects, error) -> Void in
            
            for object in objects! {
                if (object["hotelId"] as? String == value) && (object["state"] as! String == "a") && (object["userId"] as? String == userId) {
        let vc = self.storyboard!.instantiateViewController(withIdentifier: "productController") as! productController
        self.present(vc, animated: true, completion: nil)
                
                break
                }
                }
        }
        
    }

}
