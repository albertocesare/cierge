//
//  CollectionViewCell.swift
//  cierge
//
//  Created by Alberto Cesare Barbon on 18/01/17.
//  Copyright © 2017 Alberto Cesare Barbon. All rights reserved.
//

import UIKit

class CollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var imgView: UIImageView!
    @IBOutlet weak var nomeLabel: UILabel!
    @IBOutlet weak var prezzoLabel: UILabel!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

}
