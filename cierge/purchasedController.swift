//
//  purchasedController.swift
//  cierge
//
//  Created by Alberto Cesare Barbon on 03/03/17.
//  Copyright © 2017 Alberto Cesare Barbon. All rights reserved.
//

import UIKit
import Parse

class purchasedController: UIViewController, UITableViewDelegate, UITableViewDataSource {
    
    var tableView = UITableView()
    var array : [PFObject] = []

    override func viewDidLoad() {
        super.viewDidLoad()

        tableView.delegate = self
        tableView.dataSource = self
        tableView.frame = CGRect(x: 0, y: 0, width: view.frame.width, height: view.frame.height - 160)
        self.tableView.tableFooterView = UIView()
        view.addSubview(tableView)
        
        NotificationCenter.default.addObserver(self, selector: #selector(purchasedController.query), name: NSNotification.Name.init(rawValue: "reload"), object: nil)
        
        query()
        
    }
    
    override func viewDidAppear(_ animated: Bool) {
        query()
    }
    
    
    func query () {
        array = []
        let statearr = ["a", "d"]
        let userId = PFUser.current()?.objectId
        let query = PFQuery(className: "Purchasing")
        //query.cachePolicy = .cacheElseNetwork
        query.whereKey("userId", equalTo: userId!)
        //query.whereKey("state", equalTo: "a")
        //query.whereKey("state", equalTo: "d")
        query.whereKey("state", containedIn: statearr)

        query.findObjectsInBackground { (objects, error) -> Void in
            
            
            for object in objects! {
                
                self.array.append(object)
                if objects?.count == 0 {
                    
                    let textView : UITextView = UITextView()
                    textView.frame = CGRect(x: 20, y: self.view.frame.height / 2 - 50, width: self.view.frame.width - 40, height: 100)
                    textView.textAlignment = .center
                    textView.text = "In questa pagina visualizzerai i tuoi acquisti"
                    textView.font = UIFont.systemFont(ofSize: 15)
                    textView.textColor = .darkGray
                    self.view.addSubview(textView)
                }
                
            }
            
            self.tableView.reloadData()
            
        }
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return array.count
    }

    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 70
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = UITableViewCell()
        
        let random = arc4random_uniform(5)
        var color = UIColor()
        switch random {
        case 0:
            
            color = UIColor.init(red: 230/255, green: 37/255, blue: 101/255, alpha: 1)
            
            break
        case 1:
            
            color = UIColor.init(red: 43/255, green: 152/255, blue: 240/255, alpha: 1)
            
            break
        case 2:
            
            color = UIColor.init(red: 80/255, green: 174/255, blue: 85/255, alpha: 1)
            
            break
        case 3:
            
            color = UIColor.init(red: 253/255, green: 192/255, blue: 47/255, alpha: 1)
            
            break
        case 4:
            
            color = UIColor.init(red: 21/255, green: 149/255, blue: 136/255, alpha: 1)
            
            break
        case 5:
            
            color = UIColor.init(red: 241/255, green: 69/255, blue: 51/255, alpha: 1)
            
            break
        default:
            break
        }
        
        let buttonIn = UIButton(frame: CGRect(x: 15, y: 10, width: 50, height: 50))
        buttonIn.layer.cornerRadius = 25
        buttonIn.isUserInteractionEnabled = false
        buttonIn.clipsToBounds = true
        buttonIn.backgroundColor = #colorLiteral(red: 0.9019607843, green: 0.7450980392, blue: 0.4352941176, alpha: 1)
        buttonIn.setTitleColor(.white, for: .normal)
        cell.addSubview(buttonIn)
        
        let pfObj = array[indexPath.row]
        let title = UILabel(frame: CGRect(x: 80, y: cell.frame.height/2 - 10, width: cell.frame.width - 60, height: 20))
        let hotelId = pfObj["productId"]
        let query = PFQuery(className: "Products")
        query.getObjectInBackground(withId: hotelId as! String, block: { (object, error) -> Void in
            
            let name = object?["name"] as! String
            title.text = name
            cell.addSubview(title)
            
            let arrname = name.components(separatedBy: " ")
            let string1 = arrname[0]
            let ini1 = string1.characters.first
            let stringa = String(describing: ini1!)
            
            buttonIn.setTitle(stringa, for: .normal)
            
            
        })
        
        let labelDate = UILabel(frame: CGRect(x: 80, y: title.frame.maxY + 10, width: cell.frame.width - 60, height: 20))
        
        labelDate.textColor = .gray
        labelDate.font = UIFont.systemFont(ofSize: 14)
        
        
        
        let m = pfObj["method"] as! String
        if (m == "d") {
            
            let deliv = pfObj["delivered"] as! Bool
            if (deliv == true) {
                
                labelDate.text = "Inviato"
                
            } else {
                
                labelDate.text = "Non Ancora Inviato"
                
            }
            
        }
        
        print(pfObj.objectId)
        print(pfObj["delivered"] as! Bool)
        if (pfObj["delivered"] as! Bool == true) {
            
            //labelDate.text = "Pronto per il ritiro"
            
            if (pfObj["method"] as! String == "d") {
                labelDate.text = "Spedito"
            } else {
                labelDate.text = "Consegnato"
            }
            labelDate.text = ""
        }
        
        
        
        if (pfObj["state"] as! String == "d") {
            labelDate.text = "Ordine rifiutato"
        }
        
        cell.addSubview(labelDate)
        
        
        return cell
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        
        self.tableView.reloadData()
        self.tableView.tableFooterView = UIView()
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        tableView.deselectRow(at: indexPath, animated: true)
        
    }
    

}
