//
//  cartController1.swift
//  cierge
//
//  Created by Alberto Cesare Barbon on 03/03/17.
//  Copyright © 2017 Alberto Cesare Barbon. All rights reserved.
//

import UIKit
import PageMenu

class cartController1: UIViewController, CAPSPageMenuDelegate {
    
    var pageMenu : CAPSPageMenu?

    override func viewDidLoad() {
        super.viewDidLoad()
        

        UIApplication.shared.statusBarStyle = .default
        
        let viewU : UIView = UIView(frame: CGRect(x: 0, y: 0, width: view.frame.width, height: 64))
        let notifica : UIButton = UIButton(frame: CGRect(x: view.frame.width - 37.5, y: 27.5, width: 30, height: 30))
        notifica.setImage(UIImage(named: "bell.ong")?.maskWith(color: .darkGray), for: .normal)
        notifica.addTarget(self, action: #selector(cartController1.notifica), for: .touchUpInside)
        viewU.addSubview(notifica)
        viewU.backgroundColor = UIColor.init(red: 235/255, green: 235/255, blue: 235/255, alpha: 1)
        view.addSubview(viewU)
        
        let label : UILabel = UILabel(frame: CGRect(x: 20, y: 30, width: view.frame.width-40, height: 20))
        label.textAlignment = .center
        label.text = "Acquisti"
        label.textColor = UIColor.darkGray
        label.font = UIFont(name: "HelveticaNeue", size: 20)
        viewU.addSubview(label)
        
        
        var controllerArray : [UIViewController] = []
        
        
        let controllerInser : UIViewController = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "purchasedController") as! purchasedController
        controllerInser.title = "Confermati"
        controllerArray.append(controllerInser)
        
        let controllerMain : UIViewController = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "cartcartController") as! cartcartController
        controllerMain.title = "In attesa"
        controllerArray.append(controllerMain)
        
        let parameters: [CAPSPageMenuOption] = [
            .menuItemSeparatorWidth(0),
            .useMenuLikeSegmentedControl(true),
            .menuItemSeparatorPercentageHeight(0.1)
        ]
        pageMenu = CAPSPageMenu(viewControllers: controllerArray, frame: CGRect(x: 0, y: 74, width: self.view.frame.width, height: view.frame.height - 64), pageMenuOptions: parameters)
        
        
        pageMenu?.viewBackgroundColor = UIColor.white
        pageMenu?.selectionIndicatorColor = UIColor.yellow
        pageMenu?.selectedMenuItemLabelColor = UIColor.black
        pageMenu?.unselectedMenuItemLabelColor = UIColor.lightGray
        pageMenu?.scrollMenuBackgroundColor = UIColor.white
        pageMenu?.enableHorizontalBounce = false
        pageMenu?.useMenuLikeSegmentedControl = true
        
        for view in (self.pageMenu?.view.subviews)! {
            if let scrollView = view as? UIScrollView
            {
                scrollView.isScrollEnabled = false
            }
        }
        
        pageMenu!.delegate = self
        self.view.addSubview(pageMenu!.view)
    }
    

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func notifica() {
        let vc = self.storyboard!.instantiateViewController(withIdentifier: "notificaController") as! notificaController
        self.present(vc, animated: true, completion: nil)
    }

    override func viewWillAppear(_ animated: Bool) {
        UIApplication.shared.statusBarStyle = .default
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
