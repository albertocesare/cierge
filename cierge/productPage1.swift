//
//  productPage1.swift
//  cierge
//
//  Created by Alberto Cesare Barbon on 23/02/17.
//  Copyright © 2017 Alberto Cesare Barbon. All rights reserved.
//

import UIKit
import Parse
import ParseUI

class productPage1: PFQueryCollectionViewController {
    
    var array : [PFObject] = []

    
    override func queryForCollection() -> PFQuery<PFObject> {
        
        let userDefaults = Foundation.UserDefaults.standard
        let idCheck = userDefaults.value(forKey: "idCheck")
        
        if (idCheck as! String != "none") {
            
            collectionView?.allowsSelection = true
        } else {
            collectionView?.allowsSelection = false
            
            self.perform(#selector(self.popUp), with: nil, afterDelay: 0.5)
        }
        
        self.collectionView!.register(ProductsCollectionViewCell.self, forCellWithReuseIdentifier: "cell")
        navigationController?.navigationBar.isHidden = true
        
        let idHotel  = userDefaults.string(forKey: "id")
        let query = PFQuery(className: "Products")
        
        query.cachePolicy = .cacheElseNetwork
        query.whereKey("sellerId", equalTo: idHotel!)
        query.whereKey("active", equalTo: true as Bool)
        
        collectionView?.contentInset.top = 10
        
        
        
        return query
    }
    
    func popUp () {
        let alertController = UIAlertController(title: "Attenzione", message: "Puoi usufruire del servizio solo dopo aver ricevuto conferma di check-in da parte dell'hotel", preferredStyle: UIAlertControllerStyle.alert)
        
        let saveAction = UIAlertAction(title: "Ok", style: UIAlertActionStyle.cancel, handler: {
            alert -> Void in
            
            
        })
        
        
        
        alertController.addAction(saveAction)
        self.present(alertController, animated: true, completion: nil)
    }
    
    
    override func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath, object: PFObject?) -> PFCollectionViewCell? {
        
        array.append(object!)
        
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cell", for: indexPath) as? PFCollectionViewCell
        
        
        cell?.imageView.image = UIImage(named: "cierge-trasp-1.png")
        let imageFile = object?.object(forKey: "photo") as? PFFile
        
        cell?.imageView.file = imageFile
        cell?.imageView.loadInBackground()
        
        /*cell?.cellImageView.file = imageFile
        cell?.cellImageView.loadInBackground()
        
    */
    
        
        cell?.textLabel.textAlignment = .center
        cell?.textLabel.font = UIFont.systemFont(ofSize: 15)
        cell?.textLabel.textColor = .darkGray
        cell?.textLabel.text = object?["name"] as! String + " \n " + String(describing: object?["price"] as! String) + "€"
        
        
        
        return cell
        
        
    }
    
    
    override func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        let userDefaults = Foundation.UserDefaults.standard
        let idCheck = userDefaults.value(forKey: "idCheck")
        print(idCheck!)
        if (idCheck as! String != "none") {
            
        collectionView.allowsSelection = true
        let pfObj = array[indexPath.row]
        let userDefaults = Foundation.UserDefaults.standard
        userDefaults.set( pfObj.objectId, forKey: "idService")
        
        collectionView.deselectItem(at: indexPath, animated: true)
        self.performSegue(withIdentifier: "segueProd", sender: self)
            
        } else {
            collectionView.allowsSelection = false
            let alertController = UIAlertController(title: "Attenzione", message: "Puoi usufruire del servizio solo dopo aver ricevuto conferma di check-in da parte dell'hotel", preferredStyle: UIAlertControllerStyle.alert)
            
            let saveAction = UIAlertAction(title: "Ok", style: UIAlertActionStyle.cancel, handler: {
                alert -> Void in
                
                
            })
            
            alertController.addAction(saveAction)
            self.present(alertController, animated: true, completion: nil)
        }
    }
    
    

}
