//
//  placePage1.swift
//  cierge
//
//  Created by Alberto Cesare Barbon on 24/02/17.
//  Copyright © 2017 Alberto Cesare Barbon. All rights reserved.
//

import UIKit
import ParseUI
import Parse

class placePage1: PFQueryTableViewController {

    var array : [PFObject] = []
    
    
    override func queryForTable() -> PFQuery<PFObject> {
        
        //let query = PFQuery(className: "Restaurants")
        //query.cachePolicy = .cacheElseNetwork
        
        //query.whereKey("active", equalTo: true as Bool)
        navigationController?.navigationBar.isHidden = true
        
        
        let userDefaults = Foundation.UserDefaults.standard
        let idCheck = userDefaults.value(forKey: "idCheck")
        
        if (idCheck as! String != "none") {
            tableView.allowsSelection = true
        } else {
            tableView.allowsSelection = false
            
            self.perform(#selector(self.popUp), with: nil, afterDelay: 0.5)
        }
        
        let arrayid  = userDefaults.array(forKey: "arrayPlac")
        let query = PFQuery(className: "Places")
        query.cachePolicy = .cacheElseNetwork
        query.whereKey("objectId", containedIn: arrayid!)
        query.whereKey("active", equalTo: true as Bool)
        
        
        return query
        
    }
    
    func popUp () {
        let alertController = UIAlertController(title: "Attenzione", message: "Puoi usufruire del servizio solo dopo aver ricevuto conferma di check-in da parte dell'hotel", preferredStyle: UIAlertControllerStyle.alert)
        
        let saveAction = UIAlertAction(title: "Ok", style: UIAlertActionStyle.cancel, handler: {
            alert -> Void in
            
            
        })
        
        
        
        alertController.addAction(saveAction)
        self.present(alertController, animated: true, completion: nil)
    }
    
    
    
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath, object: PFObject?) -> PFTableViewCell? {
        
        array.append(object!)
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "Cell", for: indexPath) as! PlacTableViewCell
        
        let imageFile = object?.object(forKey: "photo") as? PFFile
        cell.cellImageView.backgroundColor = .clear //placeholder
        
        cell.cellImageView.file = imageFile
        cell.cellImageView.loadInBackground()
        
        cell.titleLabel.text = object?["name"] as? String
        
        return cell
        
        
    }
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        return 220
        
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        print("ciao")
        
        let pfObj = array[indexPath.row]
        let userDefaults = Foundation.UserDefaults.standard
        userDefaults.set( pfObj.objectId, forKey: "idService")
        let idCheck = userDefaults.value(forKey: "idCheck")
        
        if (idCheck as! String != "none") {
        
            tableView.allowsSelection = true
        tableView.deselectRow(at: indexPath, animated: true)
        //self.performSegue(withIdentifier: "plaSegue", sender: self)
            
        } else {
            let alertController = UIAlertController(title: "Attenzione", message: "Puoi usufruire del servizio solo dopo aver ricevuto conferma di check-in da parte dell'hotel", preferredStyle: UIAlertControllerStyle.alert)
            
            let saveAction = UIAlertAction(title: "Ok", style: UIAlertActionStyle.cancel, handler: {
                alert -> Void in
                
                
            })
            
            alertController.addAction(saveAction)
            self.present(alertController, animated: true, completion: nil)
        }
        
    }
    
    
    
}
