//
//  AutoCompleteCell.swift
//  Autocomplete
//
//  Created by Amir Rezvani on 3/6/16.
//  Copyright © 2016 cjcoaxapps. All rights reserved.
//

import UIKit
import Foundation

open class AutoCompleteCell: UITableViewCell {
    //MARK: - outlets
    @IBOutlet fileprivate weak var lblTitle: UILabel!
    @IBOutlet fileprivate weak var lblId: UILabel!
    @IBOutlet fileprivate weak var imgIcon: UIImageView!

    //MARK: - public properties
    open var textImage: AutocompleteCellData? {
        didSet {
            
            //self.lblTitle.textColor = .white
            let full: [String] = (textImage?.text.components(separatedBy: "*"))!
            self.lblId.text = full[1]
            self.lblTitle.text = full[0]
            self.imgIcon.image = textImage!.image
            
        }
    }
}
